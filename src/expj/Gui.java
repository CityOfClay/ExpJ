/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expj;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ComponentListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author MultiTool
 */
public class Gui {
  /* ********************************************************************************* */
  public static final class DrawingContext {// Let's be final until we can't anymore
    public Graphics2D gr;
    public int RecurseDepth;
    public double Excitement;// to highlight animation, range 0 to 1. 
  /* ********************************************************************************* */
    public DrawingContext() {
    }
    /* ********************************************************************************* */
    public DrawingContext(DrawingContext Fresh_Parent) {
      this.gr = Fresh_Parent.gr;
      this.RecurseDepth = Fresh_Parent.RecurseDepth + 1;
    }
  }
  /* ********************************************************************************* */
  public static class MainGui {
    public JFrame frame;
    public DrawingPanel drawpanel;
    /* ********************************************************************************* */
    public MainGui() {
      this.frame = new JFrame();
      this.frame.setTitle("Grid");
      this.frame.setSize(950, 950);
      this.frame.addWindowListener(new WindowAdapter() {
        @Override public void windowClosing(WindowEvent e) {
          System.exit(0);
        }
      });
      Container contentPane = this.frame.getContentPane();
      this.drawpanel = new DrawingPanel();
      contentPane.add(this.drawpanel);
      // this.drawpanel.BigApp = this;
      frame.setVisible(true);
    }
    /* ********************************************************************************* */
  }
  /* ********************************************************************************* */
  public static class DrawingPanel extends JPanel {// implements MouseMotionListener, MouseListener, MouseWheelListener, ComponentListener, KeyListener {
//    Waves WGrid;
    Waves1 WGrid;
    /* ********************************************************************************* */
    public DrawingPanel() {
//      this.WGrid = new Waves();
      this.WGrid = new Waves1();
//      this.addMouseListener(this);
//      this.addMouseMotionListener(this);
//      this.addMouseWheelListener(this);
//      this.addKeyListener(this);
//      WGrid.RunMe();
    }
    /* ********************************************************************************* */
    public void Draw_Me(Graphics2D g2d) {
      DrawingContext dc = new DrawingContext();
      dc.gr = g2d;
      int wdt, hgt;

      wdt = this.getWidth();
      hgt = this.getHeight();

      Rectangle2D rect = new Rectangle2D.Float();
      if (true) {
        rect.setRect(0, 0, wdt, hgt);
      }
      Stroke oldStroke = g2d.getStroke();
      BasicStroke bs = new BasicStroke(5f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
      g2d.setStroke(bs);
      g2d.setColor(Color.green);
      g2d.draw(rect);// green rectangle confidence check for clipping
      g2d.setStroke(oldStroke);
      
      WGrid.Draw_Me(dc);
      WGrid.RunCycle();
      this.repaint();
      try {
//        Thread.sleep(10);
      } catch (Exception ex) {
      }
    }
    /* ********************************************************************************* */
//    @Override public void update(Graphics g) {
//      super.paintComponent(g);
//      Graphics2D g2d = (Graphics2D) g;
//      // Draw_Me(g2d);
//    }
    /* ********************************************************************************* */
    @Override public void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2d = (Graphics2D) g;
      Draw_Me(g2d);
    }
  }
}
