package expj;

import static java.lang.Integer.toUnsignedLong;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
public class ExpJ {

  public static class Person {
    public double age;
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    if (true) {
      CoilForce.LowPass();
//      CoilForce.CoilForce();
      return;
    }
    if (true) {
      CoilForce.Capacitors();
      return;
    }
    if (true) {
      Verlet.spring_test();
      return;
    }
    if (false) {
      Verlet.VerletValues velocityVerlet = Verlet.velocity_verlet(5.0, -10, 0.01);
      System.out.println("[#]\nTime for velocity Verlet integration is:");
      System.out.println(velocityVerlet.time);
      System.out.println("[#]\nVelocity for velocity Verlet integration is:");
      System.out.println(velocityVerlet.vel);
      System.out.println();
    }
    if (true) {
      RMS();
      return;
    }
    if (true) {
      double turtle_head_start = 4751;
      double rabbit_head_start = 370;
      double turtle = 4751;
      double rabbit = 370;
      turtle -= rabbit_head_start;
      rabbit = 0;
      while (rabbit < turtle) {
        turtle += 1.0 / 7.0;
        rabbit += 1.0;
        System.out.println("Turtle:" + (turtle + rabbit_head_start) + ", rabbit:" + (rabbit + rabbit_head_start));
      }
      // Turtle:5481.285714284386, rabbit:5482.0 
      // X*6.0 = (turtle_head_start-rabbit_head_start)*7.0;
      // X = (turtle_head_start-rabbit_head_start)*7.0/6.0;
      System.out.println(rabbit_head_start + (turtle_head_start - rabbit_head_start) * 7.0 / 6.0);
    }
    Person person = new Person() {
      {
        age = 100;
      }
    };

    if (true) {
      Airship ship = new Airship();
      ship.Run();
      return;
    }
    if (false) {
      if (Waves.Mode == Waves.DisplayMode.Graphic) {
        Gui.MainGui mg = new Gui.MainGui();
      } else {
        Waves grid = new Waves();
        grid.RunMe();
      }
      return;
    }
    if (true) {
      ParseFun.Test();
      return;
    }
    if (true) {
//      EnergyMesh.WaveMetric();
      EnergyMesh.Test();
      return;
    }
    if (false) {
      double Angle = Math.PI / 2.0;// 0.0;
      Angle += 0.1;
      double Sin = Math.sin(Angle);
      double Cos = Math.cos(Angle);
      System.out.println(Sin);
      if (false) {
        double avg = 1.0 / 3.0;// global average for 3 cells.
        double cell0 = Math.abs(1.0 - avg);
        double cell1 = Math.abs(0.0 - avg);
        double sum0 = (cell0) + (cell1 * 2.0);// sum = (double) 1.3333333333333335
        double energy0 = (cell0 * cell0) + (cell1 * cell1 * 2.0);// energy0 = (double) 0.6666666666666667

        cell0 = Math.abs(1.0 - 0.5);// local averages for 3 cells in a line. 
        cell1 = Math.abs(0.0 - 0.25);
        double sum1 = (cell0) + (cell1 * 2.0);// sum1 = (double) 1.0
        double energy1 = (cell0 * cell0) + (cell1 * cell1 * 2.0);// energy1 = (double) 0.375
        return;
      }
    }
    if (false) {
      Waves1.TestFall();
      return;
    }
    if (false) {
      Waves1.WaveMetric_Trig();
//      Waves.WaveMetric_Trig();
//      Waves.WaveMetric_Simple();
//      Waves.WaveMetric();
      return;
    }
    if (false) {// testing split() function 
      String test = "Hello   World";
      String blocks[] = test.split(" ");
      System.out.println();
    }
    if (false) {
//      CsvBalance.TestBalance();
      return;
    }
    if (false) {
      Barrel.Test();
      return;
    }
    if (false) {
      Waves.Find_Closest_On_Curve(0, 0.4, 0.29552020666);
      return;
    }
    if (false) {
      Waves.TestDoppler();
      return;
    }
    if (false) {
      Waves.LorentzMass(1.0, 0.5);
      Waves.LorentzMass(1.0, 0.25);
      Waves.LorentzMass(1.0, 0.33333);
      Waves.LorentzMass(1.0, 0.98765);
      Waves.LorentzMass(1.0, 0.1);
      Waves.TestSines();
      return;
    }
    if (true) {
      Hyperbolas.SpeedField();
//      Hyperbolas.Test();
      return;
    }
    if (true) {
      test();
      return;
    }
    if (false) {
      HubbleEnergy.Calc();
    }
  }
  /* **************************************************************************** */
  public static void RMS() {
    double SumAbs = 0;
    double SumSqr = 0;
    double Angle = 0;
    double Reps = 0;
    double sn = 0;
    while (Angle < Math.PI) {
      sn = Math.sin(Angle);
      SumSqr += sn * sn;
      SumAbs += Math.abs(sn);
      Angle += 0.1;
      Reps += 1;
    }
    double Rms = Math.sqrt(SumSqr / Reps);// Rms:0.7070609430683882
    System.out.println("Rms:" + Rms);
    double spoo = 2.0 * Math.sqrt(2.0) * Rms;
    System.out.println("Reps:" + Reps + ", SumAbs:" + SumAbs);
    System.out.println("SumAbs/Reps:" + SumAbs / Reps);// Sum/Reps:0.6365372228011333
    System.out.println();
  }
  /* **************************************************************************** */
  public static int test() {
    //int[] prices = new int[]{7, 1, 5, 3, 6, 4};
    int[] prices = new int[]{7, 2, 7, 1, 3, 5, 4};// [7,1,5,3,6,4]
    if (prices.length == 0) {
      return 0;
    }
    int minPrice = Integer.MAX_VALUE;
    int maxProfit = 0;
    for (int i = 0; i < prices.length; i++) {
      if (prices[i] < minPrice) {
        minPrice = prices[i];
      } else if ((prices[i] - minPrice) > maxProfit) {
        maxProfit = prices[i] - minPrice;
      }
    }
    System.out.println("maxProfit:" + maxProfit);
    return maxProfit;
  }
}
