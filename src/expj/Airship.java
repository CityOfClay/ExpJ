package expj;

/**
 *
 * @author MultiTool
 *
 * Air drag of high atmosphere balloon to space.
 *
 */
// Air drag of high atmosphere balloon to space. 
public class Airship {
  static double density_sea_level = 1.2754; // to do: get units!
  static double air_density_sea_level_kgm3 = 1.225; // 1.225 kg/m^3  // https://macinstruments.com/blog/what-is-the-density-of-air-at-stp/
  static double density_2km = 1.007;
  static double density_50km = 0.001027;// air density at 50,000 meters  http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
  static double density_80km = 0.00001846;// air density in kg/m3 at 80 km altitude  https://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
  static double LEO_speed_kph = 28080.0; // 28,080 km/h
  static double LEO_speed_kps = LEO_speed_kph / 3600;
  static double LEO_speed_mps = LEO_speed_kps * 1000;// meters per second
  static double SEO_speed_mps = 7904.313199133749;// Surface Earth Orbit, meters per second
  static double earth_radius_km = 6371;// kilometers
  static double earth_radius_meters = earth_radius_km * 1000;// kilometers
  static double earth_grav_accel_mpss = 9.80665;// m/s2 //9.80665 m/s2
  // 0.17164962279633386 / 9.80665 = 0.01750339033
  static double newtons_per_pound = 4.44822;
  static double jet_thrust_nwt = 44000.0;// newtons
  static double mach_1_mps = 343;//  the speed of sound in air is about 343 metres per second
  // 7.354552372301305E-5 / 0.00001846 = 3.98404787232 times more dense than 80km high
  // 7.354552372301305E-5 / 0.001027 = 0.07161199973 times more dense than 50km high? 
  // 0.001027 / 7.354552372301305E-5 = 13.9641401408 50km high is 13.9641401408 more dense than this starting density
  static double hindenburg_weight_lbs = 474000; // lbs 474000 lbs to kilograms = 215002.783
  static double hindenburg_weight_kg = 215002.783; // lbs 474000 lbs to kilograms = 215002.783
  static double max_balloon_height_m = 48768.0;// The current highest altitude achieved by a NASA balloon is about 160,000 feet (48768 m). // https://www.nasa.gov/scientificballoons/faqs
  // 0.001027 / 1.007 = 0.00101986097
  // 0.00001846 / 1.007 = 0.00001833167
  // la displacement 78280.8 m3    https://en.wikipedia.org/wiki/USS_Los_Angeles_(ZR-3)
  // 78280.8 / 0.00001833167 = ?
  // 1.0 / (0.00001833167 ^ (1/3)) = 37.9256171343  USLA would have to be 38 times longer in every dimension to float to 80 km
  // if we double USLA's thrust, speed:12996.453567474255, speed/LEO:1.6662119958300325, so we could reach orbit. 
  public static void Run() {
    Zep zep;
    zep = Zep.Hindenburg();
//    zep = new Zep();
    zep.test();
    Cycle_New(zep);
    // Cycle();
  }

  /* **************************************************************************** */
  public static class Zep {
    public double resistance_area, drag_coefficient;
    public double thrust;// to do: rename to thrust
    public double top_speed_mps;
    public double mass;// total mass of airship
    public Zep() {
      // 1 Newton is the force needed to accelerate 1 kilogram of mass at the rate of 1 metre per second squared.
      // this.force = 50.0 * 0.000001;// 50 micronewtons of force per chip http://newsoffice.mit.edu/2012/microthrusters-could-propel-small-satellites-0817
      this.thrust = 5.4;// World record for ion drive is 5.4 Newtons https://www.space.com/38444-mars-thruster-design-breaks-records.html
      double ship_diameter = 27.6;// US Los Angeles zeppelin
      double ship_radius = (ship_diameter / 2.0);// US Los Angeles zeppelin
      this.resistance_area = Math.PI * ship_radius * ship_radius;

      // 0.020~0.025  is drag coefficent for airship, blimp, dirigible, zeppelin http://physics.info/drag/
      this.drag_coefficient = 0.023;// "0.023 for the relatively large slender Los Angeles" https://ntrs.nasa.gov/citations/19930091470
      this.mass = hindenburg_weight_kg;// fudge, wrong mass 
      // los angeles diameter 27.6 m
      // akron diamter 40.5 m,
      // https://grantvillegazette.com/wp/article/publish-507/  // Hindenburg 49,167 Newtons drag force at 84mph,  2,476 horsepower
    }
    public static Zep LosAngeles() {// US Los Angeles zeppelin
      Zep zep = new Zep();// https://www.airships.net/us-navy-rigid-airships/uss-los-angeles/
      // https://en.wikipedia.org/wiki/USS_Los_Angeles_(ZR-3)
      double ship_diameter = 27.639264;// US Los Angeles zeppelin, 90.68 feet diameter 
      double ship_radius = (ship_diameter / 2.0);
      zep.resistance_area = Math.PI * ship_radius * ship_radius;

      // 0.020~0.025  is drag coefficent for airship, blimp, dirigible, zeppelin http://physics.info/drag/
      zep.drag_coefficient = 0.023;// "0.023 for the relatively large slender Los Angeles" https://ntrs.nasa.gov/citations/19930091470

      // USLA weight unknown, but wiki says displacement = 2,764,460 cu ft (78,280.8 m3)
      // Assume neutral buoyancy at sea level, so 78,280.8 of air masses (1.225 kg/m^3)
      double Displacement_m3 = 78280.8;// meters cubed
      double Mass_kg = air_density_sea_level_kgm3 * Displacement_m3;
      zep.mass = Mass_kg;
      zep.top_speed_mps = 127.138 * 1000 / 3600;//  127.138 km/h, 35.3161111111 mps 

      zep.resistance_area = Math.PI * ship_radius * ship_radius;
      zep.calc_thrust(zep.top_speed_mps, density_2km);// According to this Hindenburg thrust is 21710.716672917828 Newtons 
      return zep;
      /*
Gas capacity: 2,599,110 cubic feetU
Useful lift: 66,970 lbs
Maximum speed: 79 MPH = 127.138 kph 
Cruising speed: 50 knotw
Original Powerplant: 5 Maybach VL-1 12-cylinder engines (400 HP per engine at 1,4000 RPM)
       */
    }
    public static Zep Hindenburg() {
      Zep zep = new Zep();//  https://www.airships.net/hindenburg/size-speed/  
      zep.mass = hindenburg_weight_kg;
      zep.top_speed_mps = 135.0 * 1000 / 3600;//  135 km/h, 37.5 mps 
      // 890 kW (1,190 hp) Daimler-Benz diesel engines
      // 135 km/h
      // Length: 245 m / 803.8 feet
      // Diameter: 41.2 m / 135.1 feet
      double ship_diameter = 41.2;
      double ship_radius = (ship_diameter / 2.0);// US Los Angeles zeppelin
      zep.resistance_area = Math.PI * ship_radius * ship_radius;
      zep.calc_thrust(zep.top_speed_mps, density_2km);// According to this Hindenburg thrust is 21710.716672917828 Newtons 
      return zep;
    }
    public double get_terminal_speed_mps(double air_density) {// meters per second
      //  terminal velocity equation  https://en.wikipedia.org/wiki/Terminal_velocity 
      double drag_factor = this.resistance_area * this.drag_coefficient;
      double terminal_v_mps = Math.sqrt((2.0 * this.thrust) / (air_density * drag_factor));
      return terminal_v_mps;
    }
    public double calc_thrust(double terminal_v_mps, double air_density) {// given top speed, calculate thrust.  
      double drag_factor = this.resistance_area * this.drag_coefficient;
      this.thrust = (terminal_v_mps * terminal_v_mps * (air_density * drag_factor)) / 2.0;// terminal v equation backward
      // 7935.345916335765 newtons * 24.7222222222 m/s = 196179.385154 watts  196,179.385154 = 263.0808890146744 horsepower for USLA 
      return this.thrust;
    }
    public double get_air_density(double terminal_v_mps, double thrust) {// kg per meters cubed
      //  terminal velocity equation  https://en.wikipedia.org/wiki/Terminal_velocity 
      double air_density;// given we know thrust and terminal v, what must the air density be?
      double drag_factor = this.resistance_area * this.drag_coefficient;
      air_density = (2.0 * thrust) / ((terminal_v_mps * terminal_v_mps * drag_factor));// terminal v equation backward
      return air_density;
    }
    public void test() {
      System.out.println("*************** start test");
      if (true) {
        double speed = get_terminal_speed_mps(density_50km);
        System.out.println("speed:" + speed + ", density:" + density_50km);
        double density = this.get_air_density(speed, this.thrust);
        double Speed_mach = 1174.250574155938 / 343;// Answer is Mach 3.4234710616791197
        System.out.println("speed:" + speed + ", density:" + density);// Hindenburg terminal v at 50km alt: 1174.250574155938 mps, density:0.001027 kgm3 
        // If you have an airship with the same thrust, size and shape as a zepplin but neutrally buoyant at 50km altitude, its terminal velocity will be over Mach 3. 
      }
      if (false) {
        double acc = centripetal_accel(LEO_speed_mps, earth_radius_meters);
        System.out.println("LEO_speed_mps:" + LEO_speed_mps);
        double speed = centripetal_speed(earth_grav_accel_mpss, earth_radius_meters);
        System.out.println("speed:" + speed);
      }
      if (false) {
        double fractaccel = centripetal_accel(LEO_speed_mps, earth_radius_meters);
        // earth_grav_accel_mpss = 9.80665;// m/s2 //9.80665 m/s2
        // 9.549521268246743 is the Gs we get from centripetal ????? 
        // so LEO speed is not quite enough to cancel out grave accel down. 
      }

      double terminal_v_mps = 89.0 * 1000 / 3600;// US Los Angeles, 24.7222222222 mps 
//      terminal_v_mps *= 2.0;
      // terminal_v_mps *= 1.0 / 0.8331059979150163;// tweak thrust upward for LEO
      terminal_v_mps *= 1.0 / 0.8221115003956678;// tweak thrust upward for SEO, more or less X 1.21638001599

      // 24.7222222222 /0.833105997915016 = 29.674762016 meters per second (sea level max speed) to reach orbit from 80km 
      this.calc_thrust(terminal_v_mps, density_sea_level);
      System.out.println("thrust:" + this.thrust);

      double initial_air_density_si_units = density_50km;// air density at 50,000 meters  http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
      initial_air_density_si_units = density_80km;
      // tweak thrust inadequately to see if feedback can fill the shortfall. 
      initial_air_density_si_units = this.get_air_density(SEO_speed_mps / 4.0, this.thrust);

      double speed = get_terminal_speed_mps(initial_air_density_si_units);
      System.out.println("terminal_v_mps:" + terminal_v_mps + ", speed:" + speed);// speed:6498.226783737127
      System.out.println("speed/SEO:" + (speed / SEO_speed_mps));// speed:6498.226783737127
      System.out.println("ato_hindenburg_weight_kg:" + (hindenburg_weight_kg * density_80km));
      System.out.println("*************** end test");
    }
  }
  /* **************************************************************************** */
  public static double centripetal_accel(double speed_mps, double radius_m) {
    double accel_mpss = (speed_mps * speed_mps) / radius_m; // V squared over R     
    return accel_mpss;
  }
  /* **************************************************************************** */
  public static double centripetal_speed(double accel_mpss, double radius_m) {
    double speed_mps = Math.sqrt(accel_mpss * radius_m);
    return speed_mps;
  }
  /* **************************************************************************** */
  public static void Cycle_New(Zep zep) {
    // http://www.airships.net/lz127-graf-zeppelin/size-speed
    double initial_air_density_si_units, air_virtual_density_si_units;// si units for density are kg/meters_cubed
    // 1 Newton is the force needed to accelerate 1 kilogram of mass at the rate of 1 metre per second squared.
    initial_air_density_si_units = density_sea_level * 0.001;// about 50,000 meters altitude https://en.wikipedia.org/wiki/Flight_altitude_record#Uncrewed_gas_balloon
    // https://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
    // 53.0 km
    initial_air_density_si_units = density_50km;// air density at 50,000 meters  http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
    initial_air_density_si_units = density_80km;

    if (true) {// feedback works best for this value
      initial_air_density_si_units = zep.get_air_density(SEO_speed_mps * 0.501, zep.thrust);
      System.out.print("feedback start density:" + initial_air_density_si_units);// feedback start density:7.354552372301305E-5
    }

    double orbit_fraction = 0;
    double terminal_speed_mps = 0;
    double weight_fraction_remaining = 1.0;
    air_virtual_density_si_units = initial_air_density_si_units;
    double centripedal_accel_mpss;
    int cnt = 0;
    while (orbit_fraction < 1.0) {
      if (cnt > 50) {
        break;
      }
      // The airship will always rise until it is neutrally buoyant. 
      // So it will always rise to a level of atmosphere the same density as itself. 
      air_virtual_density_si_units = weight_fraction_remaining * initial_air_density_si_units;
      System.out.println("air_virtual_density:" + air_virtual_density_si_units);
      terminal_speed_mps = zep.get_terminal_speed_mps(air_virtual_density_si_units);
      double speed = zep.get_terminal_speed_mps(air_virtual_density_si_units);
      System.out.println("speed/SEO:" + (speed / SEO_speed_mps));// speed:6498.226783737127
      System.out.println("terminal_V_mps:" + terminal_speed_mps + ", terminal_V_kps:" + terminal_speed_mps / 1000.0);
      centripedal_accel_mpss = centripetal_accel(terminal_speed_mps, earth_radius_meters);
      double net_gaccel_down = earth_grav_accel_mpss - centripedal_accel_mpss;// net accelleration down
      weight_fraction_remaining = net_gaccel_down / earth_grav_accel_mpss;// F = ma, so the force of weight scales exactly with accelleration if mass stays the same.
      orbit_fraction = terminal_speed_mps / SEO_speed_mps;
      System.out.println("centripedal_accel:" + centripedal_accel_mpss);
      System.out.println("cnt:" + cnt + "  orbit_fraction:" + orbit_fraction + "  weight_fraction_remaining:" + weight_fraction_remaining);// 0.00967752384837992 of orbit
      System.out.println();
      cnt++;
    }
  }
  /* **************************************************************************** */
  static void AirDrag() {//https://www.grc.nasa.gov/www/K-12/airplane/drageq.html
    double LEO_speed = 28080.0; // 28,080 km/h
    double Drag, coefficient, density, vel, VelSq, reference_area;
    double VelResult;
    coefficient = 1.0;
    reference_area = 1.0;// these cancel out so 1 should be fine.
    double CoRef = coefficient * reference_area;
    // 42.672 kilometers altitude
    // BU60-1 reached 53.0 km
    // https://en.wikipedia.org/wiki/Flight_altitude_record#Unmanned_gas_balloon
    // at 50km, density is 0.01027 / 12.25 = 0.00083836734 atmo http://www.engineeringtoolbox.com/standard-atmosphere-d_604.html
    // http://www.jpaerospace.com/atohandout.pdf
    density = 1.0;// atmospheres
    vel = 100;// kph, fastest airship
    vel = 140;// kph, fastest Hindenburg
    VelSq = vel * vel;
    Drag = coefficient * ((density * VelSq) / 2) * reference_area;// official formula
    Drag = coefficient * density * VelSq * 0.5 * reference_area;// official formula without parenthesis
    //1.0 = (coefficient * density * VelSq * 0.5 * reference_area) / Drag;// move drag to right
    //1.0 / VelSq = (coefficient * density * 0.5 * reference_area) / Drag;// move VelSq to left
    //VelSq = Drag / (coefficient * density * 0.5 * reference_area);// reciprocal
    {// solved for velocity
      density = 1.0 / 1000.0;// VelResult = 3162.2776601683795 kph = 1964.95 mph
      density = 1.0 / 306.0;// VelResult = 1749.28556845359 kph =1086.96 mph
      VelResult = Math.sqrt(Drag / (coefficient * density * 0.5 * reference_area));// square root
    }
    // F = C * D * (V*V) * 0.5 * A
    // solve for Velocity
    //Drag / VelSq = coefficient * density * 0.5 * reference_area;// divide both sides by VelSq
    //1 / VelSq = coefficient * density * 0.5 * reference_area * (1 / Drag);// divide both sides by Drag
    //VelSq = 1.0 / (coefficient * density * 0.5 * reference_area * (1 / Drag));// reciprocal of both sides
    density = 1.0 / 1000.0;// VelResult = 3162.2776601683795 kph = 1964.95 mph density = 1.0 / 306.0;// VelResult = 1749.28556845359 kph = 1086.96 mph
    VelResult = Math.sqrt(1.0 / (coefficient * density * 0.5 * reference_area * (1 / Drag)));// square root of both sides.
    System.out.println(VelResult);
    double Vel1, Vel0, density0, VelSq0, density1;
    density0 = 1.2754;//1.2754 kg/m3 at sea level
    Vel0 = 130000;// m/hr
    VelSq0 = Vel0 * Vel0;
    density1 = density0 / 306;// 2274.071238989667 km/hr
    density1 = density0 / 1000.0;// Vel1 = 4110.960958218893
    //density1 = density0 * 0.00083836734; // BU60-1 at 53.0 km, Vel1 = 4489.7915335727466, 4489.7915335727466 / 28080.0 = 0.15989286088 of orbit
    Vel1 = Math.sqrt((density0 * VelSq0) / density1);  // solve for Vel1
    //Vel1 = Math.sqrt(VelSq0 / density1);  // assuming density0 is 1.0
    System.out.println(Vel1); // Vel1 = 1749.28556845359 at 1/306 atmo
    // 3640157.258190404 m/hr = 3,640.157258190404 km/hr
    // 3162.2776601683795 / 28080 = 0.11261672578 or about 11 percent lighter at 1/1000 atmo
    // 1749.28556845359 / 28080 = 0.0622964946 or about 6 percent lighter at 1/306 atmo
    // VelResult = 2448.9997958350264 Hindenburg
    // VelResult = 4427.1887242357307 Hindenburg at 1/1000 atmo, 4427.1887242357307 / 28080 = 0.1576634161 orbit
  }
  /* **************************************************************************** */
  public static void Cycle_old() {
    /*
     first get thrust, then calc terminal V
     terminal velocity equation: https://en.wikipedia.org/wiki/Terminal_velocity
    terminal_V = sqrt(
     (2*mass*accel)
     /
     (air_density * area * drag_coeffient)
     )
 
     or terminal_V = sqrt( (2*force) / (density*area*drag_coeffient) );
     */
    double force, air_density, area, drag_coefficient;
    double number_of_engines = 10;
    // https://grantvillegazette.com/wp/article/publish-507/
    // Hindenburg 49,167 Newtons drag force at 84mph,  2,476 horsepower
    // 1 Newton is the force needed to accelerate 1 kilogram of mass at the rate of 1 metre per second squared.
    // 1.0000×10−6 m micro    0.000001
    // 500 microscopic tips
    force = 50.0 * 0.000001;// 50 micronewtons of force per chip http://newsoffice.mit.edu/2012/microthrusters-could-propel-small-satellites-0817
    force *= number_of_engines;// number of chips
    // 0.020~0.025              airship, blimp, dirigible, zeppelin http://physics.info/drag/
    // to 0.023 for the relatively large slender Los Angeles
    double drag_coefficient_la = 0.023;
    // los angeles diameter 27.6 m
    // akron diamter 40.5 m,
    double ship_radius = (27.6 / 2.0);//
    area = Math.PI * ship_radius * ship_radius;
    drag_coefficient = drag_coefficient_la;
    air_density = density_sea_level * 0.001;
    double orbit_fraction = 0;
    double terminal_V_kps = 0;
    double terminal_V_mps = 0;
    double weight_loss;
    int cnt = 0;
    while (orbit_fraction < 1.0) {
      terminal_V_mps = Math.sqrt((2.0 * force) / (air_density * area * drag_coefficient));
      terminal_V_kps = terminal_V_mps / 1000;
      System.out.println("terminal_V_kps:" + terminal_V_kps); // 0.07548468601736338 meters / second?
      double terminal_V_mph = terminal_V_mps * 3600.0;
      System.out.println("terminal_V_kph:" + terminal_V_mph);// meters per hour = 271.74486966250817
      //orbit_fraction = terminal_V_kps / LEO_speed_kps;
      orbit_fraction = terminal_V_mps / LEO_speed_mps;
      double weight_remaining = 1.0 - orbit_fraction;
      weight_remaining = Math.min(1.0, weight_remaining);
      System.out.println("cnt:" + cnt + "  orbit_fraction:" + orbit_fraction + "  weight_remaining:" + weight_remaining);// 0.00967752384837992 of orbit
      weight_loss = air_density * orbit_fraction;// this part is a guess. does weight reduce linearly with fraction of orbital speed?
      double centripedal = (terminal_V_mps * terminal_V_mps) / earth_radius_meters; // V squared over R
      double gaccel = earth_grav_accel_mpss - centripedal;
      air_density = gaccel * air_density;
      air_density -= weight_loss;
      // air_density *= (weight_remaining);
      // System.out.format("The square root of %d is %f.%n", i, r);
      cnt++;
    }
  }
  /* **************************************************************************** */
  public static void Cycle() {
    /*
     terminal velocity equation
     https://en.wikipedia.org/wiki/Terminal_velocity
     */
    // http://www.airships.net/lz127-graf-zeppelin/size-speed
    double force, initial_air_density, air_virtual_density, resistance_area, drag_coefficient;
    double number_of_engines;
    number_of_engines = 10.0 * 100000000.0;// 1 billion engines will work
    number_of_engines = 100;// realistic
    number_of_engines = 10000;// crazy
    // https://grantvillegazette.com/wp/article/publish-507/
    // Hindenburg 49,167 Newtons drag force at 84mph,  2,476 horsepower
    // 1 Newton is the force needed to accelerate 1 kilogram of mass at the rate of 1 metre per second squared.
    // 1.0000×10−6 m micro    0.000001
    // 500 microscopic tips
    force = 50.0 * 0.000001;// 50 micronewtons of force per chip http://newsoffice.mit.edu/2012/microthrusters-could-propel-small-satellites-0817
    force *= number_of_engines;// number of chips
    // 0.020~0.025              airship, blimp, dirigible, zeppelin http://physics.info/drag/
    // to 0.023 for the relatively large slender Los Angeles
    double drag_coefficient_la = 0.023;
    // los angeles diameter 27.6 m
    // akron diamter 40.5 m,
    double ship_diameter = 27.6;// US Los Angeles zeppelin
    double ship_radius = (ship_diameter / 2.0);// US Los Angeles zeppelin
    resistance_area = Math.PI * ship_radius * ship_radius;
    drag_coefficient = drag_coefficient_la;
    initial_air_density = density_sea_level * 0.001;// about 50,000 meters altitude
    //initial_air_density = density_sea_level * 0.00000000001;// this works with 100 engines
    double drag_factor = resistance_area * drag_coefficient;
    drag_factor = 0.00005;// crazy
    double orbit_fraction = 0;
    double terminal_V_mps = 0;
    double weight_fraction_lost = 0.0;
    double weight_fraction_remaining = 1.0;
    air_virtual_density = initial_air_density;
    double centripedal_accel;
    double Lift;
    int cnt = 0;
    while (orbit_fraction < 1.0) {
      if (cnt > 130) {
        break;
      }
      air_virtual_density = weight_fraction_remaining * initial_air_density;
      //air_virtual_density *= weight_fraction_remaining;// probably wrong
      System.out.println("air_virtual_density:" + air_virtual_density);
      terminal_V_mps = Math.sqrt((2.0 * force) / (air_virtual_density * drag_factor));
      // L = Cl * A * 0.5 * r * V^2 // lift
      // Lift = Lift_Coefficient * Area * 0.5 * air_density * Velocity^2;
      // double terminal_V_mph = terminal_V_mps * 3600.0;
      System.out.println("terminal_V_mps:" + terminal_V_mps);// meters per hour = 271.74486966250817
      centripedal_accel = (terminal_V_mps * terminal_V_mps) / earth_radius_meters; // V squared over R     
      double gaccel_down = earth_grav_accel_mpss - centripedal_accel;// net accelleration down
      weight_fraction_remaining = gaccel_down / earth_grav_accel_mpss;// F = ma, so the force of weight scales exactly with accelleration if mass stays the same
      // weight_fraction_remaining = 1.0 - weight_fraction_lost;
      orbit_fraction = terminal_V_mps / LEO_speed_mps;
      System.out.println("centripedal_accel:" + centripedal_accel);
      System.out.println("cnt:" + cnt + "  orbit_fraction:" + orbit_fraction + "  weight_fraction_remaining:" + weight_fraction_remaining);// 0.00967752384837992 of orbit
      System.out.println();
      cnt++;
    }
  }
}

/*
USS Los Angeles statistics:

    Maximum speed: 79 MPH.
    Cruising speed: 50 knotw.
    Original Powerplant: 5 Maybach VL-1 12-cylinder engines (400 HP at 1,4000 RPM)
    Flight Crew: 10 officers and 33 men.
    First flight: August 27, 1924.
    Final flight: June 24-25, 1932.
    Total flight hours: 4,181:28.
    Total flights: 331.

https://en.wikipedia.org/wiki/USS_Los_Angeles_(ZR-3)
 */
 /*
propeller
thrust:5363.240970468625 newtons
terminal_v_mps:24.72222222222222, speed:871.215293282441

jet thrust:44000.0 newtons  // Current jet planes produce thrusts up to 44 kN. // https://hypertextbook.com/facts/2000/KennethKwan.shtml
speed:2495.388524205181 mps with jet thrust

LEO_speed_mps:7800.0
871.215293282441 / 7800.0 = 0.11169426837  about 11 percent of orbital speed
7800.0 / 871.215293282441 = 8.95301088048  

jet
7800.0 / 2495.388524205181 = 3.12576575725
2495.388524205181 / 7800.0 = 0.31992160566 fraction of leo speed, about 32 percent of leo 

871.215293282441 / 343 = 2.53998627779, or mach 2.5 with propeller thrust 

Saturn V thrust 34.5 million newtons (7.6 million pounds) of thrust at launch
Rocketlab Electron 225 kN 225000 newtons 




Specific impulse, Tsiolkovsky rocket equation (to figure out 2nd stage rocket fuel savings)
m0 = mdry *e^(deltaV/exhaustV)
Electron rocket
Specific impulse 311 s (3.05 km/s)
311 s *  g(aka 9.80665 m/s2) = exhaust v
311 * 9.80665 = 3049.86815  meters per second exhaust velocity


air density temperature pressure thrust watts
http://msrc.sunysb.edu/~chang/atm205/Notes/Chapter_1_txtb.pdf
https://en.wikipedia.org/wiki/Density_altitude
https://www.rcgroups.com/forums/showthread.php?262733-Can-watts-be-converted-to-thrust
airship orbit


 */
