package expj;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author MultiTool
 */

/*
 Next steps

 Propagate vector mass from each cell to neighbors.
 On every iteration, each cell adds its energy flow vector to the carpet of vector masses. 
 Each cell reads the vector mass of its neighbors and averages them in its own vector mass. Or adds them? 

 Next 2D.  First prove or measure that evect from different angles on a 2D grid matches the actual direction waves are going. 

 Next: Set TRate based on interaction with VectorMassField. 

 Next: Print all the Cell stuff, clean it up, and apply the changes from Metric.
 */
public class Waves {
  public static final double TwoPi = Math.PI * 2.0;
  public static final int NDimsVector = 2;
//  public static final int NDims = 1;
  public static final int NDims = 2;
  public static final int NAxis = 2;
//  public static final double GravConst = 6.67408e-11;// 6.67408(31) * 10−11;// 0.00001;// 1.0;// 
  public static final double GravConst = 1.0;// 6.67408(31) * 10−11;// 0.00001;// 1.0;// 
  public static final double TRate_Standard = 0.1;
//  public static final double TRate_Standard = 0.01;

  public int GridWidth = 2;// 6;
  public int GridHeight = 2;// 6;
  public int NumCells = GridWidth * GridHeight;
  public double SourceHeight = 2.0;// 10.0;
//  public int NumCells = 4;//40;// 1D.
  public static final boolean UseInc = false;
//  public static final boolean UseInc = true;
  enum DisplayMode {
    Graphic, Text
  };
//  public static DisplayMode Mode = DisplayMode.Text;

  public static DisplayMode Mode = DisplayMode.Graphic;
  public ArrayList<Cell> Grid = new ArrayList<Cell>();
  int GenCnt = 0;
  /* ********************************************************************************* */
  public Waves() {
    int GridSize = 2;// 60;// 30;
    if (NDims == 2) {// Square grids
//      GridSize = 60;// 30;
//      GridSize = 32;// 30
//      GridSize = 16;// 30
//      GridSize = 8;
//      GridSize = 6;
//      GridSize = 5;
//      GridSize = 4;
//      GridSize = 3;
      GridSize = 2;
    }
    this.GridWidth = GridSize;
    this.GridHeight = GridSize;
    if (true) {
//      this.GridWidth = 2;
      this.GridWidth = 3;
//      this.GridWidth = 4;
//      this.GridWidth = 5;
      this.GridHeight = 1;
//      this.GridHeight = 2;
    }
    this.NumCells = this.GridWidth * this.GridHeight;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = new Cell();
      this.AddCell(cell);
    }
    if (NDims == 1) {
      Connect1D();
    } else if (NDims == 2) {
      Connect2D();
    }
//    cell = this.Grid.get(0);
//    cell.Height = 10.0;
//    for (int cnt = 0; cnt < this.GridWidth / 4; cnt++) { 
//      cell = this.Grid.get(cnt);
//      cell.Height = 10.0;
//    }
//    System.out.println();
  }
  /* ********************************************************************************* */
  public void AddCell(Cell cell) {
    cell.MyIdNum = this.Grid.size();
    this.Grid.add(cell);
  }
  /* ********************************************************************************* */
  public void DampRectangle(int MinX, int MinY, int MaxX, int MaxY, double Value) {
    int LastY = MaxY - 1, LastX = MaxX - 1;// mins are inclusive, maxes are exclusive
    Cell cell;
    // Fill top and bottom lines of rectangle.
    for (int ColCnt = MinX + 1; ColCnt < LastX; ColCnt++) {// Bottom row.
      cell = this.GetXY(ColCnt, MinY);
      cell.Set_Damp(Value);
    }
    for (int ColCnt = MinX + 1; ColCnt < LastX; ColCnt++) {// Top row.
      cell = this.GetXY(ColCnt, LastY);
      cell.Set_Damp(Value);
    }
    // Fill vertical left and right sides of rectangle.
    for (int RowCnt = MinY + 1; RowCnt < LastY; RowCnt++) {
      cell = this.GetXY(MinX, RowCnt);// left vertical side
      cell.Set_Damp(Value);
      cell = this.GetXY(LastX, RowCnt);// right vertical side
      cell.Set_Damp(Value);
    }
  }
  /* ********************************************************************************* */
  public void DampFrame(int NumFrames) {// Make a frame around the grid to absorb waves so they don't reflect back.
    int NumCols = this.GridWidth;
    int NumRows = this.GridHeight;
    int MinX = 0, MinY = 0, MaxX = NumCols, MaxY = NumRows;
//    int NumFrames = 4;// Absorbent border is NumFrames thick.
//    Other values: NumFrames = 10; or 24;20;
    double Value, FractAlong, FractFrom, StartingValue = 0.0;//10.0;
    for (int FrameCnt = 0; FrameCnt < NumFrames; FrameCnt++) {
      FractAlong = ((double) FrameCnt + StartingValue) / ((double) NumFrames + StartingValue);
      FractFrom = 1.0 - FractAlong;
      Value = 1.0 - Math.pow(FractFrom, 3.0);
      DampRectangle(MinX, MinY, MaxX, MaxY, Value);
      MinX++;// rectangle gets smaller each FrameCnt
      MinY++;
      MaxX--;
      MaxY--;
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public Cell GetXY(int XLoc, int YLoc) {
    Cell cell = this.Grid.get((YLoc * GridWidth) + XLoc);
    return cell;
  }
  /* ********************************************************************************* */
  public void Connect1D() {// Loop
    int LastPlace = NumCells - 1;
    Cell cell;
    Cell prev = this.Grid.get(LastPlace);
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      prev.NbrCross[0][1] = cell;
      cell.NbrCross[0][0] = prev;
      prev = cell;
    }
    // Seed the grid with a displaced area.
//    this.SourceHeight = 5.0;// 10.0;
    cell = this.Grid.get(0);
    cell.Height = this.SourceHeight;
//    cell.VSpeed = cell.VSpeed_Next = -0.662644757;
    cell.TRate_Prev = TRate_Standard;
    cell = this.Grid.get(1);
    cell.Height = -this.SourceHeight;
//    cell.VSpeed = cell.VSpeed_Next = -0.667475999999998;
    cell.TRate_Prev = TRate_Standard;
  }
  /* ********************************************************************************* */
  public void Connect2D() {// Torus 
    double CellSize = 20;
    CellSize = 15;
    CellSize = 50;
    Cell CellLeft, CellBelow, CellCenter;
    int XPrev, YPrev;
    YPrev = GridHeight - 1;
    for (int YCnt = 0; YCnt < GridHeight; YCnt++) {
      XPrev = GridWidth - 1;
      for (int XCnt = 0; XCnt < GridWidth; XCnt++) {
        CellCenter = this.GetXY(XCnt, YCnt);
        CellLeft = this.GetXY(XPrev, YCnt);
        CellBelow = this.GetXY(XCnt, YPrev);
        CellCenter.ConnectPrevNbr(CellLeft, 0);
        CellCenter.ConnectPrevNbr(CellBelow, 1);
        CellCenter.AssignGraphics(XCnt * CellSize, YCnt * CellSize, CellSize, CellSize);
//        CellCenter.TRate_Next = CellCenter.TRate = (YCnt + 0.00001) / (GridHeight + 0.00001);
        CellCenter.TRate_Prev = CellCenter.TRate = CellCenter.TRate_Next = TRate_Standard;
        XPrev = XCnt;
      }
      YPrev = YCnt;
    }

    if (false) {
      DampFrame(4);// Damp borders.
    }

    // Seed the grid with a displaced area.
    Cell cell;
    this.SourceHeight = 1.0;// 10.0;
//    this.SourceHeight = 2.0;// 10.0;
//    this.SourceHeight = 10.0;// 10.0;
    if (true) {
//      SourceHeight = 0.01;
      {
        cell = this.Grid.get(0);
        cell.Height = this.SourceHeight;
//        cell.TRate_Prev = TRate_Standard;
//        cell.TRate = TRate_Standard;
      }
      if (false) {
        cell = this.Grid.get(1);
        cell.Height = -10.5;//this.SourceHeight;
//        cell.TRate_Prev = TRate_Standard;
//        cell.TRate = TRate_Standard;
      }
      if (false) {
        cell = this.Grid.get(1);
        cell.Height = -this.SourceHeight;
        cell.TRate_Prev = TRate_Standard;
        cell.TRate = TRate_Standard;
      }
    } else {
      int SourceRadius = 1;
      int GridHalfHeight = this.GridHeight / 2;
      int GridHalfWidth = this.GridWidth / 2;
      for (int YCnt = GridHalfHeight - SourceRadius; YCnt < GridHalfHeight + SourceRadius; YCnt++) {
        for (int XCnt = GridHalfWidth - SourceRadius; XCnt < GridHalfWidth + SourceRadius; XCnt++) {
          cell = this.GetXY(XCnt, YCnt);
          cell.Height = this.SourceHeight;
        }
      }
    }
  }
  /* ********************************************************************************* */
  public void RunMe() {
    Cell cell;
    SetGlobalTRate(0.2);
    SetGlobalTRate_Next(0.2);
    this.Print_Header();
    System.out.print("ETot, ");

    System.out.print("VspdSq, ");
    System.out.print("TensionSq, ");

    System.out.println();
    if (true) {// Print starting state
      this.Print_Me();
      System.out.print("" + 0.0 + ", ");
      System.out.print("" + 0.0 + ", ");
      System.out.print("" + 0.0 + ", ");
      System.out.println();
    }
    if (false) {
      Cell cell0, cell1;
      cell0 = this.Grid.get(0);
      cell1 = this.Grid.get(1);

      cell0.TRate = TRate_Standard;
      cell1.TRate = TRate_Standard;
    }

    for (int tcnt = 0; tcnt < 50; tcnt++) {// 2000000 // 1000 // 100
      this.RunCycle();
    }
  }
  /* ********************************************************************************* */
  public void SetGlobalTRate_Next(double TRate_Next) {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.TRate_Next = TRate_Next;
    }
  }
  /* ********************************************************************************* */
  public void SetGlobalTRate(double TRate) {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.TRate_Prev = TRate;
      cell.TRate = TRate;
    }
  }
  /* ********************************************************************************* */
  public void RunCycle() {
    if (false) {
      if (this.GenCnt > 1000) {
        return;
      }
    }
    Cell cell;

    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Update();
    }
    double ETot;
    ETot = GetTotalEnergy();

    double VspdSq = this.GetTotalVspdSq();
    double TensionSq = this.GetTotalTensionSq();
//    ETot = VspdSq + TensionSq;

    // Error is VspdSq / TensionSq = 3.04636465349 for all starter values at TRate=0.1. At index 49. 
    // 0.081538626104734 / 0.315048104947813  = 0.25881325684 for starter value 1 at TRate=0.01. 
    // 0.464950631748337 / 0.035988728279947 = 12.9193404149 for starter value 1 at TRate=0.1. 
    // peak to peak 0.500873439828757 / 0.375548287253282 = 1.33371248606
    // peak to peak
    // 0.500164495239933 / 0.375174355092595 = 1.33315214233 at trate 0.1?
    // 0.503325486468323 / 0.377009301363002 = 1.33504792759 at trate 0.2?
    // 
    System.out.print("" + this.GenCnt + ", ");
    this.Print_Me();
    System.out.print("" + ETot + ", ");
    System.out.print("" + VspdSq + ", ");
    System.out.print("" + TensionSq + ", ");

    double AvgHeight = this.GetAvgHeight();
//    if (AvgHeight > this.SourceHeight * 1.000001) {//
//    if (AvgHeight > this.SourceHeight / (double) (this.NumCells + 1.0)) {//
//    System.out.println("AvgHeight:" + AvgHeight);
    System.out.println();
//    if (AvgHeight > 0.2 + this.SourceHeight / (double) NumCells) {// always at GenCnt == 32.
//      noop();
//    }
    if (AvgHeight > this.SourceHeight) {// 0.6666666666469995
      noop();
      // AvgHeight:1.1113410065438931 
    }
    if (false) {// change TRate_Next here
      // this.VSpeed_Next = (Tension * TRateSq) + (VSpeed * TRatio);
//      Cell cell0;
//      cell0 = this.Grid.get(0);
//      if ((this.GenCnt / 33) % 2 == 0) {
//      if ((this.GenCnt / 11) % 2 == 0) {
//      if ((this.GenCnt >= 33)) {
//        cell0.TRate_Next = 0.2;// fast time
//      } else {// if X is pointed low
//        cell0.TRate_Next = 0.1;// slow time
//      
      if ((this.GenCnt >= 25)) {
//      if ((this.GenCnt >= 33)) {
//        cell0.TRate_Next = 0.1;// slow time
        SetGlobalTRate_Next(0.1);
      } else {// if X is pointed low
//        cell0.TRate_Next = 0.2;// fast time
        SetGlobalTRate_Next(0.2);
      }
    }

    // to do: detect transitions through hgt==0 and increase trate.
    // and detect transistions where vspeed == 0 and decrease trate.
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Rollover();
    }

    if (false) {
      Cell cell0, cell1, cell2;
      cell0 = this.Grid.get(0);
      cell1 = this.Grid.get(1);
      cell2 = this.Grid.get(2);
      cell0.Print_Me();
      cell1.Print_Me();
      cell2.Print_Me();
      System.out.println();
    }
    this.GenCnt++;
  }
  /* ********************************************************************************* */
  public double GetTotalEnergy() {// I have NO idea if this will work.
    double Energy = 0;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      Energy += cell.GetTotalEnergy();
    }
    return Energy;
  }

  /* ********************************************************************************* */
  public double GetTotalVspdSq() {
    double VspdSqTotal = 0;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      VspdSqTotal += cell.GetTotalVspdSq();
    }
    return VspdSqTotal;
  }

  /* ********************************************************************************* */
  public double GetTotalTensionSq() {
    double TensionSqTotal = 0;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
//      TensionSqTotal += cell.GetTotalTensionSq();
      TensionSqTotal += cell.GetTotalTensionSq_Classic();
    }
    return TensionSqTotal;
  }

  /* ********************************************************************************* */
  public void Print_Header() {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Print_Header();
    }
//    System.out.println();
  }
  /* ********************************************************************************* */
  public void Print_Me() {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Print_Me();
    }
  }
  /* ********************************************************************************* */
  public void Draw_Me(Gui.DrawingContext ParentDC) {// IDrawable

    Font currentFont = ParentDC.gr.getFont();
    Font newFont = currentFont.deriveFont(currentFont.getSize() * 0.75F);
    ParentDC.gr.setFont(newFont);

    int NumCells = this.Grid.size();
    for (int cnt = 0; cnt < NumCells; cnt++) {
      this.Grid.get(cnt).Draw_Me(ParentDC);
    }
  }
  /* ********************************************************************************* */
  public double GetAvgHeight() {
    double AvgHeight = 0;
    Cell cell;
    for (int cnt = 0; cnt < this.Grid.size(); cnt++) {
      cell = this.Grid.get(cnt);
      AvgHeight += Math.abs(cell.Height_Next);
    }
    AvgHeight /= this.Grid.size();
    return AvgHeight;
  }
  /* ********************************************************************************* */
  public static class SineCell extends Cell {
    /* ********************************************************************************* */
    @Override
    public void Update() {// Calculate next time step.
      double FractAngle = (double) (this.CellGenCnt % 100) / 100.0;
      FractAngle *= this.TRate;
      double Angle = FractAngle * TwoPi;
      this.Height_Next = Math.sin(Angle);
      this.VSpeed_Next = this.Height_Next - this.Height;
      this.CellGenCnt++;
    }
  }

  /* ********************************************************************************* */
  public static class Cell {
    public double Height_Prev, Height, Height_Next;// Height.
    public double VSpeed_Prev, VSpeed, VSpeed_Next;// Vertical Speed.
    public double Tension, Tension_Next;
    public double Energy, Energy_Next;
    protected double Damp = 1.0; //0.99;//0.5;//0.95; //
//    public double TRate = 0.5;
//    public double TRate = 0.1;
    public double TRate_Prev, TRate = 0.001, TRate_Next;
    public Cell[][] NbrCross;
    public double[][] NbrTransfer;
    public int NumNbrs;
//    public double[] EVect;
    public Vector EVect;
    public Vector VectorMassField;
    public double XOrg, YOrg;// location to draw in grid.  Indexes or pixel coords? 
    public double Wdt, Hgt;// size of box to draw in grid.
    public Vector ScratchVect0 = new Vector(), ScratchVect1 = new Vector();// Reuse these temporary variables to avoid beating on heap memory.
    public int CellGenCnt = 0;
    public double PeakCnt = 0;
    public int MyIdNum = 0;
    /* ********************************************************************************* */
    public Cell() {
      //this.Height = Math.random() * 2.0 - 1.0;
      this.Init_NbrCross();
      this.NumNbrs = NDims * NAxis;
      this.TRate = this.TRate_Next = TRate_Standard;
    }
    /* ********************************************************************************* */
    public void Init_NbrCross() {
      this.EVect = new Vector();
      this.VectorMassField = new Vector();// double[NDims];
      this.NbrTransfer = new double[NDims][NAxis];
      this.NbrCross = new Cell[NDims][NAxis];
      for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
        for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
          this.NbrCross[DimCnt][AxCnt] = null;
          this.NbrTransfer[DimCnt][AxCnt] = 0.0;
          this.VectorMassField.Loc[DimCnt] = 0.0;
        }
      }
    }
    /* ********************************************************************************* */
    public void ConnectPrevNbr(Cell other, int Dimension) {// Connect me to a neighbor who has a lower index in the given dimension.
      this.NbrCross[Dimension][0] = other;// Connect my down pointer to neighbor.
      other.NbrCross[Dimension][1] = this;// Connect neighbor's up pointer to me.
    }
    /* ********************************************************************************* */
    public void Update() {// Calculate next time step.
//      AtomicInteger counter = new AtomicInteger();// Possible pass by reference for integer.
      Basis basis = new Basis();
      this.Calc_Basis(basis);
//      this.Calc_Height_Next(basis.HgtAvg, basis.VelAvg);
      this.Calc_Height_Next(basis.HgtAvg, 0.0);// or the old way.
      this.Calc_Energy_Vector();

//      this.Calc_Height_Next(Basis);
//      this.Calc_Energy_Vector();
      if (false) {// Disable all field interaction.
        ScratchVect0.Copy_From(this.EVect);
        if (false) {
          if (NDims == 2) {
            this.VectorMassField.Assign(0.1, 0.0);
          } else {//if (NDims == 2) {
            this.VectorMassField.Assign(0.1);
          }
          ScratchVect1.Copy_From(this.VectorMassField);
          double FrequencyFactor = this.VectorMassField.GetMagnitude();// Magnitude of neighborhood energy vector.
          FrequencyFactor *= DopplerFrequencyFactor(ScratchVect0, ScratchVect1);
          // next use FrequencyFactor * your magnitude to get your mass.
          // then use lorentz to get time slowing from your mass field.
//            this.TRate = this.TRate_Next = GetLorentzTimeFactor(FrequencyFactor);
//            this.TRate = GetLorentzTimeFactor(FrequencyFactor);
          this.TRate_Next = GetLorentzTimeFactor(FrequencyFactor);
        } else {
          int CheckDim = 0;
          if (ScratchVect0.Loc[CheckDim] > 0) {// if X is pointed high
            this.TRate_Next = 0.2;// fast time
          } else {// if X is pointed low
            this.TRate_Next = 0.1;// slow time
          }
        }
      }
      this.CellGenCnt++;
    }
    /* ********************************************************************************* */
    public void Rollover() {// set up for next cycle
      this.Height_Prev = this.Height;
      this.Height = this.Height_Next;
//      this.Tension = this.Tension_Next;

      this.VSpeed_Prev = this.VSpeed;
      this.VSpeed = this.VSpeed_Next;

      this.TRate_Prev = this.TRate;
      this.TRate = this.TRate_Next;
    }
    /* ********************************************************************************* */
    public boolean HeightPeak() {
      if (this.Height_Prev < Height && Height > this.Height_Next) {// crest
        return true;
      }
      if (this.Height_Prev > Height && Height < this.Height_Next) {// trough
//        return true;
      }
      return false;
    }
    /* ********************************************************************************* */
    public boolean VSpeedPeak() {
      if (this.VSpeed_Prev < VSpeed && VSpeed > this.VSpeed_Next) {// crest
        return true;
      }
      if (this.VSpeed_Prev > VSpeed && VSpeed < this.VSpeed_Next) {// trough
        return true;
      }
      return false;
    }
    /* ********************************************************************************* */
    public double GetTotalVspdSq_Wrong() {// Interp 
//      double VSpdScaled = VsInterp / this.TRate;// interp disabled for now.
      double VSpeedSq = this.VSpeed_Next * this.VSpeed_Next;
      double VSpdScaled = VSpeedSq / this.TRate;

//      VSpeedSq *= 1.5 / 2.0;
      return VSpdScaled;
    }
    /* ********************************************************************************* */
    public double GetTotalVspdSq() {
      double VsInterp = (this.VSpeed + this.VSpeed_Next) * 0.5;// Interp average
//      double VSpdScaled = VsInterp / this.TRate;// interp disabled for now.
      double VSpdScaled = this.VSpeed_Next / this.TRate;// snox was TRate
//      double VSpdScaled = this.VSpeed / this.TRate;// snox was VSpeed_Next 
//      double VSpeedSq = VSpdScaled * VSpdScaled;
//      VSpeedSq *= 1.5 / 2.0;
      double VSpeedSq = this.VSpeed * this.VSpeed;
      return VSpeedSq;
    }
    /* ********************************************************************************* */
    public double GetTotalVspdSq_InterpSq() {// not as good
      double VSpdScaled = this.VSpeed / this.TRate;
      double VSpeedSq = VSpdScaled * VSpdScaled;

      double VSpdScaled_Next = this.VSpeed_Next / this.TRate;
      double VSpeedSq_Next = VSpdScaled_Next * VSpdScaled_Next;

      double VsInterp = (VSpeedSq + VSpeedSq_Next) * 0.5;// average

      return VsInterp;
    }
    /* ********************************************************************************* */
    public double GetTotalTensionSq() {
      Cell nbr;
      double Delta, SumSq = 0.0;
      for (int dcnt = 0; dcnt < NDims; dcnt++) {
        for (int axcnt = 0; axcnt < NAxis; axcnt++) {
          nbr = NbrCross[dcnt][axcnt];
          Delta = nbr.Height - this.Height;
          SumSq += Delta * Delta;
        }
      }
      double TensionSq = SumSq / (double) this.NumNbrs;
      TensionSq *= 0.5; // Is this to split the tension with the neighbor?
      return TensionSq;
    }
    /* ********************************************************************************* */
    public double GetTotalTensionSq_Classic() {// 2/1.5 = 1.33333333333 is the correction we would need. 
//      double TensionScaled = this.Tension / (this.TRate * this.TRate);// Tension comes out way too big.
//      double TensionScaled = this.Tension / this.TRate;
//      double TensionSq = TensionScaled * TensionScaled;
      double TensionSq = this.Tension * this.Tension;
//      TensionSq *= 1.33371248606;
      return TensionSq;
    }
    /* ********************************************************************************* */
    public double GetTotalEnergy_Transfer() {// I have NO idea if this will work.
      double Total = 0.0;
      for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
        Total += Math.abs(this.NbrTransfer[DimCnt][0]);// Low index neighbor.
        Total += Math.abs(this.NbrTransfer[DimCnt][1]);// High index neighbor.
      }
      return Total;
    }
    /* ********************************************************************************* */
    public double GetTotalEnergy() {// I have NO idea if this will work.  GetTotalEnergy_Best
      if (true) {
        double VSpeedSq;
        if (UseInc) {
          double VSpeedAdj = this.VSpeed / this.TRate;
          VSpeedSq = VSpeedAdj * VSpeedAdj;
//          VSpeedSq = this.VSpeed * this.VSpeed;
        } else {
          VSpeedSq = this.VSpeed * this.VSpeed;
        }
        double TensionSq = this.Tension * this.Tension;
        double Energy = VSpeedSq + TensionSq;
        return Energy;
      } else {
        double VsInterp = (this.VSpeed + this.VSpeed_Next) * 0.5;// average
        double ScaledVSpeed = this.VSpeed_Next / this.TRate;
        double VSpeedSq = ScaledVSpeed * ScaledVSpeed;
        double TensionSq = this.Tension * this.Tension;
        TensionSq *= 0.5;// Share tension with each neighbor? 
        double Energy = VSpeedSq + TensionSq;
        return Energy;
      }
    }
    /* ********************************************************************************* */
    public double GetTotalEnergy2() {// I have NO idea if this will work.
      double ScaledVSpeed = this.VSpeed_Next / this.TRate;
      double VSpeedSq = ScaledVSpeed * ScaledVSpeed;
      double TensionSq = this.Tension * this.Tension * 0.5 * 0.5;
//      TensionSq *= 0.5;// Share tension with each neighbor? 
//      TensionSq *= 2.0 / 1.5;
      double Energy = VSpeedSq + TensionSq;
      return Energy;
    }
    /* ********************************************************************************* */
    public double GetTotalEnergy1() {// I have NO idea if this will work.
      double ScaledVSpeed = this.VSpeed_Next / this.TRate;
      double VSpeedSq = ScaledVSpeed;// * ScaledVSpeed;
      double TensionSq = this.Tension * this.Tension;
      TensionSq *= 0.5;// Share tension with each neighbor? 
      double Energy = VSpeedSq + TensionSq;
      return Energy;
    }
    /* ********************************************************************************* */
    public double GetTotalEnergy0() {// I have NO idea if this will work.
      double ScaledVSpeed = this.VSpeed_Next / this.TRate;
      double VSpeedSq = ScaledVSpeed * ScaledVSpeed;
      double TensionSq = this.Tension * this.Tension;
      TensionSq *= 0.5;// Share tension with each neighbor? 
      double Energy = VSpeedSq + TensionSq;
      return Energy;
    }
    /* ********************************************************************************* */
    public void Print_Header() {
      System.out.print("Height_Next, ");
      System.out.print("Tension, ");
      System.out.print("TRate_Next, ");
      System.out.print("VSpeed_Next, ");
    }
    /* ********************************************************************************* */
    public void Print_Me() {
      String rez = "%6.3e";
      rez = "%1.3f";
//      System.out.print("" + this.Height + ", ");
//      System.out.print(String.format(rez, this.Height_Next) + ", ");
//      System.out.print(String.format(rez, this.Tension) + ", ");
//      System.out.print(String.format(rez, this.TRate_Next) + ", ");
//      System.out.print(String.format(rez, this.VSpeed_Next) + ", ");

      System.out.print(" ");
      System.out.print(String.format(rez, this.Height) + ", ");
      System.out.print(String.format(rez, this.Tension) + ", ");
      System.out.print(String.format(rez, this.VSpeed) + ", ");
      System.out.print(String.format(rez, this.Energy) + ", ");
//      System.out.print(String.format(rez, this.VSpeed * this.VSpeed) + ", ");
//String txt = String.format("%1.3f\n%1.3f\n%1.3f\n%1.3f", this.Height, this.Tension, this.VSpeed, this.Energy);
      System.out.print("");
    }
    /* ********************************************************************************* */
    public static void DrawString(Gui.DrawingContext ParentDC, String text, int x, int y) {
      Graphics2D gr = ParentDC.gr;// https://stackoverflow.com/questions/4413132/problems-with-newline-in-graphics2d-drawstring
      for (String line : text.split("\n")) {
        gr.drawString(line, x, y += gr.getFontMetrics().getHeight());
      }
    }
    /* ********************************************************************************* */
    public void Print_Me_All() {
      double Stretch = 10;
//      System.out.print("(" + NbrTransfer[0][0] + ", " + this.Height + ", " + NbrTransfer[0][1] + "), ");//  print NbrTransfer[0], my amp, NbrTransfer[1].
      System.out.print("" + NbrTransfer[0][0] * Stretch + ",  " + NbrTransfer[0][1] * Stretch + ", ");
      System.out.print("" + this.EVect.Loc[0] * Stretch + ", ");
    }
    /* ********************************************************************************* */
    public void Draw_Me(Gui.DrawingContext ParentDC) {// IDrawable
      if (this.TRate <= 0.0) {
        ParentDC.gr.setColor(Color.BLACK);
      } else {
        double what = this.Height * 40;// / 8.0;// + 2; // Arbitrary magic number.
        Color col = Waves.ToNegPos(what);
        ParentDC.gr.setColor(col);
      }
      if (Double.isNaN(this.Height)) {
        noop();
      }
      // Draw cell background.
      ParentDC.gr.fillRect((int) this.XOrg, (int) this.YOrg, (int) this.Wdt, (int) this.Hgt);
      ParentDC.gr.setColor(Color.black);// Cell outline.
      ParentDC.gr.drawRect((int) this.XOrg, (int) this.YOrg, (int) this.Wdt, (int) this.Hgt);

      // Draw the energy vector.
      double EFactor = 10000;
      EFactor = 100;
      double HalfWdt = this.Wdt * 0.5;
      double HalfHgt = this.Hgt * 0.5;
      double XCtr = this.XOrg + HalfWdt;
      double YCtr = this.YOrg + HalfHgt;
      double DialX = XCtr + (this.EVect.Loc[0] * EFactor);
      double DialY = YCtr + (this.EVect.Loc[1] * EFactor);
      if (DialX < 1) {
        noop();
      }
      ParentDC.gr.setColor(Color.black);
      ParentDC.gr.drawLine((int) XCtr, (int) YCtr + 2, (int) DialX, (int) DialY + 2);
      ParentDC.gr.setColor(Color.white);
      ParentDC.gr.drawLine((int) XCtr, (int) YCtr, (int) DialX, (int) DialY);

      ParentDC.gr.setColor(Color.white);
      String txt = String.format("%1.3f\n%1.3f\n%1.3f\n%1.3f", this.Height, this.Tension, this.VSpeed, this.Energy);
//      String txt = String.format("%1.3f", this.Height);
//      String txt = String.format("%1.3f", this.Energy);
//      ParentDC.gr.drawString(txt, (int) this.XOrg, (int) YCtr);
      DrawString(ParentDC, txt, (int) this.XOrg, (int) this.YOrg);
    }
    /* ********************************************************************************* */
    public void AssignGraphics(double XOrg, double YOrg, double Wdt, double Hgt) {
      this.XOrg = XOrg;
      this.YOrg = YOrg;
      this.Wdt = Wdt;
      this.Hgt = Hgt;
    }
    /* ********************************************************************************* */
    public void Set_Damp(double Value) {
      this.Damp = Value;
    }
    /* ********************************************************************************* */
    public double Calc_Energy(double Tension0, double VSpeed0) {
      double TensionSq = Tension0 * Tension0;
      double VSpeedSq = VSpeed0 * VSpeed0;
      return VSpeedSq + TensionSq;
    }
    /* ********************************************************************************* */
    public void Calc_Energy_Vector() {// Calculate energy vector.
      Cell nbr;
      double Flow;
      for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
        this.EVect.Loc[DimCnt] = 0.0;
        for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
          nbr = this.NbrCross[DimCnt][AxCnt];
          Flow = this.Get_Energy_Flow(nbr);
          this.NbrTransfer[DimCnt][AxCnt] = Flow;
        }
        Flow = this.NbrTransfer[DimCnt][0];// Low index neighbor.
        this.EVect.Loc[DimCnt] += -Flow;// Reverse the sign for opposite side.
        Flow = this.NbrTransfer[DimCnt][1];// High index neighbor.
        this.EVect.Loc[DimCnt] += Flow;
        this.EVect.Loc[DimCnt] *= 0.5;// take average
      }
    }
    /* ********************************************************************************* */
    public static void Rotate(double XLoc, double YLoc, double Angle) {
      // Rotation matrix
      double Cos = Math.cos(Angle);
      double Sin = Math.sin(Angle);
      double XLoc_Next = (XLoc * Cos) - (YLoc * Sin);
      double YLoc_Next = (XLoc * Sin) + (YLoc * Cos);
    }
    /* ********************************************************************************* */
    public void Calc_Height_Next(double OtherHgt, double OtherVel) {// best trig version. 
      if (UseInc) {
        Calc_Height_Next_Inc(OtherHgt);
      } else {// Rotate matrix
        double Displacement, DeltaV;
        Displacement = this.Height - OtherHgt;
        this.Tension = -Displacement;// Spring force towards neighbors' height.
        DeltaV = this.VSpeed - OtherVel;// Velocity is relative to neighbors' velocity. DOES NOT WORK. 

        this.Energy = this.Calc_Energy(Displacement, DeltaV);

        // Rotation matrix
        double Cos = Math.cos(this.TRate);
        double Sin = Math.sin(this.TRate);
        this.Height_Next = (Displacement * Cos) - (DeltaV * Sin) + OtherHgt;
        this.VSpeed_Next = (Displacement * Sin) + (DeltaV * Cos) + OtherVel;

//        this.VSpeed_Next = (DeltaV * Cos) - (Displacement * Sin) + OtherHgt;
//        this.Height_Next = (DeltaV * Sin) + (Displacement * Cos) + OtherVel;
//        this.VSpeed_Next *= this.Damp;// Is this the right place to apply Damp?
//        this.Tension_Next = this.Height_Next - OtherHgt;
        this.Energy_Next = this.Calc_Energy(this.Height_Next - OtherHgt, this.VSpeed_Next - DeltaV);
      }
      if (this.MyIdNum == 0) {
        noop();
      }
    }
    /* ********************************************************************************* */
    public void Calc_Height_Next_Trig(double OtherHgt) {// best trig version. 
      double Displacement;
//      this.Tension = OtherHeight - this.Height;// Spring force towards neighbor.
      if (UseInc) {
        Calc_Height_Next_Inc(OtherHgt);
      } else {// Rotate matrix
        Displacement = this.Height - OtherHgt;
        this.Tension = -Displacement;

        this.Energy = this.Calc_Energy(this.Tension, this.VSpeed);

        double Cos = Math.cos(this.TRate);
        double Sin = Math.sin(this.TRate);
        this.Height_Next = (Displacement * Cos) - (this.VSpeed * Sin) + OtherHgt;
        this.VSpeed_Next = (Displacement * Sin) + (this.VSpeed * Cos);

//        this.VSpeed_Next *= this.Damp;// Is this the right place to apply Damp?
//        this.Tension_Next = this.Height_Next - OtherHeight;
        this.Energy_Next = this.Calc_Energy(this.Height_Next - OtherHgt, this.VSpeed_Next);
        if (this.MyIdNum == 0) {
          noop();
        }
      }
    }
    /* ********************************************************************************* */
    public void Calc_Height_Next_Inc(double OtherHgt) {// best increment version cleaned up.
      this.Tension = OtherHgt - this.Height;// Spring force towards neighbor.
      this.Tension_Next = OtherHgt - this.Height;// Spring force towards neighbor.
      double TRateSq = this.TRate * this.TRate;
      double TRatio = this.TRate / this.TRate_Prev;
      this.VSpeed_Next = (this.Tension * TRateSq) + (this.VSpeed * TRatio);// best
      this.VSpeed_Next *= this.Damp;// Is this the right place to apply Damp?
      this.Height_Next = this.Height + this.VSpeed_Next;

      this.Energy = this.Calc_Energy(this.Tension, this.VSpeed);
      this.Energy_Next = this.Calc_Energy(this.Height_Next - OtherHgt, this.VSpeed_Next);
    }

    /* ********************************************************************************* */
    public void Calc_Amp(double OtherAmp) {// From HotWaves. Calc_Amp and Rollover from bench testing.  this works.  For reference, not used here.
      double TRateSquared = this.TRate * this.TRate;
      double Delta = OtherAmp - this.Height;// Spring force
      Delta *= TRateSquared;
      this.Height_Next = this.Height + this.VSpeed;
      this.Height_Next = this.Height_Next + Delta;
      this.VSpeed_Next = this.Height_Next - this.Height;
      this.VSpeed_Next *= this.Damp;
    }
    /* ********************************************************************************* */
    public static class Basis {
      public double HgtAvg, VelAvg;
    }

    /* ********************************************************************************* */
    public void Calc_Basis(Basis basis) {// Basis is just average of all neighbors. My height and speed are attracted to this value.
      double HgtSum = 0, VelSum = 0, NNbrs = 0;
      basis.HgtAvg = basis.VelAvg = 0.0;
      Cell nbr;// 4 neighbors in 2 dimensions
      for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
        for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
          nbr = this.NbrCross[DimCnt][AxCnt];
          if (nbr != null) {
            HgtSum += nbr.Height;
            VelSum += nbr.VSpeed;
            NNbrs++;
          }
        }
      }
      if (NNbrs > 0) {// Average influence per neighbor.
        basis.HgtAvg = HgtSum / NNbrs;
        basis.VelAvg = VelSum / NNbrs;
      }
    }
    /* ********************************************************************************* */
    public double Calc_Basis() {// Basis is just average of all neighbors. My height is attracted to this value.
      double Sum = 0, NNbrs = 0;
      Cell nbr;
      double Result = 0.0;
      for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
        for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
          nbr = this.NbrCross[DimCnt][AxCnt];
          if (nbr != null) {
            Sum += nbr.Height;
            NNbrs++;
          }
        }
      }
      if (NNbrs > 0) {
        Result = Sum / NNbrs;// 4 neighbors in 2 dimensions
      }
      return Result;// 4 neighbors in 2 dimensions
    }
    /* ********************************************************************************* */
    public double Calc_Tension() {// Returns (square root of?) total potential energy.
      double Basis = this.Calc_Basis();
      return Basis - this.Height;// pull toward avg amp of neighbors
    }
    /* ********************************************************************************* */
    public double Get_Energy_Flow(Cell Other) {
      double DeltaSpace = this.Height - Other.Height;// Backward spring force. Makes energy inflow negative, outflow positive. 
//      double DeltaSpace = Other.Height - this.Height;// Spring force. Inflow positive, outflow negative. Either sign is OK, but must be consistent.
      double DeltaTime = this.Height - this.Height_Prev;// Vertical Speed.
      double EVectMagnitude = DeltaSpace * DeltaTime;
      if (EVectMagnitude != 0) {
        if (EVectMagnitude != 1) {
          noop();
        }
      }
      return EVectMagnitude;
    }
  }

  /* ********************************************************************************* */
  public static class Vector {
    double[] Loc;
    /* ********************************************************************************* */
    public Vector() {
      this.Loc = new double[NDimsVector];
    }
    /* ********************************************************************************* */
    public void Assign(double XLoc, double YLoc) {
      this.Loc[0] = XLoc;
      this.Loc[1] = YLoc;
    }
    /* ********************************************************************************* */
    public void Assign(double XLoc) {
      this.Loc[0] = XLoc;
    }
    /* ********************************************************************************* */
    public void Copy_From(Vector Other) {
      for (int dcnt = 0; dcnt < Loc.length; dcnt++) {
        this.Loc[dcnt] = Other.Loc[dcnt];
      }
    }
    /* ********************************************************************************* */
    private void Multiply(double Factor) {
      for (int cnt = 0; cnt < Loc.length; cnt++) {
        this.Loc[cnt] *= Factor;
      }
    }
    /* ********************************************************************************* */
    public double GetMagnitude() {
      double SumSq = 0.0;
      for (int cnt = 0; cnt < Loc.length; cnt++) {
        SumSq += this.Loc[cnt] * this.Loc[cnt];
      }
      SumSq = Math.sqrt(SumSq);
      return SumSq;
    }
    /* ********************************************************************************* */
    public void Unitize() {
      double VecLength = this.GetMagnitude();
      if (VecLength == 0.0) {
        VecLength = 0.0000000000000001;
      }
      for (int cnt = 0; cnt < Loc.length; cnt++) {
        this.Loc[cnt] /= VecLength;
      }
    }
    /* ********************************************************************************* */
    public double DotProduct(Vector Other) {// https://en.wikipedia.org/wiki/Dot_product
      double Sum = 0.0;
      for (int cnt = 0; cnt < NDimsVector; cnt++) {
        Sum += this.Loc[cnt] * Other.Loc[cnt];
      }
      return Sum;
    }
    /* ********************************************************************************* */
    public boolean ValueCheck() {
      if (this.Loc[0] != 0.0) {
//        System.out.println("ValueCheck");
        if (this.Loc[1] != 1.0) {
//          System.out.println("ValueCheck");
          return true;
        }
      }
      return false;
    }
  }

  /* ********************************************************************************* */
  public static double ScalarProjection(Vector MeVect, Vector YouVect) {// https://en.wikipedia.org/wiki/Scalar_projection
    // first unitize YouVect
    // then get dotprod(mevect,  youvect)
    YouVect.Unitize();//?
    double Dot = MeVect.DotProduct(YouVect);
    return Dot;
  }
  /* ********************************************************************************* */
  public static double DopplerFrequencyFactor(Vector MeVect, Vector YouVect) {// 
    /*
     take scalar projection of unitized(?) me onto you.
     add 1 to that projection. 
     The length of that number is the number of beats I hear from you, if your wavelength is 1. 
     */
    MeVect.Unitize();// Unitizing me assumes my speed is always 1, which is true for light waves.
    YouVect.Unitize();
//    MeVect.ValueCheck();
//    double Factor = ScalarProjection(MeVec, YouVect);// we want scalar projection but we've already unitized the vectors.
    double Factor = MeVect.DotProduct(YouVect);
    Factor = 1.0 - Factor;
    return Factor;
  }
  /* ********************************************************************************* */
  public static void TestDoppler() {// 
    Vector MeVect = new Vector(), YouVect = new Vector();
    MeVect.Assign(1.0, 0.0);
    double FractAngle = 0.0, Angle;
    int Limit = 1000;
    System.out.println("FractAngle, Cosine, FreqFactor");
    for (int cnt = 0; cnt < Limit; cnt++) {
      FractAngle = ((double) cnt) / (double) Limit;
      Angle = FractAngle * TwoPi;
      double XLoc = Math.cos(Angle);
      double YLoc = Math.sin(Angle);
      YouVect.Assign(XLoc, YLoc);
      double dff = DopplerFrequencyFactor(MeVect, YouVect);
      //System.out.println("FractAngle: " + FractAngle + ", XLoc: " + XLoc + ", FreqFactor: " + dff);
      System.out.println(" " + FractAngle + ", " + XLoc + ", " + dff);
    }
  }
  /* ********************************************************************************* */
  public static double DopplerFreq(double RestFreq, double MyVelocity) {// https://en.wikipedia.org/wiki/Doppler_effect
    double C = 1.0;
    double VelSource = 0.0;// Source is not moving.
    double FreqObserved = (C + MyVelocity) / (C - VelSource);// Doppler if we are moving toward each other.
    return FreqObserved;
  }
  /* ********************************************************************************* */
  public static double LorentzMass(double RestMass, double Velocity) {
    double C2 = 1.0;
    double MassRel = RestMass / Math.sqrt(1.0 - (Velocity * Velocity / C2));

    double VelocityBack = Lorentz1Velocity(RestMass, MassRel);
    System.out.print("Velocity: " + Velocity);
    System.out.println(",  VelocityBack: " + VelocityBack);

    return MassRel;
  }
  /* ********************************************************************************* */
  public static double Lorentz1Velocity(double RestMass, double MassRel) {
    double C2 = 1.0;
    /*
     MassRel = RestMass / Math.sqrt(1.0 - (Velocity * Velocity / C2));
     MassRel/RestMass  = 1.0/Math.sqrt(1.0 - (Velocity * Velocity / C2));
     RestMass/MassRel  = Math.sqrt(1.0 - (Velocity * Velocity / C2));
     (RestMass/MassRel)^2  = 1.0 - ((Velocity * Velocity) / C2);
    
     (RestMass/MassRel)^2  - 1.0 =  -((Velocity * Velocity) / C2);
     ((RestMass/MassRel)^2 - 1.0) * C2 =  -(Velocity * Velocity);
    
     ((RestMass/MassRel)^2 - 1.0) * C2 =  -(Velocity * Velocity);
     -( ((RestMass/MassRel)^2 - 1.0) * C2) =  (Velocity * Velocity);
    
     cheat?
     sqrt( ((RestMass/MassRel)^2 - 1.0) * C2) =  Velocity ? 
     then compare with forward calc.
    
     double Ratio = (RestMass / MassRel);
     double Bad = 1.0 - (Ratio * Ratio)
     Math.sqrt((1.0 - (Ratio * Ratio)) * C2);
     Math.sqrt((1.0 - (Ratio * Ratio))) * C;
     Math.sqrt(1.0 - (RestMass*RestMass) / (MassRel*MassRel)) * C;
    
     Velocity = 
     Math.sqrt(1.0 - (RestMass0*RestMass0) / (MassRel0*MassRel0)) * C -
     Math.sqrt(1.0 - (RestMass1*RestMass1) / (MassRel1*MassRel1)) * C;
    
     Velocity/C = 
     Math.sqrt(1.0 - (RestMass0*RestMass0) / (MassRel0*MassRel0)) -
     Math.sqrt(1.0 - (RestMass1*RestMass1) / (MassRel1*MassRel1));
    
     double MassRel = RestMass / Math.sqrt(1.0 - (
     Velocity * Velocity 
     / C2));
    
     */
    double Ratio = (RestMass / MassRel);
    double Bad = ((Ratio * Ratio) - 1.0);
    Bad = -Bad;// bad 
    double VelocityBack = Math.sqrt((Bad) * C2);
    return VelocityBack;
  }
  /* ********************************************************************************* */
  public static double GetLorentzTimeFactor(double MassOverDist) {// Lorentz time dilation due to gravity
    double TimeFactor;// https://en.wikipedia.org/wiki/Gravitational_time_dilation
    double C2 = 1.0;
    double Extras = (2.0 * Waves.GravConst) / (C2);
    double ThingToRoot = 1.0 - (MassOverDist * Extras);
    if (ThingToRoot >= 0.0) {// Avoid square root of negative number.
      TimeFactor = Math.sqrt(ThingToRoot);
    } else {// Inside Schwarzschild radius just say time has stopped.
      TimeFactor = 0.0;
    }
    return TimeFactor;
  }
  /* ********************************************************************************* */
  public static void FindSine(double MinX, double MaxX, double TargetY) {// Binary search for sine Angle (time along) that matches given value.
    double PrevY = Math.sin(MinX);
    double NextY = Math.sin(MaxX);
    // put binary or rolling search here. 
    double MidY;
    for (int cnt = 0; cnt < 4; cnt++) {
      MidY = (PrevY + NextY) * 0.5;
      if (TargetY < MidY) {

      }
    }
  }
  /* ****************************************************** */
  public static void Find_Closest_On_Curve(double MinX, double MaxX, double TargetY) {
    double Fineness = 0.000001;
    Fineness = 0.1;
    double PrevDistance, Distance;
    double Increment, WalkerX;
    double FirstSign = 1.0;
    double Answer = Math.asin(TargetY);
    System.out.println("Answer:" + Answer);/// Answer:0.2999999999985978
    Increment = Fineness * FirstSign;
    WalkerX = MinX;
    double SineY = Math.sin(WalkerX);
    Distance = Math.abs(SineY - TargetY);
    for (int cnt = 0; cnt < 10; cnt++) {
      do {// while getting closer
        SineY = Math.sin(WalkerX);
        PrevDistance = Distance;
        Distance = Math.abs(SineY - TargetY);
        WalkerX += Increment;
//        System.out.println("WalkerX:" + WalkerX);// WalkerX:0.3199019900000001
      } while (PrevDistance >= Distance);
      System.out.println("WalkerX:" + WalkerX);// WalkerX:0.3199019900000001
      Increment *= -Fineness;// reverse direction and increase resolution
    }
//    // at this point, PrevDistance will be the closest.  Prev Walker? 
//    Walker.Subtract(mote);// correction vector is Walker.Subtract(mote);
//    mote.Attraction.Copy_From(Walker);
////    System.out.println();
  }
  /* ********************************************************************************* */
  public static void WaveMetric() {// Was WaveMetric_Print.
    double TRate_Standard_Local = 0.1;
    double Height_Prev = 0, Height = 0, Height_Next = 0;
    double VSpeed_Prev = 0, VSpeed = 0, VSpeed_Next = 0;
    double TRate_Prev = TRate_Standard_Local, TRate = TRate_Standard_Local, TRatio;
    double CosHgt_Prev = 0, CosHgt = 0, CosHgt_Next = 0;
    double CosVSpd_Prev = 0, CosVSpd = 0, CosVSpd_Next = 0, CosVSpdScaled = 0;
    double VsDiff, VsRatio;
    double AngleCos, AngleSin;
    double SinProjHgt, InterpSineX, ISXDiff;
    double Angle, Angle_Prev;

    System.out.print("TRate, ");
    System.out.print("CosHgt_Prev" + ", " + "CosHgt" + ", " + "CosHgt_Next" + ", ");
    System.out.print("CosVSpd_Prev" + ", " + "CosVSpd" + ", " + "CosVSpd_Next" + ", ");
    System.out.print("CosVSpdScaled, ");
    System.out.print("SinProjHgt" + ", ");
    System.out.print("VsDiff" + ", ");
    System.out.print("VsRatio" + ", ");
    System.out.print("AngleCos" + ", ");
    System.out.print("AngleSin" + ", ");
    System.out.print("InterpSineX" + ", ");
    System.out.print("ISXDiff" + ", ");

    System.out.println();

    TRate_Prev = TRate = TRate_Standard_Local;

    Height_Prev = Height = Height_Next = 0;
    VSpeed_Prev = VSpeed = VSpeed_Next = 0;
    Angle_Prev = 0;
    Angle = 0;
    CosHgt = 1.0;
    Height = 1.0;
    for (int GenCnt = 0; GenCnt < 200; GenCnt++) {
//      TRate = 0.125 + ((GenCnt / 4) % 2) * 0.05;// Toggle  TRate every few timesteps.
      TRate = 0.01 + ((GenCnt / 4) % 2) * 0.005;// Toggle  TRate every few timesteps.
//      TRate = 0.01;

      {
        AngleCos = Angle;

        CosHgt_Next = Math.cos(Angle);// ideal cosine
        CosVSpd_Next = CosHgt_Next - CosHgt;

        AngleSin = (Angle_Prev) + (TRate_Prev * 0.5);
        // Both of these line sin hgt up with cos vspeed.
//          SinProjHgt = Math.sin(Angle - (TRate_Prev * 0.5));// Projected height if TRate had not changed
        SinProjHgt = -Math.sin(AngleSin);// Projected height if TRate had not changed

        CosVSpdScaled = CosVSpd_Next / TRate_Prev;// best so far
//        CosVSpdScaled = CosVSpd_Next / TRate;

        InterpSineX = Math.asin(CosVSpdScaled);
        // double SineOffsetX = SineX - Angle;
        ISXDiff = AngleCos + InterpSineX;// Really difference, as asin is negative. 

        VsDiff = CosVSpdScaled - SinProjHgt;
        VsRatio = CosVSpdScaled / SinProjHgt;
      }

      System.out.print(TRate + ", ");
      System.out.print(CosHgt_Prev + ", " + CosHgt + ", " + CosHgt_Next + ", ");
      System.out.print(CosVSpd_Prev + ", " + CosVSpd + ", " + CosVSpd_Next + ", ");
      System.out.print(CosVSpdScaled + ", ");
      System.out.print(SinProjHgt + ", ");
      System.out.print(VsDiff + ", ");
      System.out.print(VsRatio + ", ");
      System.out.print(AngleCos + ", ");
      System.out.print(AngleSin + ", ");
      System.out.print(InterpSineX + ", ");
      System.out.print(ISXDiff + ", ");
      System.out.println();

      {// Rollover
        // Cosine rollover
        CosHgt_Prev = CosHgt;
        CosHgt = CosHgt_Next;

        CosVSpd_Prev = CosVSpd;
        CosVSpd = CosVSpd_Next;

        TRate_Prev = TRate;

        Angle_Prev = Angle;
        Angle += TRate;
      }
    }
  }
  /* ********************************************************************************* */
  public static void WaveMetric_Best() {// Was best WaveMetric.
    double TRate_Standard = 0.1;
    double Height_Prev = 0, Height = 0, Height_Next = 0;
    double VSpeed_Prev = 0, VSpeed = 0, VSpeed_Next = 0;
    double Tension = 0;
    double TRate_Prev = TRate_Standard, TRate = TRate_Standard, TRatio, TRateSq;
    double TRate_Avg;
    double CosHgt_Prev = 0, CosHgt = 0, CosHgt_Next = 0;
    double CosVSpd_Prev = 0, CosVSpd = 0, CosVSpd_Next = 0, CosVSpd_Split = 0, CosVSpdScaled = 0;
    double SinHgt, SinHgtSq, VsDiff, VsRatio = 1.0;
    double SinProjHgt;
    double Angle, Angle_Prev;
    double Energy = 0, CosEnergy = 0;
    boolean Emergent = false;

    System.out.print("TRate, ");
    System.out.print("CosHgt_Prev" + ", " + "CosHgt" + ", " + "CosHgt_Next" + ", ");
    System.out.print("CosVSpd_Prev" + ", " + "CosVSpd" + ", " + "CosVSpd_Next" + ", ");
    System.out.print("CosVSpdScaled, ");
    System.out.print("CosEnergy, ");
    System.out.print("SinHgt" + ", ");
    System.out.print("SinProjHgt" + ", ");
    System.out.print("VsDiff" + ", ");
    System.out.print("VsRatio" + ", ");

    if (Emergent) {
      System.out.print("Height_Prev" + ", " + "Height" + ", " + "Height_Next" + ", ");
      System.out.print("VSpeed_Prev" + ", " + "VSpeed" + ", " + "VSpeed_Next" + ", ");
      System.out.print("Energy, ");
    }

    System.out.println();

    TRate_Prev = TRate = TRate_Standard;
//    TRate_Prev = TRate = 0.36;

    Height_Prev = Height = Height_Next = 0;
    VSpeed_Prev = VSpeed = VSpeed_Next = 0;
    Angle_Prev = 0;
    Angle = 0;
    CosHgt = 1.0;
    Height = 1.0;
    for (int GenCnt = 0; GenCnt < 100; GenCnt++) {
//      if (GenCnt == 55) {
//        TRate /= 2.0;
//      }
//      TRate = (((double) GenCnt + 1.0) / 100) * 0.5;
//      TRate = 0.5 * (1.0 - (((double) (GenCnt / 4) / 25)));// slower time rate every 4 time steps 
//      TRate = 0.5 * ((((double) (GenCnt / 4) / 25)));// faster time rate every 4 time steps 

      TRate = 0.125 + ((GenCnt / 4) % 2) * 0.05;// Toggle  TRate every few timesteps.

      TRate_Avg = (TRate_Prev + TRate) * 0.5;
      {// ideal cosine
        CosHgt_Next = Math.cos(Angle);
        CosVSpd_Next = CosHgt_Next - CosHgt;
//        CosVSpd_Split = (CosVSpd + CosVSpd_Next) * 0.5;
        CosVSpd_Split = (CosVSpd_Prev + CosVSpd) * 0.5;
        {
          double CosVsSq = CosVSpd_Next * CosVSpd_Next;
          double CosHgtSq = (CosHgt_Next * CosHgt_Next);
        }
//        SinHgt = Math.sin(Angle);
        SinHgt = Math.sin(Angle - (TRate * 0.5));// Set Sine one half time step back.  best
        SinHgt *= -1;
        SinHgtSq = SinHgt * SinHgt;

        {// Both of these line sin hgt up with cos vspeed.
//          SinProjHgt = Math.sin(Angle - (TRate_Prev * 0.5));// Projected height if TRate had not changed
          SinProjHgt = Math.sin((Angle_Prev) + (TRate_Prev * 0.5));// Projected height if TRate had not changed
          SinProjHgt *= -1;
        }
        CosVSpdScaled = CosVSpd_Next / TRate_Prev;// best so far
//        CosVSpdScaled = CosVSpd_Prev / TRate_Prev;// worse
//        CosVSpdScaled = CosVSpd_Split / TRate_Prev;
//        CosVSpdScaled = CosVSpd_Next / TRate;
//        CosVSpdScaled = CosVSpd_Next / TRate_Avg;// worse

//        VsDiff = CosVSpdScaled + SinHgt;
        VsDiff = CosVSpdScaled - SinHgt;

        VsRatio = CosVSpdScaled / SinHgt;

        double CosTension = -CosHgt_Next;// Always attracted to 0.  Tension moved!!! 
        CosEnergy = (CosTension * CosTension) + (CosVSpdScaled * CosVSpdScaled);
      }

      if (Emergent) {// emergent cosine
        TRateSq = TRate * TRate;
        TRatio = TRate / TRate_Prev;
        VSpeed_Next = (Tension * TRateSq) + (VSpeed * TRatio);// best
        Height_Next = Height + VSpeed_Next;
        Tension = -Height_Next;// Always attracted to 0.  Tension moved!!! 

        double TensionSq = (Tension * Tension);
        double VSpdInterp = (VSpeed + VSpeed_Next) * 0.5;// Average
//        double VSpdScaled = VSpdInterp / TRate;
        double VSpdScaled = VSpeed_Next / TRate;
//        double VSpdScaled = VSpeed_Next / TRate_Prev;
        double VSpdSq = VSpdScaled * VSpdScaled;
        Energy = TensionSq + VSpdSq;
      }

      System.out.print(TRate + ", ");
      System.out.print(CosHgt_Prev + ", " + CosHgt + ", " + CosHgt_Next + ", ");
      System.out.print(CosVSpd_Prev + ", " + CosVSpd + ", " + CosVSpd_Next + ", ");
      System.out.print(CosVSpdScaled + ", ");
      System.out.print(CosEnergy + ", ");
      System.out.print(SinHgt + ", ");
      System.out.print(SinProjHgt + ", ");
      System.out.print(VsDiff + ", ");
      System.out.print(VsRatio + ", ");

      if (Emergent) {
        System.out.print(Height_Prev + ", " + Height + ", " + Height_Next + ", ");
        System.out.print(VSpeed_Prev + ", " + VSpeed + ", " + VSpeed_Next + ", ");
        System.out.print(Energy + ", ");
      }

      System.out.println();

      {// Rollover
        // Cosine rollover
        CosHgt_Prev = CosHgt;
        CosHgt = CosHgt_Next;

        CosVSpd_Prev = CosVSpd;
        CosVSpd = CosVSpd_Next;

        if (Emergent) {
          Height_Prev = Height;
          Height = Height_Next;

          VSpeed_Prev = VSpeed;
          VSpeed = VSpeed_Next;
        }

        TRate_Prev = TRate;

        Angle_Prev = Angle;
        Angle += TRate;
      }
    }
  }
  /* ********************************************************************************* */
  public static void WaveMetric2() {
    double TRate_Standard = 0.1;
    double Height_Prev = 0, Height = 0, Height_Next = 0;
    double VSpeed_Prev = 0, VSpeed = 0, VSpeed_Next = 0;
    double Tension = 0;
    double TRate_Prev = TRate_Standard, TRate = TRate_Standard, TRatio;
    double CycleCnt = 0;
    double SamplesPerCycle = 0, CyclesPerSample;
    double CosHgt_Prev = 0, CosHgt = 0, CosHgt_Next = 0;
    double CosVSpd_Prev = 0, CosVSpd = 0, CosVSpd_Next = 0;
    double CosAngle = 0;
    double Energy, CosEnergy;
//    CycleCnt = 1;
//    System.out.println("TRate" + ", " + "SamplesPerCycle");
//    System.out.println("TRate" + ", " + "CyclesPerSampleSq" + ", " + "Ratio" + ", " + "Zeros");
//    System.out.println("TRate" + ", " + "CyclesPerSample" + ", " + "Ratio" + ", " + "Zeros");

    System.out.print("Height_Prev" + ", " + "Height" + ", " + "Height_Next" + ", ");
    System.out.print("VSpeed_Prev" + ", " + "VSpeed" + ", " + "VSpeed_Next" + ", ");

    System.out.print("CosHgt_Prev" + ", " + "CosHgt" + ", " + "CosHgt_Next" + ", ");
    System.out.print("CosVSpd_Prev" + ", " + "CosVSpd" + ", " + "CosVSpd_Next" + ", ");

    System.out.print("Energy, ");
    System.out.print("CosEnergy, ");

    System.out.println();

//    TRate_Prev = TRate = 0.001;// TRate_Standard;// 0.75;
    TRate_Prev = TRate = 0.1;// TRate_Standard;// 0.75;
//    TRate_Prev = TRate = 1.0;

//    for (int TRateCnt = 1; TRateCnt < 1000; TRateCnt++) {
    for (int TRateCnt = 1; TRateCnt < 2; TRateCnt++) {
//      TRate_Prev = TRate = 0.01 * (double) TRateCnt;
//      if (TRate >= 0.5) {
//        break;
//      }
      Height_Prev = Height = Height_Next = 0;
      VSpeed_Prev = VSpeed = VSpeed_Next = 0;
      Height = 1.0;
      CosHgt = 1.0;
      CycleCnt = 0;
      for (int GenCnt = 0; GenCnt < 100; GenCnt++) {// 10000000 // 1000
        if (GenCnt == 55) {
//          TRate *= Math.random();// 3.0;
//          TRate = Math.random() * 0.5;// 3.0;
//          TRate *= 2.0;
          TRate /= 2.0;
        }
        if (false) {// low freq biased random 
          double randsq = Math.random();
          randsq = randsq * randsq * randsq;
          randsq = randsq * randsq * randsq;
          TRate = (TRate * 0.99) + (randsq * 0.01);/// Flywheel
//          TRate = (TRate * 0.5) + (Math.random() * 0.5);/// Flywheel
        }
        {
//          CosAngle += TRate;
          CosHgt_Next = Math.cos(CosAngle);
          CosVSpd_Next = CosHgt_Next - CosHgt;
        }
//        Tension = -Height;// Always attracted to 0.
        double TRateSq = TRate * TRate;
        double TRate_PrevSq = TRate_Prev * TRate_Prev;
//        TRatio = TRateSq / TRate_PrevSq;
        TRatio = TRate / TRate_Prev;
        VSpeed_Next = (Tension * TRateSq) + (VSpeed * TRatio);// best
        // or VSpeed_Next = (Tension + (VSpeed / TRate_Prev)) * TRate;
        Height_Next = Height + VSpeed_Next;
        Tension = -Height_Next;// Always attracted to 0.  Tension moved!!! 

        double TensionSq = (Tension * Tension);
        double VSpdInterp = (VSpeed + VSpeed_Next) * 0.5;// Average
//        double VSpdScaled = VSpdInterp / TRate;
        double VSpdScaled = VSpeed_Next / TRate;
//        double VSpdScaled = VSpeed_Next / TRate_Prev;
        double VSpdSq = VSpdScaled * VSpdScaled;
        Energy = TensionSq + VSpdSq;

        {
          double CosVSpdScaled = CosVSpd_Next / TRate_Prev;
          double CosTension = -CosHgt_Next;// Always attracted to 0.  Tension moved!!! 
          CosEnergy = (CosTension * CosTension) + (CosVSpdScaled * CosVSpdScaled);
        }

        System.out.print(Height_Prev + ", " + Height + ", " + Height_Next + ", ");
        System.out.print(VSpeed_Prev + ", " + VSpeed + ", " + VSpeed_Next + ", ");

        System.out.print(CosHgt_Prev + ", " + CosHgt + ", " + CosHgt_Next + ", ");
        System.out.print(CosVSpd_Prev + ", " + CosVSpd + ", " + CosVSpd_Next + ", ");

        System.out.print(Energy + ", ");
        System.out.print(CosEnergy + ", ");

        System.out.println();

//        double energy = Height_Next * Height_Next + VSpeed_Next * VSpeed_Next;// these do not work.
//        double energy2 = Math.abs(Height_Next) + Math.abs(VSpeed_Next);
        {
          double vjust = VSpeed_Next / TRate;
          double VsSq = VSpeed_Next * VSpeed_Next;
          double HgtSq = (Height_Next * Height_Next);
          double energy = HgtSq + (vjust * vjust);// these do not work.
        }

        {
          double VJust = VSpeed_Next / TRate;
          double VJustSq = VJust * VJust;

          double SinHgt = Math.sin(CosAngle);
          double SinHgtSq = SinHgt * SinHgt;

          double CosVJust = CosVSpd_Next / TRate;
          double CosVJustSq = CosVJust * CosVJust;

          double CosVsSq = CosVSpd_Next * CosVSpd_Next;
          double CosHgtSq = (CosHgt_Next * CosHgt_Next);

          double Rat = CosVJustSq / VJustSq;

          double PureRat = CosVJustSq / SinHgtSq;
          double PureDeltaSq = CosVJustSq - SinHgtSq;
          double DeltaSq = VJustSq - SinHgtSq;// VJustSq is closer to SinHgtSq than it is to CosVJustSq.  9.097675529590177E-8
//          double DeltaSq = VJustSq - CosVJustSq;// 4.254956243320007E-4
//          double rat = vjust / SinHgt;
//          double energy = HgtSq + (vjust * vjust);// these do not work.
          double energy = CosHgtSq + SinHgtSq;// these do not work.
          if (Rat < 0.98 || Rat > 1.0) {
            noop();
          }
          if (GenCnt > 500) {
            noop();
          }
        }

        {
          if (Height_Prev < Height && Height > Height_Next) {// crest
            CycleCnt++;
            SamplesPerCycle = ((double) GenCnt) / CycleCnt;
          }
        }

        {// Rollover
          Height_Prev = Height;
          Height = Height_Next;

          VSpeed_Prev = VSpeed;
          VSpeed = VSpeed_Next;

          // Cosine rollover
          CosHgt_Prev = CosHgt;
          CosHgt = CosHgt_Next;

          CosVSpd_Prev = CosVSpd;
          CosVSpd = CosVSpd_Next;

          TRate_Prev = TRate;

          CosAngle += TRate;
        }
//        System.out.println();
      }
//      System.out.println(TRate + ", " + SamplesPerCycle);
      CyclesPerSample = 1.0 / SamplesPerCycle;
//      System.out.println(TRate + ", " + CyclesPerSample + ", " + CyclesPerSample / TRate + ", " + 0.0);
      break;
//      System.out.println(TRate + ", " + CyclesPerSample + ", " + TRate / CyclesPerSample + ", " + 0.0);

      // TRate / CyclesPerSample approaches TwoPi. 
      // TRate * SamplesPerCycle approaches TwoPi.  So when TRate is 1, SamplesPerCycle is TwoPi samples. 
//      double CyclesPerSampleSq = CyclesPerSample * CyclesPerSample;
//      System.out.println(TRate + ", " + CyclesPerSampleSq + ", " + CyclesPerSampleSq / TRate + ", " + 0.0);
    }
//    System.out.println(SamplesPerCycle);
    /*
     Also try this without scaling speed!  Just see what the ratio is. 
    
     What next? 
     Now we can define what sine wave we expect to see for a given time rate.
     That means we can feed the formula sine inputs and compare with the sine next step. 
     While that is happening, change the time rate at peak vspeed. 
     See what diverges first I guess. 
    
     */
  }
  /* ********************************************************************************* */
  public static void WaveMetric_Trig() {
    double TRate = 0.1;
    double Height = 0, Height_Next = 0;
    double VSpeed = 0, VSpeed_Next = 0;
    double VSpeed_Temp;
    double Displacement, Base = 0.0;
    double Cycles = 0.0;// a true cycle would be two pi, these are just 0-1.0 
    int Generations = 400;
    Generations = 4000;

    Height = 0.5;
    for (int GenCnt = 0; GenCnt < Generations; GenCnt++) {
      if (false) {
        if (GenCnt == 50) {
          TRate *= 0.1;
        } else if (GenCnt == 150) {
          TRate *= 4.0;
        }
      }
      Base = Math.sin(Cycles * 3.17);
      Displacement = Height - Base;
      double VSpeedSq = VSpeed * VSpeed;
      double HeightSq = Height * Height;
      double Energy = HeightSq + VSpeedSq;

      VSpeed_Temp = VSpeed;
      if (false) {
        double TRateSq = TRate * TRate;
        double TRatio = 1.0;
        double Tension = -Displacement;
        VSpeed_Temp = (VSpeed * TRatio) + (Tension * TRateSq);
      }

      if (false) {// Rotate 
        double Radius = Math.hypot(Displacement, VSpeed);
        double Angle = Math.atan2(VSpeed, Displacement);
        Angle += TRate;
        Height_Next = Math.cos(Angle) * Radius;
        VSpeed_Next = Math.sin(Angle) * Radius;
        System.out.print(Displacement + ", " + VSpeed + ", " + Angle + ", " + Energy + ", ");
      } else if (true) {// Rotate matrix
        double Cos = Math.cos(TRate);
        double Sin = Math.sin(TRate);
        Height_Next = (Displacement * Cos) - (VSpeed_Temp * Sin);
        Height_Next += Base;
        VSpeed_Next = (Displacement * Sin) + (VSpeed_Temp * Cos);
        System.out.print(Height + ", " + VSpeed + ", " + Base + ", " + Displacement + ", " + Energy + ", ");
      }

//      Base = GenCnt * TRate * 3.17;
//      Base += TRate * 3.17;
      Cycles += TRate * 3.17;
      Height = Height_Next;
      VSpeed = VSpeed_Next;
      System.out.println();
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public static void WaveMetric_Simple() {// 
    double TRate = 0.1, TRate_Prev;
    double Height_Prev = 0, Height = 0, Height_Next = 0;
    double VSpeed = 0, VSpeed_Next = 0;
    double Tension = 0.0;
    double SinHgt, CosHgt, Angle = 0;
    double SinSq, CosSq;
    double Energy_Prev = 1.0, Energy = 1.0;
//    String test = String.format("%-3s:%-3s", "abcdefg","b");
//    Angle = TRate / 2.0;
//    TRate *= 0.1;
    TRate *= 4.0;
    TRate_Prev = TRate;
    Height_Prev = Height = 1.0;
    for (int GenCnt = 0; GenCnt < 400; GenCnt++) {
      Angle = (400.0 / 279.0) * TRate * GenCnt;
      // 400.0 / 279.0 = 1.43369175627,   Height=0.84, VSpd=-0.4000000000000001, Cos=0.840019750626949, Energy=0.8656331814433617, 
//      Angle = (394.0 / 391.0) * TRate * GenCnt;
//      Angle = (394.0 / 391.0) * TRate * GenCnt + (TRate * 0.1);
//      Angle = (394.0 / 393.6) * TRate * GenCnt;
//      Angle = (394.0 / 393.6) * TRate * GenCnt;
      SinHgt = Math.sin(Angle);
      CosHgt = Math.cos(Angle);
      SinSq = SinHgt * SinHgt;
      CosSq = CosHgt * CosHgt;

//      Height = CosHgt;
//      VSpeed = SinHgt * TRate;
      Tension = -Height;// Always attracted to 0.  

      double VSpeedAdj = VSpeed / TRate;
      double VSpeedSq = VSpeedAdj * VSpeedAdj;
      double TensionSq = Tension * Tension;
//      double VSpeedSq = SinHgt * SinHgt;// test 
//      double TensionSq = CosHgt * CosHgt;// test 

      {// Energy clamp
        Energy_Prev = Energy;
//        Energy = VSpeedSq + TensionSq;
//        Energy = SinSq + TensionSq;
        Energy = VSpeedSq + CosSq;// Evidentally Height or Tension are worse for noise. 
        double Corrector = Math.sqrt(Energy_Prev / Energy);
//        Height *= Corrector;
//        Tension *= Corrector;
//        VSpeed *= Corrector;
      }

//      Tension = Math.sqrt(Math.abs(Energy - VSpeedSq)) * -Math.signum(Height);
//      VSpeed_Next = Math.sqrt(Energy-TensionSq);
      System.out.print(Height + ", " + VSpeedAdj + ", " + CosHgt + ", " + Energy + ", ");
//      System.out.print(-Tension + ", " + VSpeedAdj + ", " + CosHgt + ", " + Energy + ", ");

      TRate_Prev = TRate;
      if (false) {
        if (GenCnt == 50) {
//        TRate *= 0.5;
//        TRate *= 2.0;
          TRate *= 4.0;
//        TRate /= 4.0;
        } else if (GenCnt == 150) {
          TRate *= 0.1;
        }
      }
//      VSpeed_Next = VSpeed + (Tension * TRate);
      double TRateSq = TRate * TRate;
      double TRatio = TRate / TRate_Prev;
//      VSpeed_Next = (VSpeed) + (Tension * TRate);// 
      VSpeed_Next = (VSpeed * TRatio) + (Tension * TRateSq);// best
      Height_Next = Height + VSpeed_Next;

      if (false) {
        double TRateSq2 = TRateSq * 0.5;// slow down the height
        double VSpeed_Next2 = (VSpeed * TRatio) + (Tension * TRateSq2);// 
        Height_Next = Height + VSpeed_Next2;
      }

//      Height_Next = Height + VSpeed;// This inflates energy for some reason.
      {// set up for next cycle  public void Rollover() 
        Height_Prev = Height;
//        Height = (Height + Height_Next) / 2.0;
        Height = Height_Next;
        VSpeed = VSpeed_Next;
//        Angle += TRate;
      }
      System.out.println();
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public static void TestSines() {
    double FractAngle = 0.0, Angle;
    double PrevHeight = 0.0, Height = 0.0, NextHeight = 0.0, NorthNbrHeight = 0.0, SouthNbrHeight = 0.0;
    double PrevSpeed = 0.0, Speed = 0.0, NextSpeed = 0.0, NbrSpeed = 0.0;

    double DeltaPast = 0.0, DeltaNorthNbr = 0.0, DeltaSouthNbr = 0.0;
    double SouthCheck, NorthCheck;
    double Stretch = 100.0;
    int Limit = 100;
    double NumCycles = 2.0;
    double Damp = 1.0;
    Stretch = 2.0 * Limit / Math.PI;
    System.out.println("Height, NorthCheck, SouthCheck, HeightSpeed, HeightSpeedSquared");
    for (int cnt = 0; cnt < Limit; cnt++) {
      FractAngle = ((double) cnt) / (double) Limit;
      FractAngle *= NumCycles;
      Angle = FractAngle * TwoPi;

      NextHeight = Math.sin(Angle);
//      NextHeight = SawTooth(FractAngle);
//      NextHeight = Triangle(FractAngle);
      NextHeight = NextHeight * Damp;

      NorthNbrHeight = NextHeight;
      SouthNbrHeight = PrevHeight;
      DeltaNorthNbr = Height - NorthNbrHeight;
      DeltaSouthNbr = Height - SouthNbrHeight;

      DeltaPast = Height - PrevHeight;

      NorthCheck = DeltaNorthNbr * DeltaPast;
      SouthCheck = DeltaSouthNbr * DeltaPast;
      System.out.println(Height + ", " + NorthCheck * Stretch + ", " + SouthCheck * Stretch + ", " + DeltaPast + ", " + DeltaPast * DeltaPast * Stretch);

      PrevHeight = Height;
      Height = NextHeight;
//      Damp *= 0.95;
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public static double Triangle(double XVal) {
    int XInt = (int) (XVal * 1000);
    XInt = XInt % 1000;
    if (XInt > 500) {
      XInt = 1000 - XInt;
    }
    XVal = ((double) XInt) / 500.0;
    return XVal;
  }
  /* ********************************************************************************* */
  public static double SawTooth(double XVal) {
    int XInt = (int) (XVal * 1000);
    XInt = XInt % 1000;
    XVal = ((double) XInt) / 1000.0;
    return XVal;
  }
  /* ********************************************************************************* */
  public static double SawToothPi(double XVal) {
    XVal = XVal / TwoPi;
    int XInt = (int) (XVal * 1000);
    XInt = XInt % 1000;
    XVal = ((double) XInt) / 1000.0;
    return XVal;
  }
  /* ********************************************************************************* */
  public static double Sigmoid(double xin) {
    double OutVal;
    OutVal = xin / Math.sqrt(1.0 + xin * xin);// symmetrical sigmoid function in range -1.0 to 1.0.
    return OutVal;
    /*
     double power = 2.0; 
     OutVal = xin / Math.pow(1 + Math.abs(Math.pow(xin, power)), 1.0 / power);
     */
  }
  /* ********************************************************************************* */
  public static Color ToNegPos(double Fraction) {// negative is blue, positive is red 
    Fraction = Sigmoid(Fraction * 0.06);
    Fraction = (Fraction + 1.0) / 2.0;
    return new Color((float) (Fraction), (float) 0.0, (float) (1.0 - Fraction));
  }
  /* ********************************************************************************* */
  public static void noop() {
  }
}

/*
 (0.662644757 + 0.667475999999998)/2.0 =

 0.6650603785 
 0.664364364364364

 */
