/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expj;

/**
 *
 * @author MultiTool
 */
public class Hyperbolas {
  public static void SpeedField() {
    // Map the apparent speed of moving object A, 
    // from the point of view of object B, for all the different angles it can move in relation to A.
    // All speeds are assumed to be 1, so maximum combined speed is 2. 
    int NumSteps = 100;
    for (int cnt = 0; cnt < NumSteps; cnt++) {
      double FractAngle = (double) (cnt) / (double) NumSteps;
      double FractAngleOpposite = FractAngle + 0.5;// 180 degress out of phase
      double Angle = FractAngle * 2.0 * Math.PI;
      double HalfAngle = Angle / 2.0;
      double Length = Math.sin(HalfAngle) * 2.0;
      double Energy0 = Angle2Energy(FractAngle);
      double Energy1 = Angle2Energy(FractAngleOpposite);
      System.out.println("" + (Energy0) + ", " + (Energy1) + ", " + (Energy0 + Energy1));
//      double Wave = (Math.cos(Angle) + 1.0) / 2.0;
//      System.out.println("Length:" + (Length / 2.0) + ", Wave:" + Wave);
//      System.out.println("Length:" + (Length) + ", Wave:" + Wave);
//      System.out.println("" + (Length) + ", " + Energy0 + ", " + Wave);
    }
//    for (double angle = 0;angle<NumDivs;)
  }
  public static double Angle2Energy(double FractAngle) {
    double Angle = FractAngle * 2.0 * Math.PI;
    double HalfAngle = Angle / 2.0;
    double Length = Math.sin(HalfAngle) * 2.0;
    double Energy = Length * Length;
    return Energy;
  }
  public static void Test() {
    int Limit = 200;
    double Diff = 10.0;// 4.0;// 2.0;
    double DragY0, YVal0 = 0, DragY1, YVal1 = 0;
    double Ratio0, Ratio1;
    double DragSum, Sum = 0, RatioSum;
    for (double XCnt = 0; XCnt < Limit; XCnt++) {
      DragY0 = YVal0;
      DragY1 = YVal1;
      YVal0 = 1.0 / XCnt;
      YVal1 = 1.0 / (XCnt + Diff);
      DragSum = Sum;
      Sum = YVal0 + YVal1;

      Ratio0 = YVal0 / DragY0;
      Ratio1 = YVal1 / DragY1;
      RatioSum = Sum / DragSum;

      double Next0 = CalcNext(DragY0, YVal0);
      double Next1 = CalcNext(DragY1, YVal1);
      double NextSum = CalcNext(DragSum, Sum);

      //System.out.println("Ratio0:" + Ratio0 + ", Ratio1:" + Ratio1 + ", RatioSum:" + RatioSum);
      System.out.println("YVal0:" + YVal0 + ", YVal1:" + YVal1 + ", Sum:" + Sum);
      System.out.println();
      // System.out.println("Next0:" + Next0 + ", Next1:" + Next1 + ", NextSum:" + NextSum);
      System.out.println("Next0:" + Next0 + ", Next1:" + Next1 + ", NextSumError:" + NextSum / Sum);
    }
  }
  public static double CalcNext(double Drag, double Current) {
    double Ratio = Current / Drag;
    double Comp = 1.0 - Ratio;
    double Inv = 1.0 / Comp;
    double NextInv = Inv + 1.0;
    double Next = 1.0 / NextInv;
    return Next;
  }
}
/*
 So next step is
 find formula that takes now/drag and computes next
 5/6
 1 - 5/6 = 1/6
 6/1 + 1 = 7
 next is 1/7
 */
