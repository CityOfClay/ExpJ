package expj;

import java.io.FileWriter;   // Import the FileWriter class
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MultiTool
 *
 * Unroll tilted cylinder to height map.
 *
 */
// Air drag of high atmosphere balloon to space. 
public class Barrel {

  /* **************************************************************************** */
// Barrel Terrain math
  public static class Point3D {

    public double X, Y, Z;

    public Point3D() {
      this(0, 0, 0);
    }

    public Point3D(double X0, double Y0, double Z0) {
      this.X = X0;
      this.Y = Y0;
      this.Z = Z0;
    }

    public double Magnitude() {
      return Math.sqrt((this.X * this.X) + (this.Y * this.Y) + (this.Z * this.Z));
    }

    public double Distance(Point3D other) {
      double DX, DY, DZ;
      DX = this.X - other.X;
      DY = this.Y - other.Y;
      DZ = this.Z - other.Z;
      return Math.sqrt((DX * DX) + (DY * DY) + (DZ * DZ));
    }
  };

  public static class GridVec {

    public ArrayList<Point3D> Ray = new ArrayList<Point3D>();
    public int Wdt, Hgt;//  uintmax_t

    public GridVec(int Wdt0, int Hgt0) {
      this.Wdt = Wdt0;
      this.Hgt = Hgt0;
      this.Ray.ensureCapacity(Wdt0 * Hgt0);
      for (int cnt = 0; cnt < Wdt0 * Hgt0; cnt++) {
        this.Ray.add(new Point3D());
      }
    }

    public Point3D Get(int Dex) {
      return this.Ray.get(Dex);
    }

    public Point3D Get(int XLoc, int YLoc) {
      int Dex = (YLoc * this.Wdt) + XLoc;
      return this.Ray.get(Dex);
    }

    public void Set(int XLoc, int YLoc, Point3D Value) {
      int Dex = (YLoc * this.Wdt) + XLoc;
      this.Ray.set(Dex, Value);
    }

    int GetDex(int XLoc, int YLoc) {
      return (YLoc * this.Wdt) + XLoc;
    }
  };

  public static void SaveObj(GridVec Grid, String fname) {
    Point3D pnt;
    try {
      FileWriter file = new FileWriter(fname);
      file.write("# Terrain Map \n");
      for (int GridDex = 0; GridDex < Grid.Ray.size(); GridDex++) {
        pnt = Grid.Get(GridDex);
        file.write("v " + pnt.X + " " + pnt.Y + " " + pnt.Z + " 1.0 " + "\n");// vertex 
      }
      int DragY = 0;
      for (int GridY = 1; GridY < Grid.Hgt; GridY++) { // Y is cylinder height.
        int DragX = 0;
        for (int GridX = 1; GridX < Grid.Wdt; GridX++) { // X is circumference.
          int Dex0 = Grid.GetDex(DragX, DragY) + 1;
          int Dex1 = Grid.GetDex(DragX, GridY) + 1;
          int Dex2 = Grid.GetDex(GridX, GridY) + 1;
          int Dex3 = Grid.GetDex(GridX, DragY) + 1;
          file.write("f " + Dex0 + " " + Dex1 + " " + Dex2 + " " + Dex3 + " " + "\n");// face
          DragX = GridX;
        }
        DragY = GridY;
      }
      file.close();
    } catch (IOException ex) {
      Logger.getLogger(Barrel.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public static void Test() {
    double TwoPi = Math.PI * 2.0;
    double GeoRadius = 1.0;
    int GridWdt = 600, GridHgt = 100;// not really
    GridVec Grid = new GridVec(GridWdt, GridHgt);

    // First create grid array of points.
    double Tilt = TwoPi * 0.01; // 1/100 of a circle
    Tilt = TwoPi * 0.1; // 1/10 of a circle
    // Tilt = TwoPi * 0.9; // 1/2 of a circle
    double GeoCircum = TwoPi * GeoRadius;
    Point3D GeoCenter = new Point3D(0, 0, 0);

    for (int GridY = 0; GridY < GridHgt; GridY++) { // Y is cylinder height.
      double AlongAxis = (((double) GridY) / (double) GridHgt) - 0.5;   // -0.5 to 0.0 to +0.5
      double SpinCenterY = AlongAxis * (Math.sin(Tilt) / Math.cos(Tilt)); // Tangent
      Point3D SpinCenter = new Point3D(0.0, SpinCenterY, AlongAxis);
      for (int GridX = 0; GridX < GridWdt; GridX++) { // X is circumference.
        double FractAngle = ((double) GridX) / (double) GridWdt;
        double Angle = FractAngle * TwoPi;
        // Get xy loc on edge of real cylinder.
        double GeoEdgeX = GeoRadius * Math.cos(Angle);
        double GeoEdgeY = GeoRadius * Math.sin(Angle);
        Point3D GeoEdge = new Point3D(GeoEdgeX, GeoEdgeY, AlongAxis);
        double Height = SpinCenter.Distance(GeoEdge);// get xy dist from SpinCenter
        double FlatX = GeoCircum * FractAngle;
        double FlatY = AlongAxis;
        double FlatZ = Height;
        Point3D MapPoint = new Point3D(FlatX, FlatY, FlatZ);
        Grid.Set(GridX, GridY, MapPoint);
      }
    }
    SaveObj(Grid, "Howdy.obj");

  }
}
