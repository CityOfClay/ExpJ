/*
 */
package expj;

/**
 *
 * @author MultiTool
 */
public class Verlet {

  public static class time_vel {
    public double time, vel;
  };

  /* ********************************************************************************* */
  public static class Body {// https://en.wikipedia.org/wiki/Verlet_integration#Algorithmic_representation
    double pos = 0.0;
    double vel = 2.0; // 2 m/s along x-axis
    double acc = 0.0; // no acceleration at first
    double mass = 1.0; // 1kg
    double drag = 0.1; // rho*C*Area – simplified drag for this example
    double new_pos, new_acc, new_vel;

    /*
     * Update pos and vel using "Velocity Verlet" integration dt DeltaTime / time step [eg: 0.01]
     */
    public void update(double dt) {
      this.new_pos = this.pos + this.vel * dt + this.acc * (dt * dt * 0.5);
      this.new_acc = this.apply_forces(-9.81); // only needed if acceleration is not constant
      this.new_vel = this.vel + (this.acc + this.new_acc) * (dt * 0.5);// why is this different? 
    }
    public void Rollover() {
      this.pos = this.new_pos;
      this.vel = this.new_vel;
      this.acc = this.new_acc;
    }
    public double apply_forces(double grav_acc) {
//      double grav_acc = -9.81; // 9.81 m/s² down in the z-axis
      double drag_force = 0.5 * drag * (vel * vel); // D = 0.5 * (rho * C * Area * vel^2)
      double drag_acc = drag_force / mass; // a = F/m
      return grav_acc - drag_acc;
    }
  };

  /* ********************************************************************************* */
  public static class Body3D {// https://en.wikipedia.org/wiki/Verlet_integration#Algorithmic_representation
    // Wrong wikipedia Verlet. Superseded on 20241024.
    public static class Vec3d {// try this
    }
//    Vec3d pos = new Vec3d{ 0.0, 0.0, 0.0 };
//    Vec3d vel { 2.0, 0.0, 0.0 }; // 2 m/s along x-axis
//    Vec3d acc { 0.0, 0.0, 0.0 }; // no acceleration at first
    double mass = 1.0; // 1kg
    double drag = 0.1; // rho*C*Area – simplified drag for this example

    /**
     * Update pos and vel using "Velocity Verlet" integration
     *
     * @param dt DeltaTime / time step [eg: 0.01]
     */
    public void update(double dt) {
//      Vec3d new_pos = pos + vel*dt + acc*(dt*dt*0.5);
//      Vec3d new_acc = apply_forces(); // only needed if acceleration is not constant
//      Vec3d new_vel = vel + (acc+new_acc)*(dt*0.5);
//      pos = new_pos;
//      vel = new_vel;
//      acc = new_acc;
    }

//    public Vec3d apply_forces() {
//      Vec3d grav_acc = Vec3d{0.0, 0.0, -9.81 }; // 9.81 m/s² down in the z-axis
//      Vec3d drag_force = 0.5 * drag * (vel * vel); // D = 0.5 * (rho * C * Area * vel^2)
//      Vec3d drag_acc = drag_force / mass; // a = F/m
//      return grav_acc - drag_acc;
//    }
  };

  public static class VerletValues {
    public double time;
    public double vel;

    public VerletValues(double time, double vel) {
      this.time = time;
      this.vel = vel;
    }
  }

  public static VerletValues velocity_verlet(double pos, double acc, double dt) {
    // Note that we are using a temp variable for the previous position
    // https://www.algorithm-archive.org/contents/verlet_integration/verlet_integration.html
    double time, vel;
    vel = 0;
    time = 0;
    int cnt = 0;
    while (pos > 0) {
      if (cnt == 25) {
        dt *= 2.0;
      }
      time += dt;
      pos += vel * dt + 0.5 * acc * dt * dt;
      vel += acc * dt;
//      System.out.println("time:" + time + ", vel:" + vel + ", pos:" + pos);
      System.out.println("" + time + ", " + vel + ", " + pos);
      cnt++;
    }
    return new VerletValues(time, vel);
  }

  /* ********************************************************************************* */
  public void velocity_verlet(double pos, double acc, double dt, time_vel result) {
    // https://www.algorithm-archive.org/contents/verlet_integration/verlet_integration.html
    double prev_pos = pos;
    double time = 0.0;
    double vel = 0.0;
    while (pos > 0.0) {
      time += dt;
      pos += vel * dt + 0.5 * acc * dt * dt;
      vel += acc * dt;// why is this different? 
    }
    result.time = time;
    result.vel = vel;
  }

  /* ********************************************************************************* */
  public static void spring_test() {// Not Verlet related
    double KConst = 1.0, DeltaX, Energy;
    /*
    Goal is to measure the energy of a spring,
    then divide spring in parts, and sum the energy of those parts.
     */
    DeltaX = 1.0;
//    Energy = 0.5 * KConst * (DeltaX * DeltaX);

    for (double cnt = 1.0; cnt < 10.0; cnt++) {
      double KConstFrac = KConst * cnt;
      double DeltaXFrac = DeltaX / cnt;
      Energy = 0.5 * KConstFrac * (DeltaXFrac * DeltaXFrac);
      Energy *= cnt;
      System.out.println("Fraction:" + cnt + " Energy:" + Energy);
    }

    // Next divide spring in 2 with inequal displacements.
    // double KConst0 = KConst * 2.0, KConst1 = KConst * 2.0;
    double DeltaX0 = 1.0, DeltaX1 = 3.0;
    DeltaX = DeltaX0 + DeltaX1;

    double KConst0 = DeltaX0, KConst1 = DeltaX1;// K constant must be proportional to displacement.
//    KConst = 1.0 / KConst0 + 1.0 / KConst1;

    double Energy0 = 0.5 * KConst0 * (DeltaX0 * DeltaX0);
    double Energy1 = 0.5 * KConst1 * (DeltaX1 * DeltaX1);
    double EnergySum = Energy0 + Energy1;

    Energy = 0.5 * KConst * (DeltaX * DeltaX);

    System.out.println("EnergySum:" + EnergySum + " Energy:" + Energy);
  }
}


// latest wikipedia Verlet. Best wiki version so far on 20241024.
//struct Body
//{ 
//    Vec3d pos { 0.0, 0.0, 0.0 };
//    Vec3d vel { 2.0, 0.0, 0.0 }; // 2 m/s along x-axis
//    Vec3d acc { 0.0, 0.0, 0.0 }; // no acceleration at first
//    double mass = 1.0; // 1kg
//
//    /**
//     * Updates pos and vel using "Velocity Verlet" integration
//     * @param dt DeltaTime / time step [eg: 0.01]
//     */
//    void update(double dt)
//    {
//        Vec3d new_pos = pos + vel*dt + acc*(dt*dt*0.5);
//        Vec3d new_acc = apply_forces();
//        Vec3d new_vel = vel + (acc+new_acc)*(dt*0.5);
//        pos = new_pos;
//        vel = new_vel;
//        acc = new_acc;
//    }
//
//    /**
//     * To apply velocity to your objects, calculate the required Force vector instead
//     * and apply the accumulated forces here.
//     */
//    Vec3d apply_forces() const
//    {
//        Vec3d new_acc = Vec3d{0.0, 0.0, -9.81 }; // 9.81 m/s² down in the z-axis
//        // Apply any other forces here...
//        // NOTE: Avoid depending on `vel` because Velocity Verlet assumes acceleration depends on position.
//        return new_acc;
//    }
//};

