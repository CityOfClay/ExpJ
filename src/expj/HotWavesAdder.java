package expj;

import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
public class HotWavesAdder {
  public static class Vector extends ArrayList<Double> {
    public double MassField = 0.0;// or call them ScalarMassField and VectorMassField?
    public void Add(Vector other) {
      if (this.size() != other.size()) {
        System.out.println("Vector size mismatch in Vector.Add.");
        System.exit(1);
      }
      int NDims = this.size();// Math.min(this.size(), other.size());// wrong way. if other has more dims, self needs to increase dims too.
      for (int cnt = 0; cnt < NDims; cnt++) {
        double VectEnergy, AbsEnergy = 0;
        AbsEnergy = Math.abs(this.get(cnt)) + Math.abs(other.get(cnt));
        VectEnergy = this.get(cnt) + other.get(cnt);
        this.MassField += AbsEnergy - VectEnergy;// All energy lost to opposing vectors becomes apparent mass.
        this.set(cnt, VectEnergy);
      }
    }
  }
  public void Add() {

  }
}
/*

 for starters, create vector type.
 to add two vectors
 TotalMassField = 0;
 for each dimension
 . get absolute value of each vector's length in that dimension, 
 . add the two absolute values to get TotalEnergy
 . then add the original (signed) lengths to get final length
 . MassField = TotalEnergy - FinalLength; 
 . TotalMassField += MassField;


 */
