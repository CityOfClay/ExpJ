package expj;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Multitool Csv Parser
 */
public class Csv {

  public HashMap<String, Integer> HeaderLut = null;
  public ArrayList<String> HeaderTxt = null;
  public ArrayList<ArrayList<String>> Table = null;

  /* ********************************************************************************************************* */
  public static class Special {

    public static String Empty = "";
    public static String NewLine = "\n";
    public static char Quote = '"';
    public static char Delim = ',';
    public static char EolCr = '\r';
    public static char EolLf = '\n';
  }

  /* ********************************************************************************************************* */
  public Csv() {
  }

  /* ********************************************************************************************************* */
  public void Import(String CsvFileName) {
    String CsvTxt = ReadFileAsString(CsvFileName);
    this.Table = ParseCsv(CsvTxt);
    this.HeaderTxt = this.Table.remove(0);
    this.HeaderLut = ConvertHeader(this.HeaderTxt);
  }

  /* ********************************************************************************************************* */
  public void Merge(Csv other) {
    for (int rcnt = 0; rcnt < other.Table.size(); rcnt++) {
      ArrayList<String> Row = new ArrayList<String>(other.Table.get(rcnt));
      this.Table.add(Row);
    }
  }

  /* ********************************************************************************************************* */
  public static HashMap<String, Integer> ConvertHeader(ArrayList<String> TextHeader) {
    HashMap<String, Integer> HeaderLut = new HashMap<String, Integer>();
    for (int rcnt = 0; rcnt < TextHeader.size(); rcnt++) {
      String Item = TextHeader.get(rcnt);
      HeaderLut.put(Item, rcnt);
    }
    return HeaderLut;
  }

  /* ********************************************************************************************************* */
  public String GetItem(ArrayList<String> Row, String Label) {
    Integer rcnt = this.HeaderLut.get(Label);
    return Row.get(rcnt);
  }

  /* ********************************************************************************************************* */
  public void SetItem(ArrayList<String> Row, String Label, String Value) {
    Integer rcnt = this.HeaderLut.get(Label);
    Row.set(rcnt, Value);
  }

  /* ********************************************************************************************************* */
  public double GetNumber(ArrayList<String> Row, String Label) {
    String Item = this.GetItem(Row, Label);
    Item = Item.replaceAll(",", "").trim();// Remove commas
    double retval = 0.0;
    try {
      retval = Double.parseDouble(Item);
    } catch (Exception ex) {
      System.out.println("");
    }
    return retval;
  }

  /* ********************************************************************************************************* */
  public void PrintMe() {
    PrintRow(this.HeaderTxt);
    for (int rcnt = 0; rcnt < this.Table.size(); rcnt++) {
      ArrayList<String> Row = this.Table.get(rcnt);
      System.out.print(rcnt + ", ");
      PrintRow(Row);
      System.out.println();
    }
  }

  /* ********************************************************************************************************* */
  public static void PrintRow(ArrayList<String> Row) {
    for (int ccnt = 0; ccnt < Row.size(); ccnt++) {
      String Item = Row.get(ccnt);
      System.out.print(" [" + Item + "] ");
    }
  }

  /* ********************************************************************************************************* */
  public static String ReadFileAsString(String FileName) {// throws Exception 
    String data = "";// https://www.geeksforgeeks.org/different-ways-reading-text-file-java/
    try {
      data = new String(Files.readAllBytes(Paths.get(FileName)));
    } catch (IOException e) {
      System.out.println(e.getMessage());
      System.out.println(e.getStackTrace());
      System.exit(1);
    }
    return data;
  }

  /* ********************************************************************************************************* */
  public static ArrayList<ArrayList<String>> ParseCsv(String CsvTxt) {// 7:21 AM 12/8/2014 attempt
    int TxtLen = CsvTxt.length();
    if (CsvTxt.charAt(TxtLen - 1) != Special.EolLf) {//  if it doesn't end with eol, add an eol to force processing of last row
      CsvTxt += Special.EolLf;
      TxtLen = CsvTxt.length();
    }

    ArrayList<ArrayList<String>> Rows = new ArrayList<ArrayList<String>>();
    ArrayList<String> Chunks = new ArrayList<String>();
    String OneChunk = Special.Empty;
    boolean CaseOpen = true;
    boolean InQuote = false;
    int ChCnt = 0;
    char PrevChar = Special.EolCr;
    for (int chdex = 0; chdex < TxtLen; chdex++) {
      char Chr = CsvTxt.charAt(chdex);
      if (InQuote) {
        if (Chr == Special.Quote) {// transition out of quote
          InQuote = false;
          CaseOpen = false;
        } else {
          OneChunk += Chr;
        }
      } else {
        if (Chr == Special.Quote) {// transition in to quote
          if (PrevChar == Special.Quote) {
            OneChunk += Special.Quote;
          }// for double quotes
          //else { OneChunk = String.Empty; } // to trim stuff before quote
          InQuote = true;
        } else if (Chr == Special.Delim) {// break off a cell
          Chunks.add(OneChunk);
          OneChunk = Special.Empty;
          CaseOpen = true;
        } else if (Chr == Special.EolLf) {// break off a row
          Chunks.add(OneChunk);
          OneChunk = Special.Empty;
          CaseOpen = true;
          Rows.add(Chunks);
          Chunks = new ArrayList<String>();
        } else if (Chr == Special.EolCr) {
        } else {
          //if (CaseOpen)
          {// find a way to trim stuff after quote, but before comma
            OneChunk += Chr;
          }
        }
      }
      PrevChar = Chr;
      ChCnt++;
    }
    return Rows;
  }

  /* ********************************************************************************************************* */
  public static void Test() {
    CreateFile();
    String CsvTxt;
    CsvTxt = "neptune,pluto, \"mars\" , \"Jupiter, with 10\"\" rings \",  fleb  " + Special.EolLf;
    CsvTxt += "snord, ford, gord , creb,  \"crash:" + Special.NewLine + ":splash\", ned";
    ArrayList<ArrayList<String>> List = ParseCsv(CsvTxt);
    PrintTable(List);
  }

  /* ********************************************************************************************************* */
  public static void PrintTable(ArrayList<ArrayList<String>> List) {
    for (int rcnt = 0; rcnt < List.size(); rcnt++) {
      ArrayList<String> Row = List.get(rcnt);
      for (int ccnt = 0; ccnt < Row.size(); ccnt++) {
        String Item = Row.get(ccnt);
        System.out.print(" [" + Item + "] ");
      }
      System.out.println();
    }
  }

  /* ********************************************************************************************************* */
  public static void CreateFile() {
    try {// https://www.w3schools.com/java/java_files_create.asp
      File myObj = new File("filename.txt");
      if (myObj.createNewFile()) {
        System.out.println("File created: " + myObj.getName());
      } else {
        System.out.println("File already exists.");
      }
    } catch (IOException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }
  }

}
