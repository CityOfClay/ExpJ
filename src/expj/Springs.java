package expj;

import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
public class Springs {
  public static final double TwoPi = Math.PI * 2.0;
  /* **************************************************************************** */
  public static class Unit {
    public static final int NumNbrs = 2;
    public static final double LRate = 0.2;
    public double Height = 0;
    public Unit[] nbrs = new Unit[NumNbrs];
    public double Konst;// K constant
    /* **************************************************************************** */
    public Unit() {// constructor
      this.Konst = 1.0;
    }
    /* **************************************************************************** */
    public void AdjustMe() {
      double SumDelta = 0;
      for (int ncnt = 0; ncnt < NumNbrs; ncnt++) {
        Unit nbr = this.nbrs[ncnt];
        double Displacement = nbr.Height - this.Height;
        SumDelta += Displacement;
      }
      SumDelta /= this.Konst;// Greater Konst makes movement more stubborn. Response displacement is reduced. 
      this.Height += SumDelta * LRate;
    }
    /* **************************************************************************** */
    public void ApplyForce(double Force) {
      this.Height += Force * LRate;
    }
  }
  /* **************************************************************************** */
  public static class UnitList extends ArrayList<Unit> {
    public void CreateLine(int NumUnits) {
      Unit cell;
      for (int cnt = 0; cnt < NumUnits; cnt++) {
        cell = new Unit();
        cell.Konst = cnt + 1.0;
        this.add(cell);
      }
    }
  }
  /* ********************************************************************************* */
  public static class Cell {
    public static final int NDims = 2;
    public static final int NAxis = 2;
    public static final int NumNbrs = NDims * NAxis;
    public Cell[][] NbrCross;// another way
    protected int XDex, YDex;
    public double XLoc = 0, YLoc = 0, Wdt = 10, Hgt = 10;// for drawing
    public double XCtr = 0.0, YCtr = 0.0;
    protected double Amp = 0.0;
    /* ********************************************************************************* */
    public void Init_NbrCross() {
      this.NbrCross = new Cell[NDims][NAxis];
      for (int dcnt = 0; dcnt < NDims; dcnt++) {
        for (int axcnt = 0; axcnt < NAxis; axcnt++) {
          this.NbrCross[dcnt][axcnt] = null;
        }
      }
    }
    /* ********************************************************************************* */
    public void Set_Dex(int XDex0, int YDex0) {
      this.XDex = XDex0;
      this.YDex = YDex0;
    }
    /* ********************************************************************************* */
    public double Get_Amp() {
      return this.Amp;
    }
    /* ********************************************************************************* */
    public void Set_Amp(double Amp0) {
      this.Amp = Amp0;
    }
  }
  /* ********************************************************************************* */
  public static class CellGrid {

    /* ********************************************************************************* */
//    public void ConnectRows(CellRow otherprev) {
//      int NumCells = this.cells.length;
//      if (NumCells != otherprev.cells.length) {
//        System.out.println("Row sizes do not match!!!");// would be throw but I hate catch. 
//      }
//      for (int cnt = 0; cnt < NumCells; cnt++) {
//        CellBase cell0 = otherprev.cells[cnt];
//        CellBase cell1 = this.cells[cnt];
//        cell1.ConnectCross(cell0, 1, 0);// 1 is ydim, 0 is north axis
//      }
//    }
//    /* ********************************************************************************* */
//    public void ConnectCells() {// wrapped horizontal connections
//      int NumCells = this.cells.length;
//      CellBase cell_prev = null, cell_now = this.Get(NumCells - 1);
//      for (int XCnt = 0; XCnt < NumCells; XCnt++) {
//        cell_prev = cell_now;
//        cell_now = this.Get(XCnt);
//        cell_now.ConnectCross(cell_prev, 0, 0);// 0 is xdim, 0 is west axis
//      }
//    }
  }
  
  public static void Experiment() {// unrelated to everything here, except that it is about springs.
    // energy = 0.5 * K * disp^2;
    // k0 = 1.0 / d0;
    // k1 = 1.0 / d1;
    // energy = 0.5*(d0^d0 * k0) + 0.5*(d1^d1 * k1)
    /*
    in halves, 
    k0=2*k
    k1=2*k
    e0 = 0.5 * k0 * d0^2 
    e1 = 0.5 * k1 * d1^2 
    etot = 0.5 * k * (d0+d1)^2
    
    
    */
    
  }
}

/*
junkyard
for 1d springs, read desired displacement in response to evey neighbor.
add the desired displacements together.
but unless we average, energy builds up?
could average on intake, or on cell output to neighbors.


tests
 Put springs on 2d grid, compare to hyperobolas.
 2d topology works for 1/r metric, but 3d creates 1/(r*r) metric.
 The metric is entirely a product of how the force is spread out.
 So can we make the force spread out less, per hop? 
 We can make it disperse more, by adding an extra dimension and hiding it.

 */
