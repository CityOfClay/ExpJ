package expj;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author MultiTool
 */
public class EnergyMesh {
  public static final double TwoPi = Math.PI * 2.0;
  public static final int NDimsVector = 2;
  public static final int NDims = 1;
//  public static final int NDims = 2;
  public static final int NAxis = 2;
//  public int GridWidth = 2;
//  public int GridHeight = 2;
  public int GridWidth = 2;
  public int GridHeight = 1;
  public int NumCells = GridWidth * GridHeight;

  public ArrayList<Cell> Grid = new ArrayList<Cell>();
  public ArrayList<Link> Links = new ArrayList<Link>();

  int GenCnt = 0;
  public double GlobalEnergy;
  Random rand = new Random();

  /* ********************************************************************************* */
  public static class Cell {
    public double VSpeed;//Height, 
    public double VelSum = 0;
    public double VelSqSum = 0;

    public Cell[][] NbrCross;
    public Link[][] NbrCrossLink;
    public double XOrg, YOrg, Wdt, Hgt;
    public int MyIdNum = -1;
    private int NumNbrs;
    /* ********************************************************************************* */
    public Cell() {
      this.Init_NbrCross();
//      this.TRate = this.TRate_Next = TRate_Standard;
    }
    /* ********************************************************************************* */
    public void Init_NbrCross() {
      this.NumNbrs = NDims * NAxis;
      this.NbrCross = new Cell[NDims][NAxis];
      this.NbrCrossLink = new Link[NDims][NAxis];
      for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
        for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
          this.NbrCross[DimCnt][AxCnt] = null;
          this.NbrCrossLink[DimCnt][AxCnt] = null;
        }
      }
    }
    /* ********************************************************************************* */
    public Link ConnectPrevNbr(Cell other, int Dimension) {// Connect me to a neighbor who has a lower index in the given dimension.
      Link link = new Link();
      link.cells[0] = other;
      link.cells[1] = this;
      this.NbrCrossLink[Dimension][0] = link;// Connect my down pointer to link to neighbor.
      other.NbrCrossLink[Dimension][1] = link;// Connect neighbor's up pointer to same link.

      this.NbrCross[Dimension][0] = other;// Connect my down pointer to neighbor.
      other.NbrCross[Dimension][1] = this;// Connect neighbor's up pointer to me.
      return link;
    }

    /* ********************************************************************************* */
    public void AssignGraphics(double XOrg, double YOrg, double Wdt, double Hgt) {
      this.XOrg = XOrg;
      this.YOrg = YOrg;
      this.Wdt = Wdt;
      this.Hgt = Hgt;
    }
    /* ********************************************************************************* */
    public void Update() {
      this.VelSum = 0;
      this.VelSqSum = 0;
    }
    /* ********************************************************************************* */
    public void Rollover() {
      this.VSpeed = this.VelSum / (double) this.NumNbrs;
      this.VelSum = 0.0;
      this.VelSqSum = 0;
    }
  }

  /* ********************************************************************************* */
  public static class Link {
    public Cell[] cells = new Cell[2];
    public double Tension = 0, Tension_Next = 0;
    public double TensionSum = 0;
    public double Cos = 0.0, Sin = 0.0;// For rotation matrix.

    /* ********************************************************************************* */
    public Link() {
    }
    public Link(Cell first, Cell second) {
      this();
      this.cells[0] = first;
      this.cells[1] = second;
    }
    /* ********************************************************************************* */
    public static double GetSumEnergy(double Vel0, double Vel1, double Tension0) {
      double VelSq0 = Vel0 * Vel0;
      double VelSq1 = Vel1 * Vel1;
      double TensionSq = Tension0 * Tension0;
      return TensionSq + VelSq0 + VelSq1;
    }
    /* ********************************************************************************* */
    public double GetSumEnergy() {
      Cell cell0 = this.cells[0];
      Cell cell1 = this.cells[1];
      return GetSumEnergy(cell0.VSpeed, cell1.VSpeed, this.Tension);
    }
    /* ********************************************************************************* */
    public void AssignAngle(double Angle) {
      this.Cos = Math.cos(Angle);// For rotation matrix
      this.Sin = Math.sin(Angle);
    }
    /* ********************************************************************************* */
    public double Interact(Cell cell, double TensionSigned) {
      double XLoc = cell.VSpeed;
      double YLoc = TensionSigned;// double YLoc = this.Tension;

      // Rotation matrix
      double XLoc_Next = (XLoc * this.Cos) - (YLoc * this.Sin);
      double YLoc_Next = (XLoc * this.Sin) + (YLoc * this.Cos);

      cell.VelSum += XLoc_Next;// this.TensionSum += YLoc_Next;
      double VelSq = XLoc_Next * XLoc_Next;
      cell.VelSqSum += VelSq;
      double EDiff = VelSq - (cell.VSpeed * cell.VSpeed);
      return YLoc_Next;
    }
    /* ********************************************************************************* */
    public void Exchange() {
      Cell cell0 = this.cells[0];
      Cell cell1 = this.cells[1];
      this.TensionSum = 0.0;
      this.TensionSum += this.Interact(cell0, this.Tension);
      this.TensionSum -= this.Interact(cell1, -this.Tension);
//      this.Tension = this.TensionSum;// / 2.0;
      this.Tension = this.TensionSum / 2.0;
    }
  }

  /* ********************************************************************************* */
  public EnergyMesh() {
    this.Init();
  }
  /* ********************************************************************************* */
  public void Init() {
    if (NDims == 1) {
      this.Connect1D();
    } else if (NDims == 2) {
      this.Connect2D();
    }
    double TRate = TwoPi * 0.13;
//    TRate = TwoPi * 0.5;
//    TRate = 0.0;
    this.AssignAngle(TRate);

    if (true) {
      this.Grid.get(0).VSpeed = 0.0;
      this.Grid.get(1).VSpeed = 0.0;
      this.Links.get(0).Tension = 2.1;
      this.Links.get(1).Tension = -2.1;
    }
    if (true) {
      this.Grid.get(0).VSpeed = 1.3;
      this.Grid.get(1).VSpeed = -1.0;
      this.Links.get(0).Tension = 0.0;
      this.Links.get(1).Tension = 0.0;
    }
    this.GlobalEnergy = this.GetSumEnergy();// GlobalEnergy = sum energy of all values, taken globally
  }
  /* ********************************************************************************* */
  public static void Test() {
    EnergyMesh fm = new EnergyMesh();
    int NumGens = 1000;
    System.out.println("NDims:" + NDims + ", NumGens:" + NumGens + ", NumCells:" + fm.NumCells);
    for (int GenCnt = 0; GenCnt < NumGens; GenCnt++) {
      fm.RunCycle();
    }
    noop();
  }
  /* ********************************************************************************* */
  public void AddCell(Cell cell) {
    cell.MyIdNum = this.Grid.size();
    this.Grid.add(cell);
  }
  /* ********************************************************************************* */
  public void AssignAngle(double Angle) {
    Link link;
    for (int LinkCnt = 0; LinkCnt < this.Links.size(); LinkCnt++) {
      link = this.Links.get(LinkCnt);
      link.AssignAngle(Angle);
    }
  }
  /* ********************************************************************************* */
  public void Connect1D() {// Loop
    Cell CellCenter;
    for (int cnt = 0; cnt < this.NumCells; cnt++) {
      CellCenter = new Cell();
      this.AddCell(CellCenter);
    }
    Link link;
    int XPrev = this.NumCells - 1;
    Cell CellPrev = this.Grid.get(XPrev);
    for (int XCnt = 0; XCnt < this.NumCells; XCnt++) {
      CellCenter = this.Grid.get(XCnt);
      link = CellCenter.ConnectPrevNbr(CellPrev, 0);
      this.Links.add(link);
      CellPrev = CellCenter;
    }
  }
  /* ********************************************************************************* */
  public void Connect2D() {// Torus 
    double CellSize = 20;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = new Cell();
      this.AddCell(cell);
    }
    Link link;
    Cell CellLeft, CellBelow, CellCenter;
    int XPrev, YPrev;
    this.Links.clear();
    YPrev = GridHeight - 1;
    for (int YCnt = 0; YCnt < GridHeight; YCnt++) {
      XPrev = GridWidth - 1;
      for (int XCnt = 0; XCnt < GridWidth; XCnt++) {
        CellCenter = this.GetXY(XCnt, YCnt);
        CellLeft = this.GetXY(XPrev, YCnt);
        CellBelow = this.GetXY(XCnt, YPrev);
        link = CellCenter.ConnectPrevNbr(CellLeft, 0);
        this.Links.add(link);
        link = CellCenter.ConnectPrevNbr(CellBelow, 1);
        this.Links.add(link);
        CellCenter.AssignGraphics(XCnt * CellSize, YCnt * CellSize, CellSize, CellSize);
        XPrev = XCnt;
      }
      YPrev = YCnt;
    }
  }
  /* ********************************************************************************* */
  public void Seed(double SeedVal) {
    // Seed the grid with a vertical speed. Normally 1. 
    Cell cell;// Seed energy 
    for (int cnt = 0; cnt < 1; cnt++) {
      cell = this.Grid.get(cnt);
      cell.VSpeed = SeedVal;
    }
  }
  /* ********************************************************************************* */
  public void Randomize(double Amp) {
    double Range = Amp * 2.0;// Random numbers will be from -Amp to +Amp. 
    Cell cell;
    for (int cnt = 0; cnt < this.Grid.size(); cnt++) {
      cell = this.Grid.get(cnt);
      cell.VSpeed = rand.nextDouble() * Range - Amp;
      cell.VSpeed = 1.0;
    }
    Link link;
    for (int cnt = 0; cnt < this.Links.size(); cnt++) {
      link = this.Links.get(cnt);
      link.Tension = rand.nextDouble() * Range - Amp;
      link.Tension = 1.0;
    }
  }
  /* ********************************************************************************* */
  public double GetSumEnergy() {
    double Sum = 0;
    Cell cell;
    for (int cnt = 0; cnt < this.Grid.size(); cnt++) {
      cell = this.Grid.get(cnt);
      Sum += cell.VSpeed * cell.VSpeed;// SQUARE
    }
    Link link;
    for (int cnt = 0; cnt < this.Links.size(); cnt++) {
      link = this.Links.get(cnt);
      Sum += link.Tension * link.Tension;// SQUARE
    }
    return Sum;
  }
  /* ********************************************************************************* */
  public Cell GetXY(int XLoc, int YLoc) {
    Cell cell = this.Grid.get((YLoc * GridWidth) + XLoc);
    return cell;
  }
  /* ********************************************************************************* */
  public void UpdateLinks() {
    int NumLinks = this.Links.size();
    Link link;
    for (int cnt = 0; cnt < NumLinks; cnt++) {
      link = this.Links.get(cnt);
      link.Exchange();
    }
  }
  /* ********************************************************************************* */
  public void RunCycle() {// generation loop 
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Update();
    }
    this.UpdateLinks();
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Rollover();
    }
    double EnergyNow = this.GetSumEnergy();
    System.out.println("" + this.GenCnt + ", " + this.GlobalEnergy + ", " + EnergyNow + "");
    this.GenCnt++;
  }
  /* ********************************************************************************* */
  public static void WaveMetric() {// Calc_Amp from HotWaves 
    double Amp, Amp_Next;
    double OtherAmp = 0;
    double Speed = 0.0, Speed_Next = 0.0;
    double TRate = 0.1;
    double TRateSquared = TRate * TRate;
    double Damp = 1.0;
    int NumGens = 100;
    Amp = 1.0;
    int GenCnt = 0;
    for (GenCnt = 0; GenCnt < NumGens; GenCnt++) {// Calc_Amp and Rollover from bench testing.  this works.
      double Tension = OtherAmp - Amp;// Spring force
      Tension *= TRateSquared;
      Amp_Next = Amp + Speed + Tension;
      Speed_Next = Amp_Next - Amp;
      Speed_Next *= Damp;

      double SpeedAdj = Speed / TRate;
      double Energy = Amp * Amp + SpeedAdj * SpeedAdj;

      System.out.print("" + GenCnt + "");
      System.out.print(", " + Amp + "");
      System.out.print(", " + Tension + "");
      System.out.print(", " + SpeedAdj + "");
      System.out.print(", " + Energy + "");

//      So Cell's future speed (no damp) will be internal prevs added to Delta. 
//      So we can accumulate deltas link for link, until the last step. 
      {// Rollover 
        Amp = Amp_Next;
        Speed = Speed_Next;
      }
      System.out.println();
    }
  }
  /* ********************************************************************************* */
  public static void noop() {
  }
}
