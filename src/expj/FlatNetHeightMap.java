package expj;

import java.util.ArrayList;

/**
 *
 * @author MultiTool
 */
/* ********************************************************************************* */
public class FlatNetHeightMap {
  /* ********************************************************************************* */
  public static class Cell {
    public double Height;
    public double Usage;
  }
  /* ********************************************************************************* */
  public ArrayList<Cell> Grid = new ArrayList<Cell>();
  public int GridDiameter = 10;
  public int GridWdt = GridDiameter, GridHgt = GridDiameter;
  public int GridMaxX = GridWdt - 1, GridMaxY = GridHgt - 1;
  public int GridSize = GridWdt * GridHgt;
  /* ********************************************************************************* */
  public FlatNetHeightMap() {
    this.Init();
  }
  /* ********************************************************************************* */
  public void Init() {
    Cell cell;
    for (int cnt = 0; cnt < GridSize; cnt++) {
      cell = new Cell();
      this.Grid.add(cell);
    }
  }
  /* ********************************************************************************* */
  public Cell GetXY(int XLoc, int YLoc) {
    Cell cell = this.Grid.get((YLoc * GridWdt) + XLoc);
    return cell;
  }
  /* ********************************************************************************* */
  public Cell MapToXY(double XLoc, double YLoc) {
    // Assumed range of Locs is 0.0 to 1.0 
    XLoc = XLoc * this.GridMaxX;
    YLoc = YLoc * this.GridMaxY;
    int XInt = (int) Math.round(XLoc);
    int YInt = (int) Math.round(YLoc);
    Cell cell = this.GetXY(XInt, YInt);
    return cell;
  }
}
