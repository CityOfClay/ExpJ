package expj;
 
import java.util.ArrayList;
import java.util.Arrays;

public class data_grid {
 
  /* ********************************************************************************************************* */
  public static class data_line extends ArrayList<String> {
    public data_line() {
      super();
    }
 
    public data_line(String[] init) {
      super(Arrays.asList(init));
    }
 
    public void print_me() {
      System.out.println(this.toString());
    }
 
    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      int ndx = 0;
      sb.append(this.get(ndx++));
      while (ndx < this.size()) {
        // sb.append(", " + this.get(ndx++));
        sb.append(" ").append(this.get(ndx++));
      }
      return sb.toString();
    }
 
    public static data_line split_space(String line) {
      return new data_line(line.trim().split("\\s+"));// split on all whitespace
    }
  }
 
  /* ********************************************************************************************************* */
  public static class data_table extends ArrayList<data_line> {// This is to represent the whole file as a list of lines, each of which is split into a list of fields.
    public void consume(String txt) {
      ArrayList<String> lines = new ArrayList<String>(Arrays.asList(txt.split("\n")));
      consume(lines);
    }
    public void consume(ArrayList<String> lines) {
      for (String line:lines) {
        this.add(data_line.split_space(line));
      }
    }
 
    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      String linetxt;
      int ndx = 0;
      sb.append(this.get(ndx++));
      while (ndx < this.size()) {
        linetxt = this.get(ndx++).toString();
        sb.append(System.lineSeparator()).append(linetxt);
      }
      sb.append(System.lineSeparator());
      return sb.toString();
    }
 
  }
 
}
 
