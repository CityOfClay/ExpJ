package expj;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 *
 * @author MultiTool
 */
public class HubbleEnergy {

  public static double SecondsPerHour = 60.0 * 60.0;
  public static double SecondsPerMillenium = SecondsPerHour * 24.0 * 365.0 * 1000.0;
  public static double KmPerLightYear = 9.461 * 10E12;// https://en.wikipedia.org/wiki/Light-year
  public static double KmPerParsec = 31000000000000.0;// https://en.wikipedia.org/wiki/Parsec  //31 trillion kilometres
  public static double KmPerParsec2 = 3.086 * 10E13;//3.086e+13
  public static double KmPerMegaParsec = KmPerParsec * 1000000;
  public static double ExpansionPerMegaParsecKm = 74.0;
  public static double GConst = 1.0;

  // https://www.livescience.com/hubble-constant.html
  public static double HubbleConstMaybe = ExpansionPerMegaParsecKm / KmPerMegaParsec; //73.4 km/s/Mpc
  public static double HubbleConstCepheids = 73.4 / KmPerMegaParsec; //73.4 km/s/Mpc
  public static double HubbleConst2018 = 67.4 / KmPerMegaParsec; //67.4 km/s/Mpc
  public static double HubbleConst2019 = 69.8 / KmPerMegaParsec; //69.8 km/s/Mpc
  public static double HubbleConst = HubbleConst2019;

//3.1220411e+19 KmPerParsec?
  public static BigDecimal SquarePower(BigDecimal Value, int Power) {
    int bsize = Integer.SIZE;// sizeof(Power)
    int bfirst = bsize - 1;// skip sign bit
    int Flag = 0;
    BigDecimal Snowball = new BigDecimal(1.0);
    int Mask;
    for (int cnt = bfirst; cnt >= 0; cnt--) {
      Mask = 1 << cnt;// scan left to right, high bit to low bit
      Flag = Mask & Power;
      Snowball = Snowball.multiply(Snowball);// square it
      if (Flag != 0) {
        Snowball = Snowball.multiply(Value);
      }
      // System.out.println(" cnt:" + cnt + ", Snowball:" + Snowball);
    }
    return Snowball;
  }
  public static double SquarePower(double Value, int Power) {
    int bsize = Integer.SIZE;// sizeof(Power)
    int bfirst = bsize - 1;// skip sign bit
    int Flag = 0;
    double Snowball = 1.0;
    int Mask;
    for (int cnt = bfirst; cnt >= 0; cnt--) {
      Mask = 1 << cnt;// scan left to right, high bit to low bit
      Flag = Mask & Power;
      Snowball *= Snowball;// square it
      if (Flag != 0) {
        Snowball *= Value;
      }
      //System.out.println(" cnt:" + cnt + ", Snowball:" + Snowball);
    }
    return Snowball;
  }
  public static double SquarePowerLong(double Value, long Power) {// apparently functional
    int bsize = Long.SIZE;// sizeof(Power)
    long Divided = 0;
    double Snowball = 1.0;
    long Mask = 0x4000000000000000L;// skipped sign bit. otherwise would start with 8.
    long Remainder = 0;
    int cnt = 0;
    while (Mask > 0) {
      Snowball *= Snowball;// square it
      Divided = Power / Mask;
      if (Divided != 0) {// Divided can only be 0 or 1.
        Snowball *= Value;
        Remainder = Power - Mask;// watch out could be negative
        Power = Remainder;
      }
      Mask /= 2;// scan left to right, high bit to low bit
//      System.out.println(" cnt:" + cnt + ", Snowball:" + Snowball + ", Mask:" + Mask + ", Power:" + Power + ", Divided:" + Divided + ", Remainder:" + Remainder);
      cnt++;
    }
    return Snowball;
  }
  public static BigDecimal SquarePowerLong(BigDecimal Value, long Power) {// apparently functional
    int bsize = Long.SIZE;// sizeof(Power)
    long Divided = 0;
    BigDecimal Snowball = new BigDecimal(1.0);
    long Mask = 0x4000000000000000L;// skipped sign bit. otherwise would start with 8.
    long Remainder = 0;
    int cnt = 0;
    while (Mask > 0) {
      Snowball = Snowball.multiply(Snowball);// square it
      Divided = Power / Mask;
      if (Divided != 0) {// Divided can only be 0 or 1.
        Snowball = Snowball.multiply(Value);
        Remainder = Power - Mask;// watch out could be negative
        Power = Remainder;
      }
//      Snowball = Snowball.setScale(30);
      Snowball = Snowball.round(MathContext.DECIMAL128);// BigDecimal gets bogged down by increasing precision.
      Mask /= 2;// scan left to right, high bit to low bit
//      System.out.println(" cnt:" + cnt + ", Snowball:" + Snowball + ", Mask:" + Mask + ", Power:" + Power + ", Divided:" + Divided + ", Remainder:" + Remainder);
      cnt++;
    }
    return Snowball;
  }
  public static void Calc() {

    double Power;

    Power = SquarePower(2.0, 4);/// Power:16.0
    System.out.println("Power:" + Power);

    Power = SquarePower(3.0, 6);/// Power:729.0
    System.out.println("Power:" + Power);

    Power = SquarePower(3.0, 7);// Power:2187.0
    System.out.println("Power:" + Power);

    Power = SquarePower(2.0, 15);// Power:32768.0
    System.out.println("Power:" + Power);

    Power = SquarePowerLong(2.0, 2);// should be 4
    System.out.println("SquarePowerLong:" + Power);

    Power = SquarePowerLong(2.0, 6);// should be 64
    System.out.println("SquarePowerLong:" + Power);

    Power = SquarePowerLong(3.0, 6);// Power:729.0
    System.out.println("SquarePowerLong:" + Power);

    Power = SquarePowerLong(3.0, 7);// Power:2187.0
    System.out.println("SquarePowerLong:" + Power);

    Power = SquarePowerLong(2.0, 15);// Power:32768.0
    System.out.println("SquarePowerLong:" + Power);

    double Mass = 1.0, Velocity, Velocity2;
    double Energy = 1.0, Energy2;
    BigDecimal bd = new BigDecimal(HubbleConst);
    BigDecimal Planck = new BigDecimal(1.616255 * 10E-35);

    Velocity = EnergyToSpeed(Mass, Energy);
    System.out.println("Energy:" + fmt(Energy));
    System.out.println("Velocity:" + fmt(Velocity));

    Energy2 = SpeedToEnergy(Mass, Velocity);
    Velocity2 = EnergyToSpeed(Mass, Energy2);
    System.out.println("Energy2:" + fmt(Energy2));
    System.out.println("Velocity2:" + fmt(Velocity2));

    if (false) {
      double Height;
      for (Energy = 0.0; Energy < 10; Energy += 0.1) {
        Height = EnergyToHeight(Mass, Energy);
        System.out.println("Height:" + fmt(Height) + ",  Energy:" + fmt(Energy));
      }
    }
    if (false) {
      double Time;
      for (Energy = 0.0; Energy < 10; Energy += 0.1) {
        Time = EnergyToTime(Mass, Energy);
        System.out.println("Time:" + fmt(Time) + ",  Energy:" + fmt(Energy) + ",  Ratio:" + fmt(Energy / Time));
      }
    }
    if (true) {
      double Time;
      for (Time = 1.0; Time > 0.0001; Time *= 0.1) {
        Energy = TimeToEnergy(Mass, Time);
        System.out.println("Time:" + fmt(Time) + ",  Energy:" + fmt(Energy) + ",  Ratio:" + fmt(Energy / Time));
      }
    }

    System.out.println("SecondsPerMillenium:" + fmt(SecondsPerMillenium));// SecondsPerMillenium:3.1536E10

    double test = SecondsPerMillenium / KmPerMegaParsec;// test:0.00000000101729032258
    System.out.println("test:" + fmt(test));

    System.out.println("HubbleConst:" + fmt(HubbleConst));

    // 3.3 million light-years farther away a galaxy is from us, it appears to be moving 74 kilometers (46 miles) per second faster
    double ExpansonRatioSeconds = HubbleConst + 1.0;
    System.out.println("ExpansonRatioSeconds:" + fmt(ExpansonRatioSeconds));

    double ExpansonRatioMillenia;

    if (true) {
      BigDecimal HubbleConstBig = new BigDecimal(HubbleConst);
      long SecondsPerMilleniumLong = (long) Math.round(SecondsPerMillenium);// good, no overflow

//      ExpansonRatioMillenia = SquarePower(ExpansonRatioSeconds, SecondsPerMilleniumLong);
//      System.out.println("ExpansonRatioMillenia:" + ExpansonRatioMillenia);
      BigDecimal SecondsPerMilleniumBig = new BigDecimal(SecondsPerMillenium);

      BigDecimal ExpansonRatioSecondsBig = new BigDecimal(HubbleConst).add(BigDecimal.ONE);

      System.out.println("ExpansonRatioSecondsBig:" + ExpansonRatioSecondsBig);// ExpansonRatioSecondsBig:1.000000000000000002251612903225806510810841619920759086563048253818125447878184530736689339391887187957763671875

      BigDecimal ExpansonRatioMilleniaBig = new BigDecimal(1.0);
      if (false) {
        for (double cnt = 0; cnt < SecondsPerMillenium; cnt++) {
          ExpansonRatioMilleniaBig = ExpansonRatioMilleniaBig.multiply(ExpansonRatioSecondsBig);
          System.out.println("ExpansonRatioMilleniaBig:" + ExpansonRatioMilleniaBig);
        }
      }

      if (true) {
        ExpansonRatioMilleniaBig = SquarePowerLong(ExpansonRatioSecondsBig, SecondsPerMilleniumLong);// ExpansonRatioMilleniaBig:1.000000071006867037116511729903745
        System.out.println("ExpansonRatioMilleniaBig Power:" + ExpansonRatioMilleniaBig);
      }

      ExpansonRatioMilleniaBig = HubbleConstBig.multiply(SecondsPerMilleniumBig).add(BigDecimal.ONE);// ExpansonRatioMilleniaBig:1.000000071006864516129034124930701325821058553852289732408404124286427361312235007062554359436035156250000000000
//      ExpansonRatioMilleniaBig = ExpansonRatioSecondsBig.pow(SecondsPerMilleniumInt);
//      ExpansonRatioMillenia = Math.pow(ExpansonRatioSeconds, SecondsPerMilleniumLong);
//      System.out.println("ExpansonRatioMillenia Math.pow:" + fmt(ExpansonRatioMillenia));

//      ExpansonRatioMilleniaBig = SquarePower(ExpansonRatioSecondsBig, SecondsPerMilleniumLong);
      System.out.println("ExpansonRatioMilleniaBig Linear: " + ExpansonRatioMilleniaBig);
      /*
       ExpansonRatioMilleniaBig Power:  1.000000071006867037116511729903745
       ExpansonRatioMilleniaBig Linear: 1.000000071006864516129034124930701325821058553852289732408404124286427361312235007062554359436035156250000000000
       */

      System.out.println();
    }

    ExpansonRatioMillenia = (HubbleConst * SecondsPerMillenium) + 1.0;// ExpansonRatioMillenia:1.000000071006864600000000000000
    System.out.println("ExpansonRatioMillenia:" + fmt(ExpansonRatioMillenia));

    //1.00000007476 * 1.00000007476 * 1.00000007476 = 1.00000022428 = cubic ratio of space per 1k years (2000ad)/(1000ad)
    double ExpansonRatioMilleniaCubed = Math.pow(ExpansonRatioMillenia, 3.0);// ExpansonRatioMilleniaCubed:1.00000021302060900000
    System.out.println("ExpansonRatioMilleniaCubed:" + fmt(ExpansonRatioMilleniaCubed));

    /*
     so what next?
     toss 1 brick 1 meter up for 1 second
     or 1kg brick 1km up for 1k years
     how to keep same energy when
     time is divided 
     space is divided - seems to be linear, write down 
     mass is divided (done, easy)

     Is this true?
     A: 1kg brick 1m up for 1k years gains energy by X percent - given
     1kg brick 1m tossed up for 1 years gains energy by X/(1000*1000) percent
     1kg brick 1m up for 1 years gains energy by X/1000 percent
     1000k brick 1m up for 1 years gains energy by X percent of A
     1000 1kg bricks 1m up for 1 years gains energy by X percent of A
     (1000*1000) 1g bricks 1m up for 1 years gains energy by X percent of A
     (1000*1000*1000) 1g bricks 1mmm up for 1 years gains energy by X percent of A
     time, distance, mass
    
     */
    System.out.println();
    /*
     ( (74 * 3.154e+10) + 3.1220411e+19) / 3.1220411e+19 = 1.00000007476 = linear ratio of space per 1k years (2000ad)/(1000ad)
     */
  }
  public static String fmt(double val) {
    String str = String.format("%.020f", val);
//    String str = String.format("%.040f", val);
    return str;
  }

  // All of these are for a projectile's mass, energy, speed, and time of flight.
  public static double EnergyToSpeed(double Mass, double Energy) {
    double VSquared, Velocity;
    VSquared = Energy / (0.5 * Mass);
    Velocity = Math.sqrt(VSquared);
    return Velocity;
  }
  public static double SpeedToEnergy(double Mass, double Speed) {
    return 0.5 * Mass * (Speed * Speed);//KineticEnergy = 1/2 m * v^2
  }
  public static double SpeedToHeight(double Speed) {
    return (Speed * Speed) / (2.0 * GConst);// (V0*V0)/(2*g) https://www.quora.com/What-is-the-formula-for-finding-the-maximum-height-in-a-projectile
  }
  public static double SpeedToTime(double Speed) {
    return Speed * (2.0 / GConst);
  }
  public static double TimeToSpeed(double Time) {
    return Time * (GConst / 2.0);
  }
  public static double TimeToEnergy(double Mass, double Time) {
    double Speed = TimeToSpeed(Time);
    return SpeedToEnergy(Mass, Speed);
  }
  public static double HeightToSpeed(double Height) {
    double VSqared = Height * (2.0 * GConst);
    return Math.sqrt(VSqared);
  }
  public static double EnergyToHeight(double Mass, double Energy) {
    double Vel0 = EnergyToSpeed(Mass, Energy);
    return SpeedToHeight(Vel0);
  }
  public static double EnergyToTime(double Mass, double Energy) {
    double Vel0 = EnergyToSpeed(Mass, Energy);
    return SpeedToTime(Vel0);
  }
}

/*

 https://courses.lumenlearning.com/boundless-physics/chapter/projectile-motion/
 timeofflight = (2*V0)/G


 T=2⋅uyg

 universe expands

 https://www.nasa.gov/feature/goddard/2019/mystery-of-the-universe-s-expansion-rate-widens-with-new-hubble-data
 3.3 million light-years farther away a galaxy is from us, it appears to be moving 74 kilometers (46 miles) per second faster

 1 light year = 9.461e+12 kilometers

 3300000 light years to kilometers = 3.1220411e+19 

 74 / 3.1220411e+19 = 2.3702443e-18 per second

 (74 + 3.1220411e+19) / 3.1220411e+19 = 

 3.154e+9 seconds per century 

 ( (74 * 3.154e+9) + 3.1220411e+19) / 3.1220411e+19 = 1.00000000748 = linear ratio of space per 100 years (2000ad)/(1900ad) 


 3.154e+10 seconds per millenium
 ( (74 * 3.154e+10) + 3.1220411e+19) / 3.1220411e+19 = 1.00000007476 = linear ratio of space per 1k years (2000ad)/(1000ad)
 1.00000007476 * 1.00000007476 * 1.00000007476 = 1.00000022428 = cubic ratio of space per 1k years (2000ad)/(1000ad)

 ( (74 * 3.154e+10) + 3.1220411e+19) = 3.1220413e+19
 3.1220413e+19 * 3.1220413e+19 * 3.1220413e+19 = 3.043098e+58 
 3.1220411e+19 * 3.1220411e+19 * 3.1220411e+19 = 3.0430974e+58
 3.043098e+58 / 3.0430974e+58 = 1.00000019717 = cubic ratio of space (2000ad)/(1000ad)  WRONG

 space is linearly expanding by a ratio at every iteration
 every 1k years, it is 1.00000007476 times its previous linear dimension
 so the size of the vacuum increases by 
 programming: lets take a toy linear increase of 1.5 each iteration

 so the cubic volume also increases by a constant ratio. if the linear ratio is 1.5, the cubic ratio is 3.375:
 1*1*1 = 1 cube
 1.5 = 1.5
 1.5 * 1.5 * 1.5 = 3.375 cube
 1.5 * 1.5 = 2.25
 2.25*2.25*2.25 = 11.390625  cube
 1.5 * 1.5 * 1.5 = 3.375
 3.375*3.375*3.375 = 38.443359375  cube
 1.5 * 1.5 * 1.5  * 1.5 = 5.0625
 5.0625*5.0625*5.0625 = 129.746337891  cube
 1.5 * 1.5 * 1.5  * 1.5 * 1.5 = 7.59375
 7.59375* 7.59375*7.59375 = 437.893890381  cube

 437.893890381/129.746337891 = 3.37499999999
 129.746337891/38.443359375 = 3.37500000001
 38.443359375/11.390625 = 3.375
 11.390625/3.375 = 3.375


 Space expands the same ratio at all scales.  The scale of particles is small, but so is 

 density of the universe
 https://wmap.gsfc.nasa.gov/universe/uni_matter.html
 WMAP determined that the universe is flat, from which it follows that the mean energy density in the universe is equal to the critical density (within a 0.5% margin of error). This is equivalent to a mass density of 9.9 x 10-30 g/cm3, which is equivalent to only 5.9 protons per cubic meter. 

 5.9 protons per cubic meter

 5.9 * 1.00000000748 = 5.90000004413, or 0.00000004413 protons of new mass per cubic meter per century
 or 0.00000004413 * 10000000 = 0.4413  protons of new mass per cubic meter per billion years

 so almost one new proton's mass per cubic meter every 2 billion years is needed to keep constant density
 (2.26603217766 billion years)

 if you throw a ball at a wall and the wall is moving, no matter how slightly, the ball will come back to you with more or less energy depending on the movement.  

 if you bounce a frictionless ball on the floor it will come back exactly to your hand.  
 if the floor is 0.00000000748 of its original distance further away, it will not come back exactly to your hand.  

 so if 0.00000000748 more spontaneous particles are born than die every century, the ledger tips toward creation.  


 What's the Energy Density of the Vacuum?
 http://math.ucr.edu/home/baez/vacuum.html


 Never trust common sense estimates of scale without doing the math.
 ------------------------------------------------------------------------------------------------------
 Never trust common sense estimates of scale.

 First the model:

 If I toss a ball straight up and there is no friction, the ball will fall back with the same energy I put into it.

 But if I toss a ball straight up while space is expanding, the ball will have farther to fall on the return trip than when it went up, so it will hit my hand with more energy than I initially gave it.

 The expansion of space injects potential energy into the ball on the whole trip, which comes back as kinetic energy.

 So whenever potential energy takes the form of distance, such as between a ball and the Earth, or a particle and its antiparticle, any expansion of space -no matter how small- injects energy, and therefore mass, into the system.

 Now the math:
 ------------------------------------------------------------------------------------------------------
 Never trust common sense estimates of scale.

 First the model:

 If I toss a ball straight up without friction, the ball will fall back with the same energy I put into it.

 But if I toss a ball straight up while space is expanding, the ball will have farther to fall on the return trip than when it went up, so it will hit my hand with more energy than I initially gave it.

 The expansion of space injects potential energy into the ball on the whole trip, which comes back as kinetic energy.

 So whenever potential energy takes the form of distance, such as between a ball and the Earth, or a particle and its antiparticle, any expansion of space -no matter how small- injects energy, and therefore mass, into the system.

 Scale does not matter.  
 If a ball is 1 meter up and it gains X potential energy, then 100 balls each 1 centimeter up will together gain the same X potential energy.  
 Or a billion each one nanometer up, etc.

 The potential energy gained is always the same proportion to 

 Now the math:


 public static double SquarePower2(double Value, int Power) {// not as good as SquarePower
 int bsize = Integer.SIZE;// sizeof(Power)
 int bfirst = bsize - 2;// skip sign bit
 int Flag = 0;
 double Snowball = 1.0;
 int Mask = 1 << bfirst;
 for (int cnt = 1; cnt < bsize; cnt++) {
 Snowball *= Snowball;// square it
 Flag = Mask & Power;
 if (Flag != 0) {
 Snowball *= Value;
 }
 //System.out.println(" cnt:" + cnt + ", Snowball:" + Snowball);
 Mask >>= 1;// scan left to right, high bit to low bit
 }
 return Snowball;
 }

http://math.ucr.edu/home/baez/vacuum.html

 */
