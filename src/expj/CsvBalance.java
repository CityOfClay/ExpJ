package expj;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Multitool Csv Parser
 */
public class CsvBalance extends Csv {
  public HashMap<String, Double> CategorySums = null;
  /* ********************************************************************************************************* */
  public CsvBalance() {
  }

  /* ********************************************************************************************************* */
  public static void TestBalance() {
    CsvBalance CkCsv = new CsvBalance();
    CkCsv.Import("../Data/Year2023.pr.csv");

    CsvBalance SavCsv = new CsvBalance();
    SavCsv.Import("../Data/Year2023.bat.csv");
    CkCsv.Merge(SavCsv);

    CsvBalance CrCsv = new CsvBalance();
    CrCsv.Import("../Data/Year2023.ng.csv");
    CkCsv.Merge(CrCsv);

    CsvBalance MkCsv = new CsvBalance();
    MkCsv.Import("../Data/Year2023.uv.csv");
    CkCsv.Merge(MkCsv);

    CkCsv.Compact();

    CkCsv.PrintMe();
    // CkCsv.JustShopping();
    CkCsv.JustGroceries();
    CkCsv.Balance();
    CkCsv.PrintSums();
  }

  /* ********************************************************************************************************* */
  public static boolean IsAmazon(String Description) {
    Description = Description.toLowerCase();
    if (Description.startsWith("amazon")) {
      return true;
    }
    if (Description.startsWith("amzn")) {
      return true;
    }
    return false;
  }

  /* ********************************************************************************************************* */
  public static boolean IsTarget(String Description) {
    Description = Description.toLowerCase();
    if (Description.startsWith("target")) {
      return true;
    }
    return false;
  }

  /* ********************************************************************************************************* */
  public static boolean IsKohls(String Description) {
    Description = Description.toLowerCase();
    if (Description.startsWith("kohl")) {
      return true;
    }
    return false;
  }

  /* ********************************************************************************************************* */
  public static boolean IsMoon(String Description) {
    Description = Description.toLowerCase();
    if (Description.startsWith("moon")) {
      return true;
    }
    return false;
  }

  /* ********************************************************************************************************* */
  public static boolean IsEtsy(String Description) {
    Description = Description.toLowerCase();
    if (Description.startsWith("etsy")) {
      return true;
    }
    return false;
  }

  /* ********************************************************************************************************* */
  public void JustShopping() {
    ArrayList<ArrayList<String>> TableNext = new ArrayList<ArrayList<String>>();
    for (int rcnt = 0; rcnt < this.Table.size(); rcnt++) {
      ArrayList<String> Row = this.Table.get(rcnt);
      String Description = this.GetItem(Row, "Description");
      String Category = this.GetItem(Row, "Category");
      double Amount = this.GetNumber(Row, "Amount");

      boolean found = false;
      if (Category.equals("Shopping")) {
        if (found = IsAmazon(Description)) {
          Category = "Amazon";
        } else if (found = IsTarget(Description)) {
          Category = "Target";
        } else if (found = IsKohls(Description)) {
          Category = "Kohls";
        } else if (found = IsMoon(Description)) {
          Category = "Moon";
        } else if (found = IsEtsy(Description)) {
          Category = "Etsy";
        } else {
          Category = Description;
        }
        //if (found) {
        this.SetItem(Row, "Category", Category);
        //}
        TableNext.add(Row);
      }
    }
    this.Table = TableNext;
  }

  /* ********************************************************************************************************* */
  public void JustGroceries() {
    ArrayList<ArrayList<String>> TableNext = new ArrayList<ArrayList<String>>();
    for (int rcnt = 0; rcnt < this.Table.size(); rcnt++) {
      ArrayList<String> Row = this.Table.get(rcnt);
      String Description = this.GetItem(Row, "Description");
      String Category = this.GetItem(Row, "Category");
      double Amount = this.GetNumber(Row, "Amount");

      boolean found = false;
      if (Category.equals("Groceries")) {
        Description = Description.toLowerCase();
        if (found = Description.startsWith("trader")) {
          Category = "TJ";
        } else if (found = Description.startsWith("aldi")) {
          Category = "Aldi";
        } else if (found = Description.startsWith("safeway")) {
          Category = "Safeway";
        } else if (found = Description.startsWith("food lion")) {
          Category = "Food Lion";
        } else if (found = Description.startsWith("dollar tree")) {
          Category = "Dollar Tree";
        } else if (found = Description.startsWith("publix")) {
          Category = "Publix";
        } else {
          Category = Description;
        }
        //if (found) {
        this.SetItem(Row, "Category", Category);
        //}
        TableNext.add(Row);
      }
    }
    this.Table = TableNext;
  }

  /* ********************************************************************************************************* */
  public void Balance() {
    this.CategorySums = new HashMap<String, Double>();
    for (int rcnt = 0; rcnt < this.Table.size(); rcnt++) {
      ArrayList<String> Row = this.Table.get(rcnt);
      String Category = this.GetItem(Row, "Category");
      double Amount = this.GetNumber(Row, "Amount");
      if (Amount <= 0) {// Only count expenses
        if (this.CategorySums.containsKey(Category)) {// Build hash table.
          double Sum = this.CategorySums.get(Category);
          Sum += Amount;
          this.CategorySums.put(Category, Sum);
        } else {
          this.CategorySums.put(Category, Amount);
        }
      }
    }
  }

  /* ********************************************************************************************************* */
  public void Compact() {
    ArrayList<ArrayList<String>> TableNext = new ArrayList<ArrayList<String>>();
    for (int rcnt = 0; rcnt < this.Table.size(); rcnt++) {
      ArrayList<String> Row = this.Table.get(rcnt);
      String Category = this.GetItem(Row, "Category");
      double Amount = this.GetNumber(Row, "Amount");

      if (Amount <= 0) {// Only count expenses
        if (Category.equals("Auto & Transport") || Category.equals("Gas") || Category.equals("Service & Parts")) {
          Category = "Cars";
          this.SetItem(Row, "Category", Category);
        }
        if (!Category.equals("Transfer")) {// Self payments don't count. 
          if (!Category.equals("Credit Card Payment")) {
            if (!Category.equals("Paycheck")) {
              // Amount = -Amount;// Make positive for pie chart.
              // this.SetItem(Row, "Amount", Double.toString(Amount));
              TableNext.add(Row);
            }
          }
        }
      }
    }
    this.Table = TableNext;
  }

  /* ********************************************************************************************************* */
  public void PrintSums() {
    System.out.println("" + "Category" + ", " + "Sum");
    for (Map.Entry<String, Double> entry : this.CategorySums.entrySet()) {
      String key = entry.getKey();
      double value = entry.getValue();
      System.out.println("" + key + ", " + -value);// Make positive for pie chart.
    }
  }
}
