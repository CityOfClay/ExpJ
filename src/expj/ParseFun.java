package expj;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author MultiTool
 */
public class ParseFun {
  /* ********************************************************************************* */
  public static void Test() {
    /* For safeway, remove WT lines and clip any money that has 0.123 right of the decimal.  */
    String HomeDir = System.getProperty("user.home");
//    String FileName = HomeDir + "/Documents/Receipts/groceries00.txt";
    String FileName = HomeDir + "/Documents/Receipts/groceries002.txt";
//    FileName = HomeDir + "/Documents/Receipts/groceries006.txt";
//    FileName = HomeDir + "/Documents/Receipts/groceries005.txt";
//    FileName = HomeDir + "/Documents/Receipts/groceries004.txt";
//    FileName = HomeDir + "/Documents/Receipts/groceries003.txt";

//    String FileName = "../../Receipts/groceries00.txt";
    String txt = Csv.ReadFileAsString(FileName);
    String[] lines = txt.split("\n");
    StringBuilder SaveSb = new StringBuilder();
    for (String line : lines) {
      String[] chunks = line.split(" ");
      chunks = RemoveEmpty(chunks);
      if (chunks.length > 0 && !chunks[0].trim().toUpperCase().equals("WT")) {
        StringBuilder LineSb = new StringBuilder();
        boolean LineHasMoney = false;
        for (String WordIn : chunks) {
          String WordOut;
          if (IsMoney(WordIn)) {
            WordOut = ", " + FormatMoney(WordIn) + ", ";
            LineHasMoney = true;
          } else {
            WordOut = " " + WordIn + "";
          }
          LineSb.append(WordOut);
        }
        if (LineHasMoney) {
          SaveSb.append(LineSb.toString()).append("\n");
        }
      }
    }
    System.out.println(SaveSb.toString());
    System.out.println("");
  }
  /* ********************************************************************************* */
  public static String[] RemoveEmpty(String[] chunks) {
    int cnt = 0;// pack down empty strings 
    for (int ndx = 0; ndx < chunks.length; ndx++) {
      String word = chunks[ndx].trim();
      if (!word.equals("")) {
        chunks[cnt++] = word;
      }
    }
    return Arrays.copyOf(chunks, cnt);
  }
  /* ********************************************************************************* */
  public static boolean IsMoney(String txt) {
    /*
    Better plan:
    starts with $ means for sure, read until not a number or decimal. 
    starts with number, then look for decimal, then 2 more numbers. 
    every price has a decimal, right?
     */
    final String numbers = "0123456789.$";
    if (!txt.contains(".")) {
      return false;
    }
    for (int cnt = 0; cnt < txt.length(); cnt++) {
      char ch = txt.charAt(cnt);
//      if (ch == '$') { return true; }
      if (numbers.indexOf(ch) < 0) {// if (!numbers.contains(ch)) {
        return false;
      }
    }
    return true;
  }
  /* ********************************************************************************* */
  public static String FormatMoney(String txt) {
    txt = txt.trim();
    int FinalPoint = txt.lastIndexOf(".");
    if (FinalPoint >= 0) {
      if (txt.length() - FinalPoint > 2) {// turn $1.234 into $1.23 
        txt = txt.substring(0, FinalPoint + 3);
      }
    }
    if (!txt.contains("$")) {
      txt = "$" + txt;
    }
    return txt;
  }
}
