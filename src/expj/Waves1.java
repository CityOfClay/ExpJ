package expj;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author MultiTool to do: look up Verlet integration.
 */
public class Waves1 {
  public static final double TwoPi = Math.PI * 2.0;
  public static final int NDimsVector = 2;
  public static final int NDims = 2;
  public static final int NAxis = 2;
  public static final double GravConst = 1.0;// 6.67408(31) * 10−11;// 0.00001;// 1.0;// 
  public static final double TRate_Standard = 0.1;
//  public static final double TRate_Standard = 0.01;

  public int GridWidth = 2;// 6;
  public int GridHeight = 2;// 6;
  public int NumCells = GridWidth * GridHeight;
  public double SourceHeight = 2.0;// 10.0;
//  public int NumCells = 4;//40;// 1D.
  public static final boolean UseInc = false;
  private double VelAvg, HgtAvg, CorrectGlobalEnergy;
//  public static final boolean UseInc = true;

  enum DisplayMode {
    Graphic, Text
  };
//  public static DisplayMode Mode = DisplayMode.Text;

  public static DisplayMode Mode = DisplayMode.Graphic;
  public ArrayList<Cell> Grid = new ArrayList<Cell>();
  int GenCnt = 0;

  /* ********************************************************************************* */
  public static class Basis {
    public double HgtAvg, VelAvg;
  }

  /* ********************************************************************************* */
  public Waves1() {
    int GridSize = 2;// 60;// 30;
    if (NDims == 2) {// Square grids
//      GridSize = 60;// 30;
//      GridSize = 32;// 30
//      GridSize = 16;// 30
//      GridSize = 8;
//      GridSize = 6;
//      GridSize = 5;
//      GridSize = 4;
//      GridSize = 3;
      GridSize = 2;
    }
    this.GridWidth = GridSize;
    this.GridHeight = GridSize;
    if (false) {
//      this.GridWidth = 2;
      this.GridWidth = 3;
//      this.GridWidth = 4;
//      this.GridWidth = 5;
//      this.GridWidth = 6;

      this.GridHeight = 1;
//      this.GridHeight = 2;
    }
    this.NumCells = this.GridWidth * this.GridHeight;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = new Cell();
      this.AddCell(cell);
    }
    if (NDims == 1) {
      Connect1D();
    } else if (NDims == 2) {
      Connect2D();
    }
//    cell = this.Grid.get(0);
//    cell.Height = 10.0;
//    for (int cnt = 0; cnt < this.GridWidth / 4; cnt++) { 
//      cell = this.Grid.get(cnt);
//      cell.Height = 10.0;
//    }
//    System.out.println();
//    System.out.println("TRate:",this);
    this.Get_Global_Averages();
//    this.CorrectGlobalEnergy = this.Get_Global_Avg_Energy();
//    this.CorrectGlobalEnergy = this.Get_Global_Energy();

    //    SetGlobalTRate(0.4);
//    SetGlobalTRate(TwoPi / 2.0);
//    this.SetGlobalTRate(TwoPi / 4.0);
    this.SetGlobalTRate(TRate_Standard);
    this.Update_Tension();
    this.CorrectGlobalEnergy = this.Get_Global_Energy();
  }
  /* ********************************************************************************* */
  public static void TestFall() {
    double NumHills = 99.0;
    double NumHoles = 1.0;
    double Total = NumHills + NumHoles;
    double Avg = (NumHills - NumHoles) / Total;// 0.98
    double MoveEach = 1.0 - Avg;// 0.02
    double FallDist = NumHills / Total;
    double sum = 0.0;
    // Total potential energy is: (sum of every cell's distance from global average) / 2.0. But not for springs! 
    {
      double NumHillsExtra = NumHills - NumHoles;
      sum = NumHoles; // 1 for each hole filled
      sum += NumHillsExtra / Total;
      System.out.print(sum);
    }
    {
      FallDist = NumHills / Total;
      sum = 0.0;
      for (int cnt = 0; cnt < NumHills; cnt++) {// first hole (filled but not hill)
        sum += MoveEach * FallDist;
        FallDist -= MoveEach;
        if (FallDist < 0.0) {
          noop();
        }
      }
      System.out.print(sum);
    }

    FallDist = NumHills / Total;
    sum = 0.0;
    for (int cnt = 0; cnt < 98; cnt++) {// first hole (filled but not hill)
      sum += 0.01 * FallDist;
      FallDist -= 0.01;
    }
    FallDist = 0.98;
    for (int cnt = 0; cnt < 98; cnt++) {// second hole (filled but not hill)
      sum += 0.01 * FallDist;
      FallDist -= 0.01;
    }
    // sum = (double) 0.9799999999999992, so with 99 hills and 1 hole, total potential energy is 1.98. 
    System.out.print(sum);
  }
  /* ********************************************************************************* */
  public void AddCell(Cell cell) {
    cell.MyIdNum = this.Grid.size();
    this.Grid.add(cell);
  }
  /* ********************************************************************************* */
  public void DampRectangle(int MinX, int MinY, int MaxX, int MaxY, double Value) {
    int LastY = MaxY - 1, LastX = MaxX - 1;// mins are inclusive, maxes are exclusive
    Cell cell;
    // Fill top and bottom lines of rectangle.
    for (int ColCnt = MinX + 1; ColCnt < LastX; ColCnt++) {// Bottom row.
      cell = this.GetXY(ColCnt, MinY);
      cell.Set_Damp(Value);
    }
    for (int ColCnt = MinX + 1; ColCnt < LastX; ColCnt++) {// Top row.
      cell = this.GetXY(ColCnt, LastY);
      cell.Set_Damp(Value);
    }
    // Fill vertical left and right sides of rectangle.
    for (int RowCnt = MinY + 1; RowCnt < LastY; RowCnt++) {
      cell = this.GetXY(MinX, RowCnt);// left vertical side
      cell.Set_Damp(Value);
      cell = this.GetXY(LastX, RowCnt);// right vertical side
      cell.Set_Damp(Value);
    }
  }
  /* ********************************************************************************* */
  public void DampFrame(int NumFrames) {// Make a frame around the grid to absorb waves so they don't reflect back.
    int NumCols = this.GridWidth;
    int NumRows = this.GridHeight;
    int MinX = 0, MinY = 0, MaxX = NumCols, MaxY = NumRows;
//    int NumFrames = 4;// Absorbent border is NumFrames thick.
//    Other values: NumFrames = 10; or 24;20;
    double Value, FractAlong, FractFrom, StartingValue = 0.0;//10.0;
    for (int FrameCnt = 0; FrameCnt < NumFrames; FrameCnt++) {
      FractAlong = ((double) FrameCnt + StartingValue) / ((double) NumFrames + StartingValue);
      FractFrom = 1.0 - FractAlong;
      Value = 1.0 - Math.pow(FractFrom, 3.0);
      DampRectangle(MinX, MinY, MaxX, MaxY, Value);
      MinX++;// rectangle gets smaller each FrameCnt
      MinY++;
      MaxX--;
      MaxY--;
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public Cell GetXY(int XLoc, int YLoc) {
    Cell cell = this.Grid.get((YLoc * GridWidth) + XLoc);
    return cell;
  }
  /* ********************************************************************************* */
  public void Connect1D() {// Loop
    int LastPlace = NumCells - 1;
    Cell cell;
    Cell prev = this.Grid.get(LastPlace);
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      prev.NbrCross[0][1] = cell;
      cell.NbrCross[0][0] = prev;
      prev = cell;
    }
    // Seed the grid with a displaced area.
//    this.SourceHeight = 5.0;// 10.0;
    cell = this.Grid.get(0);
    cell.Height = this.SourceHeight;
//    cell.VSpeed = cell.VSpeed_Next = -0.662644757;
    cell.TRate_Prev = TRate_Standard;
    cell = this.Grid.get(1);
    cell.Height = -this.SourceHeight;
//    cell.VSpeed = cell.VSpeed_Next = -0.667475999999998;
    cell.TRate_Prev = TRate_Standard;
  }
  /* ********************************************************************************* */
  public void Connect2D() {// Torus 
    double CellSize = 20;
//    CellSize = 15;
//    CellSize = 50;
    Cell CellLeft, CellBelow, CellCenter;
    int XPrev, YPrev;
    YPrev = GridHeight - 1;
    for (int YCnt = 0; YCnt < GridHeight; YCnt++) {
      XPrev = GridWidth - 1;
      for (int XCnt = 0; XCnt < GridWidth; XCnt++) {
        CellCenter = this.GetXY(XCnt, YCnt);
        CellLeft = this.GetXY(XPrev, YCnt);
        CellBelow = this.GetXY(XCnt, YPrev);
        CellCenter.ConnectPrevNbr(CellLeft, 0);
        CellCenter.ConnectPrevNbr(CellBelow, 1);
        CellCenter.AssignGraphics(XCnt * CellSize, YCnt * CellSize, CellSize, CellSize);
//        CellCenter.TRate_Next = CellCenter.TRate = (YCnt + 0.00001) / (GridHeight + 0.00001);
        CellCenter.TRate_Prev = CellCenter.TRate = CellCenter.TRate_Next = TRate_Standard;
        XPrev = XCnt;
      }
      YPrev = YCnt;
    }

    if (false) {
      DampFrame(4);// Damp borders.
    }

    // Seed the grid with a displaced area.
    Cell cell;
    this.SourceHeight = 1.0;// 10.0;
//    this.SourceHeight = 2.0;// 10.0;
//    this.SourceHeight = 10.0;// 10.0;
    if (true) {
//      SourceHeight = 0.01;
      {// Seed energy 
        for (int cnt = 0; cnt < 1; cnt++) {
          cell = this.Grid.get(cnt);
          cell.Height = this.SourceHeight;
        }

//        cell.TRate_Prev = TRate_Standard;
//        cell.TRate = TRate_Standard;
      }
    }
  }
  /* ********************************************************************************* */
  public void Update_Tension() {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Update_Tension();
    }
  }
  /* ********************************************************************************* */
  public void RunCycle() {
    double WrongEnergy = this.Get_Global_Energy();
    this.Clamp_Global_Energy(this.CorrectGlobalEnergy / WrongEnergy);
    double RightEnergy = this.Get_Global_Energy();

    double FixRatio = this.CorrectGlobalEnergy / WrongEnergy;

    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Update();
    }

    double ETotLocal;
    ETotLocal = this.Get_Total_Local_Energy();

    double VspdSq = this.GetTotalVspdSq();
    double TensionSq = this.GetTotalTensionSq();
//    ETotLocal = VspdSq + TensionSq;

    System.out.print("" + this.GenCnt + ", ");
    this.Print_Me();
    System.out.print("" + ETotLocal + ", ");
    System.out.print("" + RightEnergy + ", ");
    System.out.print("" + VspdSq + ", ");
    System.out.print("" + TensionSq + ", ");
    System.out.print("" + FixRatio + ", ");

    System.out.println();

    if (true) {
      cell = this.Grid.get(0);
      switch (this.GenCnt) {
        case 100:
          // cell.TRate *= 4.0;
//          SetGlobalTRate(cell.TRate * 4.0);
          break;
        case 200:
          // cell.TRate *= 0.1;
//          SetGlobalTRate(cell.TRate * 0.1);
          break;
        case 500:
          System.exit(0);
        default:
          break;
      }
    }

    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Rollover();
    }
    this.GenCnt++;
  }
  /* ********************************************************************************* */
  public double Get_Total_Local_Energy() {
    double Energy = 0;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      Energy += cell.GetTotalEnergy();
      Energy += cell.Energy;
    }
    return Energy;
  }
  /* ********************************************************************************* */
  public double GetTotalVspdSq() {
    double VspdSqTotal = 0;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      VspdSqTotal += cell.GetTotalVspdSq();
    }
    return VspdSqTotal;
  }
  /* ********************************************************************************* */
  public double GetTotalTensionSq() {
    double TensionSqTotal = 0;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
//      TensionSqTotal += cell.GetTotalTensionSq();
      TensionSqTotal += cell.GetTotalTensionSq_Classic();
    }
    return TensionSqTotal;
  }
  /* ********************************************************************************* */
  public void SetGlobalTRate(double TRate) {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.TRate_Prev = TRate;
      cell.TRate = TRate;
    }
  }
  /* ********************************************************************************* */
  public void Get_Global_Averages() {// For energy clamp.
    Cell cell;
    this.VelAvg = 0;
    this.HgtAvg = 0;
    for (int cnt = 0; cnt < this.NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      this.VelAvg += cell.VSpeed;
      this.HgtAvg += cell.Height;
    }
    this.VelAvg /= (double) this.NumCells;
    this.HgtAvg /= (double) this.NumCells;
  }
  /* ********************************************************************************* */
  public double Get_Global_Avg_Energy() {// For energy clamp. Assumes Get_Global_Averages has been run. 
    double Energy = 0;
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      double DV = cell.VSpeed - this.VelAvg;
      double DH = cell.Height - this.HgtAvg;
      Energy += Cell.Calc_Energy(DV, DH);
    }
    return Energy;
  }
  /* ********************************************************************************* */
  public void Clamp_Global_Avg_Energy(double EnergyRatio) {// For energy clamp.
    Cell cell;// Energy clamping is an ugly hack to cover up a big bug - that trig-based cell energy collapses or explodes over time. 
    double EnergyRatioSqrt = Math.sqrt(EnergyRatio);
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Clamp_Relative_Energy(this.VelAvg, this.HgtAvg, EnergyRatioSqrt);
    }
  }
  /* ********************************************************************************* */
  public double Get_Global_Energy() {// For energy clamp.
    double Energy = 0;
    Cell cell;// Energy clamping is an ugly hack to cover up a big bug - that trig-based cell energy collapses or explodes over time. 
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      Energy += cell.GetTotalEnergy();
    }
    return Energy;
  }
  /* ********************************************************************************* */
  public void Clamp_Global_Energy(double EnergyRatio) {// For energy clamp.
    Cell cell;// Energy clamping is an ugly hack to cover up a big bug - that trig-based cell energy collapses or explodes over time. 
    double EnergyRatioSqrt = Math.sqrt(EnergyRatio);
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Clamp_Relative_Energy(EnergyRatioSqrt);
    }
  }
  /* ********************************************************************************* */
  public void Print_Header() {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Print_Header();
    }
  }
  /* ********************************************************************************* */
  public void Print_Me() {
    Cell cell;
    for (int cnt = 0; cnt < NumCells; cnt++) {
      cell = this.Grid.get(cnt);
      cell.Print_Me();
    }
  }
  /* ********************************************************************************* */
  public void Draw_Me(Gui.DrawingContext ParentDC) {// IDrawable

    Font currentFont = ParentDC.gr.getFont();
    Font newFont = currentFont.deriveFont(currentFont.getSize() * 0.75F);
    ParentDC.gr.setFont(newFont);

    int NumCells = this.Grid.size();
    for (int cnt = 0; cnt < NumCells; cnt++) {
      this.Grid.get(cnt).Draw_Me(ParentDC);
    }
  }

  /* ********************************************************************************* */
  public static class Cell {
    public double Height_Prev, Height, Height_Next;// Height.
    public double VSpeed_Prev, VSpeed, VSpeed_Next;// Vertical Speed.
    public double Tension, Tension_Next;
    double Displacement, DeltaV;
    public double Energy, Energy_Next;
    protected double Damp = 1.0; //0.99;//0.5;//0.95; //
    public double TRate_Prev, TRate = 0.001, TRate_Next;
    public Basis basis = new Basis();
    public Cell[][] NbrCross;
    public double[][] NbrTransfer;
    public int NumNbrs;
    public double XOrg, YOrg;// location to draw in grid.  Indexes or pixel coords? 
    public double Wdt, Hgt;// size of box to draw in grid.
    public int CellGenCnt = 0;
    public double PeakCnt = 0;
    public int MyIdNum = 0;
    /* ********************************************************************************* */
    public Cell() {
      this.Init_NbrCross();
      this.NumNbrs = NDims * NAxis;
      this.TRate = this.TRate_Next = TRate_Standard;
    }
    /* ********************************************************************************* */
    public void Init_NbrCross() {
      this.NbrTransfer = new double[NDims][NAxis];
      this.NbrCross = new Cell[NDims][NAxis];
      for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
        for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
          this.NbrCross[DimCnt][AxCnt] = null;
          this.NbrTransfer[DimCnt][AxCnt] = 0.0;
        }
      }
    }
    /* ********************************************************************************* */
    public void ConnectPrevNbr(Cell other, int Dimension) {// Connect me to a neighbor who has a lower index in the given dimension.
      this.NbrCross[Dimension][0] = other;// Connect my down pointer to neighbor.
      other.NbrCross[Dimension][1] = this;// Connect neighbor's up pointer to me.
    }
    /* ********************************************************************************* */
    public void Update() {// Calculate next time step.
//      AtomicInteger counter = new AtomicInteger();// Possible pass by reference for integer.
      this.Calc_Basis(this.basis);
      this.Calc_Height_Next(this.basis.VelAvg, this.basis.HgtAvg);
//      this.Calc_Height_Next(0.0, basis.HgtAvg);// or the old way.
      if (this.MyIdNum == 0) {
        noop();
      }
      this.CellGenCnt++;
    }
    /* ********************************************************************************* */
    public void Rollover() {// set up for next cycle
      this.Height_Prev = this.Height;
      this.Height = this.Height_Next;
//      this.Tension = this.Tension_Next;

      this.VSpeed_Prev = this.VSpeed;
      this.VSpeed = this.VSpeed_Next;

      this.TRate_Prev = this.TRate;
//      this.TRate = this.TRate_Next;
    }
    /* ********************************************************************************* */
    public double GetTotalEnergy() {
      double VSpeedSq;
      if (UseInc) {
        double VSpeedAdj = this.VSpeed / this.TRate;
        VSpeedSq = VSpeedAdj * VSpeedAdj;
      } else {
        VSpeedSq = this.VSpeed * this.VSpeed;
      }
      double TensionSq = this.Tension * this.Tension;
      double EnergyLocal = VSpeedSq + TensionSq;
      return EnergyLocal;
    }
    /* ********************************************************************************* */
    public double GetTotalVspdSq() {
      double VSpeedSq = this.VSpeed * this.VSpeed;
      return VSpeedSq;
    }
    /* ********************************************************************************* */
    public double GetTotalTensionSq() {// This is just wrong, delete. Squares are always positive and cannot cancel out. 
      Cell nbr;
      double Delta, SumSq = 0.0;
      for (int dcnt = 0; dcnt < NDims; dcnt++) {
        for (int axcnt = 0; axcnt < NAxis; axcnt++) {
          nbr = NbrCross[dcnt][axcnt];
          Delta = nbr.Height - this.Height;
          SumSq += Delta * Delta;// Sum of the squares, then average of the squares. 
        }
      }
      double TensionSq = SumSq / (double) this.NumNbrs;
      TensionSq *= 0.5; // Is this to split the tension with the neighbor?
      return TensionSq;
    }
    /* ********************************************************************************* */
    public double GetTotalTensionSq_Classic() {// 2/1.5 = 1.33333333333 is the correction we would need. 
      double TensionSq = this.Tension * this.Tension;
      return TensionSq;
    }
    /* ********************************************************************************* */
    public void Print_Header() {
      System.out.print("Height_Next, ");
      System.out.print("Tension, ");
      System.out.print("TRate_Next, ");
      System.out.print("VSpeed_Next, ");
    }
    /* ********************************************************************************* */
    public void Print_Me() {
      String rez = "%6.3e";
      rez = "%1.4f";
//      System.out.print("" + this.Height + ", ");
//      System.out.print(String.format(rez, this.Height_Next) + ", ");
//      System.out.print(String.format(rez, this.Tension) + ", ");
//      System.out.print(String.format(rez, this.TRate_Next) + ", ");
//      System.out.print(String.format(rez, this.VSpeed_Next) + ", ");

      System.out.print(" ");
      System.out.print(String.format(rez, this.Height) + ", ");
      System.out.print(String.format(rez, this.Tension) + ", ");
      System.out.print(String.format(rez, this.VSpeed) + ", ");
      System.out.print(String.format(rez, this.Energy) + ", ");
//      System.out.print(String.format(rez, this.VSpeed * this.VSpeed) + ", ");
//String txt = String.format("%1.3f\n%1.3f\n%1.3f\n%1.3f", this.Height, this.Tension, this.VSpeed, this.Energy);
      System.out.print("");
    }
    /* ********************************************************************************* */
    public static void DrawString(Gui.DrawingContext ParentDC, String text, int x, int y) {
      Graphics2D gr = ParentDC.gr;// https://stackoverflow.com/questions/4413132/problems-with-newline-in-graphics2d-drawstring
      for (String line : text.split("\n")) {
        gr.drawString(line, x, y += gr.getFontMetrics().getHeight());
      }
    }
    /* ********************************************************************************* */
    public void Print_Me_All() {
      double Stretch = 10;
//      System.out.print("(" + NbrTransfer[0][0] + ", " + this.Height + ", " + NbrTransfer[0][1] + "), ");//  print NbrTransfer[0], my amp, NbrTransfer[1].
      System.out.print("" + NbrTransfer[0][0] * Stretch + ",  " + NbrTransfer[0][1] * Stretch + ", ");
    }
    /* ********************************************************************************* */
    public void Draw_Me(Gui.DrawingContext ParentDC) {// IDrawable
      if (this.TRate <= 0.0) {
        ParentDC.gr.setColor(Color.BLACK);
      } else {
        double what = this.Height * 40;// / 8.0;// + 2; // Arbitrary magic number.
        Color col = Waves.ToNegPos(what);
        ParentDC.gr.setColor(col);
      }
      if (Double.isNaN(this.Height)) {
        noop();
      }
      // Draw cell background.
      ParentDC.gr.fillRect((int) this.XOrg, (int) this.YOrg, (int) this.Wdt, (int) this.Hgt);
      ParentDC.gr.setColor(Color.black);// Cell outline.
      ParentDC.gr.drawRect((int) this.XOrg, (int) this.YOrg, (int) this.Wdt, (int) this.Hgt);

      // Draw the energy vector.
      double EFactor = 100;
      double HalfWdt = this.Wdt * 0.5;
      double HalfHgt = this.Hgt * 0.5;
      double XCtr = this.XOrg + HalfWdt;
      double YCtr = this.YOrg + HalfHgt;
      if (false) {
        ParentDC.gr.setColor(Color.white);
        String txt = String.format("%1.3f\n%1.3f\n%1.3f\n%1.3f", this.Height, this.Tension, this.VSpeed, this.Energy);
        DrawString(ParentDC, txt, (int) this.XOrg, (int) this.YOrg);
      }
    }
    /* ********************************************************************************* */
    public void AssignGraphics(double XOrg, double YOrg, double Wdt, double Hgt) {
      this.XOrg = XOrg;
      this.YOrg = YOrg;
      this.Wdt = Wdt;
      this.Hgt = Hgt;
    }
    /* ********************************************************************************* */
    public void Set_Damp(double Value) {
      this.Damp = Value;
    }
    /* ********************************************************************************* */
    public void Assign_Energy(double Energy0) {// For energy clamp.
      double Linear = Math.sqrt(Energy0);
      double Hypot = Math.hypot(this.DeltaV, this.Tension);
      double Ratio = Linear / Hypot;
      this.DeltaV *= Ratio;
      this.Displacement *= Ratio;
      this.Tension *= Ratio;
      this.VSpeed = this.basis.VelAvg + this.DeltaV;
//      this.Height = this.basis.HgtAvg + this.Tension;
      this.Height = this.basis.HgtAvg + this.Displacement;// or this? 
    }
    /* ********************************************************************************* */
    public void Clamp_Relative_Energy(double EnergyRatioSqrt) {// For energy clamp. Scales energy by a ratio. 
      this.VSpeed *= EnergyRatioSqrt;// Coordinates are linear so we must use square root of energy ratio.
      this.Tension *= EnergyRatioSqrt;
    }
    /* ********************************************************************************* */
    public void Clamp_Relative_Energy(double VelOrg, double HgtOrg, double EnergyRatioSqrt) {// For energy clamp.
      double VDiff = this.VSpeed - VelOrg;// Scales energy relative to a given origin. 
      double HDiff = this.Height - HgtOrg;// Coordinates are linear so we must use square root of energy ratio.
      VDiff *= EnergyRatioSqrt;
      HDiff *= EnergyRatioSqrt;
      this.VSpeed = VelOrg + VDiff;
      this.Height = HgtOrg + HDiff;
//      this.Energy = Calc_Energy(this.VSpeed, double Tension0)
    }
    /* ********************************************************************************* */
    public static double Calc_Energy(double VSpeed0, double Tension0) {
      double TensionSq = Tension0 * Tension0;
      double VSpeedSq = VSpeed0 * VSpeed0;
      return VSpeedSq + TensionSq;
    }
    /* ********************************************************************************* */
    public static void Rotate(double XLoc, double YLoc, double Angle, Basis result) {
      // Rotation matrix
      double Cos = Math.cos(Angle);
      double Sin = Math.sin(Angle);
      double XLoc_Next = (XLoc * Cos) - (YLoc * Sin);
      double YLoc_Next = (XLoc * Sin) + (YLoc * Cos);

      result.VelAvg = (XLoc * Cos) - (YLoc * Sin);
      result.HgtAvg = (XLoc * Sin) + (YLoc * Cos);
    }
    /* ********************************************************************************* */
    public void Update_Tension() {
      Basis bas = new Basis();
      Calc_Basis(bas);
      this.Displacement = this.Height - bas.HgtAvg;
      this.Tension = -this.Displacement;// Spring force towards neighbors' height.
      this.Energy = Cell.Calc_Energy(this.VSpeed, this.Tension);
    }
    /* ********************************************************************************* */
    public void Calc_Height_Next(double OtherVel, double OtherHgt) {// best trig version. 
      this.Displacement = this.Height - OtherHgt;
      this.DeltaV = this.VSpeed - OtherVel;// Velocity is relative to neighbors' velocity. DOES NOT WORK. 
      this.Tension = -this.Displacement;// Spring force towards neighbors' height.
//      this.Energy = Cell.Calc_Energy(this.DeltaV, this.Displacement);
      this.Energy = Cell.Calc_Energy(this.VSpeed, this.Displacement);

//      double Hypot = Math.hypot(this.DeltaV, this.Displacement);
      // Rotation matrix
      double Cos = Math.cos(this.TRate);
      double Sin = Math.sin(this.TRate);

      double SpinX = (this.DeltaV * Cos) - (this.Displacement * Sin);
      double SpinY = (this.DeltaV * Sin) + (this.Displacement * Cos);

      this.Tension = -SpinY;

      this.VSpeed_Next = SpinX + OtherVel;
      this.Height_Next = SpinY + OtherHgt;

//      this.VSpeed_Next = (this.DeltaV * Cos) - (this.Displacement * Sin) + OtherVel;
//      this.Height_Next = (this.DeltaV * Sin) + (this.Displacement * Cos) + OtherHgt;
//        this.Tension_Next = this.Height_Next - OtherHgt;
      this.Energy_Next = Cell.Calc_Energy(SpinX, SpinY);
      if (this.MyIdNum == 0) {
        noop();
      }
    }
    /* ********************************************************************************* */
    public void Calc_Basis(Basis basis) {// Basis is just average of all neighbors. My height and speed are attracted to this value.
      double HgtSum = 0, VelSum = 0;//, NNbrs = 0;
      basis.HgtAvg = basis.VelAvg = 0.0;
      if (this.NumNbrs > 0) {// Average influence per neighbor.
        Cell nbr;// 4 neighbors in 2 dimensions
        for (int DimCnt = 0; DimCnt < NDims; DimCnt++) {
          for (int AxCnt = 0; AxCnt < NAxis; AxCnt++) {
            nbr = this.NbrCross[DimCnt][AxCnt];
            if (nbr != null) {
              HgtSum += nbr.Height;
              VelSum += nbr.VSpeed;
//            NNbrs++;
            }
          }
        }
//        basis.HgtAvg = HgtSum / NNbrs;//  this.NumNbrs;
//        basis.VelAvg = VelSum / NNbrs;
        basis.HgtAvg = HgtSum / (double) this.NumNbrs;
        basis.VelAvg = VelSum / (double) this.NumNbrs;
      }
    }
    /* ********************************************************************************* */
    public void Interact(Cell other) {// Experimental. 1:1 interaction between cells so we can audit the sum energy level. 
      double HgtSum = 0, VelSum = 0;
      double DV = this.VSpeed - other.VSpeed;
      double DH = this.Height - other.Height;
      /*
      Plan is
      get energy between cells?  or sum energy of both cells? 
      get tension between cells.
      apply tension to own vspeed.
      apply tension to other vspeed.
      get delta energy for both sides.
      add deltas, should be same as total energy when we started. 
      multiply both energies by corrector: corr = goodE/badE; 
      
      how can 
      
      // see who is gaining and who is losing energy? 
      
       */
    }
    /* ********************************************************************************* */
    public void Calc_Amp(double OtherAmp) {// From HotWaves. Calc_Amp and Rollover from bench testing.  this works.  For reference, not used here.
      double TRateSquared = this.TRate * this.TRate;
      double Delta = OtherAmp - this.Height;// Spring force
      Delta *= TRateSquared;
      this.Height_Next = this.Height + this.VSpeed;
      this.Height_Next = this.Height_Next + Delta;
      this.VSpeed_Next = this.Height_Next - this.Height;
      this.VSpeed_Next *= this.Damp;
    }
  }

  /* ********************************************************************************* */
  public static void WaveMetric_Trig() {
    double TRate = 0.1;
    double Height = 0, Height_Next = 0;
    double VSpeed = 0, VSpeed_Next = 0;
    double VSpeed_Temp;
    double Displacement, Base = 0.0;
    double Cycles = 0.0;// a true cycle would be two pi, these are just 0-1.0 
    int Generations = 400;
    Generations = 4000;

    Height = 1.0;
    for (int GenCnt = 0; GenCnt < Generations; GenCnt++) {
      if (true) {
        if (GenCnt == Generations / 4) {
          TRate *= 0.1;
        } else if (GenCnt == Generations / 2) {
          TRate *= 4.0;
        }
      }
      Base = Math.sin(Cycles * 3.17);
      Base = 0;
      Displacement = Height - Base;
      double VSpeedSq = VSpeed * VSpeed;
      double HeightSq = Height * Height;
      double Energy = HeightSq + VSpeedSq;

      VSpeed_Temp = VSpeed;
      if (false) {
        double TRateSq = TRate * TRate;
        double TRatio = 1.0;
        double Tension = -Displacement;
        VSpeed_Temp = (VSpeed * TRatio) + (Tension * TRateSq);
      }

      if (false) {// Rotate 
        double Radius = Math.hypot(VSpeed, Displacement);
        double Angle = Math.atan2(Displacement, VSpeed);
        Angle += TRate;
        VSpeed_Next = Math.cos(Angle) * Radius;
        Height_Next = Math.sin(Angle) * Radius;
        System.out.print(Displacement + ", " + VSpeed + ", " + Angle + ", " + Energy + ", ");
      } else if (true) {// Rotate matrix
        double Cos = Math.cos(TRate);
        double Sin = Math.sin(TRate);
        VSpeed_Next = (VSpeed_Temp * Cos) - (Displacement * Sin);
        Height_Next = (VSpeed_Temp * Sin) + (Displacement * Cos);
        Height_Next += Base;
        System.out.print(GenCnt + ", " + Height + ", " + VSpeed + ", " + Base + ", " + Displacement + ", " + Energy + ", ");
      }

//      Base += TRate * 3.17;
      Cycles += TRate * 3.17;
      Height = Height_Next;
      VSpeed = VSpeed_Next;
      System.out.println();
    }
    System.out.println();
  }
  /* ********************************************************************************* */
  public static double Sigmoid(double xin) {
    double OutVal;
    OutVal = xin / Math.sqrt(1.0 + xin * xin);// symmetrical sigmoid function in range -1.0 to 1.0.
    return OutVal;
  }
  /* ********************************************************************************* */
  public static Color ToNegPos(double Fraction) {// negative is blue, positive is red 
    Fraction = Sigmoid(Fraction * 0.06);
    Fraction = (Fraction + 1.0) / 2.0;
    return new Color((float) (Fraction), (float) 0.0, (float) (1.0 - Fraction));
  }

  /* ********************************************************************************* */
  public static void noop() {
  }
}
