package expj;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * @author Multitool
 */
public class CoilForce {

  /* ********************************************************************************* */
  public static class WireType {
    // 22 awg, diam 0.6438, ohms per km 52.962, max amps 7
    public double diameter;
    public double ohms_per_mm;
    public double max_amps;
    public WireType() {
    }
    public WireType(double diameter, double ohms_per_mm, double max_amps) {
      this.diameter = diameter;
      this.ohms_per_mm = ohms_per_mm;
      this.max_amps = max_amps;
    }
  }

  /* ********************************************************************************* */
  public static void CoilForce() {
    /*
https://www.electricaltechnology.org/2022/04/american-wire-gauge-awg-chart-wire-size-ampacity-table.html

    diameter        area            ??      resistance      max amps
awg mm      inch    mm2     Inch2   kcmil   Ω/kft   Ω/km    Chassis Wiring  Power Transfer

20	0.8118	0.0320	0.5176	0.0008	1.0215	10.152	33.308	11              1.5
21	0.7229	0.0285	0.4105	0.0006	0.8101	12.802	42.001	9               1.2
22	0.6438	0.0253	0.3255	0.0005	0.6424	16.143	52.962	7               0.92

20 awg, diam 0.8118, ohms per km 33.308, max amps 11
22 awg, diam 0.6438, ohms per km 52.962, max amps 7
     */
    double NewtonsToPounds = 0.224809;
    double PoundsToNewtons = 1.0 / NewtonsToPounds;
    double PoundsToGrams = 453.592;
    double GramsToPounds = 1.0 / PoundsToGrams;
    double PoundsToKilograms = PoundsToGrams / 1000.0;
    double piston_grams = 10;
    // piston_newtons = grams to pounds, pounds to newtons
    // 
    double piston_newtons = (piston_grams * GramsToPounds) * PoundsToNewtons;// magnetomotive force
    double turnamps = piston_newtons;// magnetomotive force, // newtons = amps*turns
    double profile = Math.sqrt(turnamps);
    System.out.println("turnamps:" + turnamps);
    System.out.println("profile:" + profile);
//   0.09902855909927259
//   0.3131558001582938
//turnamps:0.09806655517278123
//profile:0.3131558001582938
    System.out.println();
    {// opposite direction
      double turns = 200;
      double amps = 6;
      turnamps = turns * amps;
      piston_newtons = turnamps;
      double mass = piston_newtons * NewtonsToPounds * PoundsToGrams;
      System.out.println("mass:" + mass);
      /*
turnamps:0.09806655517278123
profile:0.3131558001582938
mass:122365.8767136 grams
mass:122.3658767136 kilograms
       */
    }
    if (false) {// object is to start with wire length, get volume, then get radius of circle.
      double mm_per_foot = 304.8;
      double feet_per_mm = 1.0 / mm_per_foot;
      double ohms_needed = 2.0;
      // 22 awg resistance is 16.14 per 1000 feet.
      // 1000 feet = 304800 mm 
      // 51.6315572076 feet of wire for a coil
      double wire_len_ft = 51.6315572076;// from 5v/6amps = 0.83333333333 ohms, so 0.83333333333 / 0.01614 = 51.6315572076 feet of wire.
      double wire_len_mm = wire_len_ft * mm_per_foot;

      double wire_ohms_ft = 0.01614;
      double wire_ohms_mm = wire_ohms_ft * feet_per_mm;
      double wire_mm_per_ohm = 1.0 / wire_ohms_mm;
      double hole_radius_cm = 0.25; // cm
      double hole_radius_mm = hole_radius_cm * 10.0;
//      double wire_len_mm = wire_mm_per_ohm * ohms_needed;

//      double wire_radius_mm = 1.0;// get this later
//      double wire_diameter_mm = wire_radius_mm * 2.0;// get this later
      double wire_diameter_mm = 0.6426;// 22 awg 
      double wire_radius_mm = wire_diameter_mm / 2.0;
      double coil_len_mm = 1.0;
      coil_len_mm = 1.0;
      double coil_radius_mm = 1.0;
      double wire_area_mm = wire_diameter_mm * wire_diameter_mm;// square cross section of wire 
      double wire_volume_mm = wire_area_mm * wire_len_mm;// vol per unit length.
      double hole_area_mm = (Math.PI * (hole_radius_mm * hole_radius_mm)) * coil_len_mm;
      double coil_face_area_mm = (Math.PI * (coil_radius_mm * coil_radius_mm)) - hole_area_mm;
      double coil_volume_mm = coil_face_area_mm * coil_len_mm;

//      coil_volume = ((Math.PI * (coil_radius * coil_radius)) - hole_area) * coil_len;
//      coil_volume/coil_len + hole_area = (Math.PI * (coil_radius * coil_radius))
//      coil_volume/coil_len + hole_area / Math.PI= (coil_radius * coil_radius)
//      sqrt(coil_volume/coil_len + hole_area / Math.PI) = coil_radius
      coil_volume_mm = wire_volume_mm;// could use wire area and save steps.
      coil_radius_mm = Math.sqrt((coil_volume_mm / coil_len_mm) + hole_area_mm / Math.PI);
      System.out.println("coil_radius_mm:" + coil_radius_mm);// 124.91035315637593
      System.out.println("coil_radius_cm:" + coil_radius_mm / 10);// 124.91035315637593
      // coil_radius_mm:124.91035315637593
      // coil_radius_cm:12.491035315637593
      System.out.println();
    }
    if (true) {// object is to start with wire length, get volume, then get radius of circle.
      // 20 awg, diam 0.8118, ohms per km 33.308, max amps 11
      // 22 awg, diam 0.6438, ohms per km 52.962, max amps 7

      WireType awg20 = new WireType(0.8118, 33.308 / (1000 * 1000), 11.0);
      WireType awg22 = new WireType(0.6438, 52.962 / (1000 * 1000), 7.0);
//      WireType wire = awg20;
      WireType wire = awg22;

      double spool_len_mm = 154.534 * 1000.0;// 507 feet = 154.534 meters 
      double mm_per_foot = 304.8;
      double feet_per_mm = 1.0 / mm_per_foot;
      double hole_radius_mm = 2.5;
//      hole_radius_mm = 2.75;// 5.5 mm diameter
      hole_radius_mm = 3.0;// 6 mm diameter
      hole_radius_mm = 4.0;// 8 mm diameter, coil_radius_cm:1.4952926456091422

      double wire_diameter_mm = wire.diameter;// conflict of sources! 0.6426;// 22 awg 
      double wire_ohms_ft = 0.01614;// 22 awg 
      double wire_ohms_mm = wire.ohms_per_mm;// wire_ohms_ft * feet_per_mm;// 22 awg wire_ohms_mm = 5.295275590551181E-5 

      double wire_ohms_km = wire_ohms_mm * 1000 * 1000;// 22 awg wire_ohms_km = 52.952755905511815  conflict! table says wire_ohms_km = (double) 33.308
      double wire_area_mm2 = (wire_diameter_mm * wire_diameter_mm);// square area, for how the wire stacks.
      double wire_volume_per_mm_mm3 = wire_area_mm2 * 1.0;// volume of wire per mm

      double wire_max_amps_ohm = wire.max_amps - 1.0;// 6.0;// 22 awg max amps is 7, so 6 for margin.
      double volts = 5.0;
      double ohms_needed = volts / wire_max_amps_ohm;// minimum ohms needed to prevent an overload/short.
      double wire_len_mm = ohms_needed / wire_ohms_mm;

//      double wire_len_ft = 51.6315572076;// from 5v/6amps = 0.83333333333 ohms, so 0.83333333333 / 0.01614 = 51.6315572076 feet of wire.
//      double wire_len_mm = wire_len_ft * mm_per_foot;
      double coil_len_mm = 10.0;// 1 cm
      coil_len_mm = 8.9;// L size bobbin 
      coil_len_mm = 8.0;// L size bobbin minus .9mm thickness, coil_radius_mm:16.59781648294734
      double coil_radius_mm = get_coil_radius(wire_volume_per_mm_mm3, wire_len_mm, hole_radius_mm, coil_len_mm);

      double turns = coil_len_mm * (coil_radius_mm - hole_radius_mm) / wire_diameter_mm;

      System.out.println("wire_len_mm:" + wire_len_mm);// wire_len_mm:15737.29863692689
      System.out.println("wire_len_m:" + wire_len_mm / 1000.0);// wire_len_m:15.737298636926889
      System.out.println("wire_len_ft:" + wire_len_mm * feet_per_mm);// wire_len_ft:51.63155720776538
      System.out.println("coil_radius_mm:" + coil_radius_mm);// coil_radius_mm:14.598046706929818 mm
      System.out.println("coil_radius_cm:" + coil_radius_mm / 10.0);// coil_radius_cm:1.4598046706929817
      System.out.println("turns:" + turns);// turns:188.2671445211612

//      turns = 188;/* now start with 200 turns and find the length of wire. */
      turns = 200;/* now start with 200 turns and find the length of wire. */
//      turns = 190;/* now start with 200 turns and find the length of wire. */
      wire_len_mm = get_wire_len_mm(turns, wire_diameter_mm, hole_radius_mm, coil_len_mm);
      System.out.println("turns:" + turns + ", wire_len_mm:" + wire_len_mm);// turns:188.26714452151438, wire_len_mm:15737.298636926895
      System.out.println("turns:" + turns + ", wire_len_m:" + wire_len_mm / 1000.0);// turns:188.26714452151438, wire_len_m:15.737298636926894
      System.out.println("turns:" + turns + ", wire_len_feet:" + wire_len_mm * feet_per_mm);// turns:188.26714452151438, wire_len_feet:51.631557207765404
      System.out.println("turns:" + turns + ", wire_len_feet * 8:" + wire_len_mm * feet_per_mm * 8);// turns:190.0, wire_len_feet * 8:443.9493380563057
      // turns:181.94733058388186, wire_len_feet * 8:413.05245766212323
      // turns:200.0, wire_len_feet * 8:483.80639270969215

      System.out.println("spool_len_mm:" + spool_len_mm);// spool_len_mm:154534.0
      System.out.println("spool_len_mm div 8:" + spool_len_mm / 8.0);// spool_len_mm div 8: 19316.75 mm, 19.31675 meters max coil wire len. 
      /*
turns:200.0, wire_len_mm:17455.24807092592
turns:200.0, wire_len_m:17.455248070925922
turns:200.0, wire_len_feet:57.26787424844463
for 200 turns, 507 foot spool/ 57 feet per coil = 8.89473684211 coils

507 feet = 154.534 meters 

turns:190.0, wire_len_mm:15985.583063197562 per coil 
turns:190.0, wire_len_m:15.985583063197563
turns:190.0, wire_len_feet:52.446138658784655

154.534 / turns
       */
      System.out.println();
    }
    System.out.println();
  }
  /*
 
wire_len_mm:15737.29863692689
wire_len_m:15.737298636926889
wire_len_ft:51.63155720776538
coil_radius_mm:14.598046706952513
coil_radius_cm:1.4598046706952512
turns:188.26714452151438
turns:188.26714452151438, wire_len_mm:15737.298636926893
turns:188.26714452151438, wire_len_m:15.737298636926893
turns:188.26714452151438, wire_len_feet:51.6315572077654
   */
  /* ********************************************************************************* */
  public static double get_wire_len_mm(double turns, double wire_diameter_mm, double hole_radius_mm, double coil_len_mm) {
    /* Return wire length, using number of turns. */
    double wire_area_per_mm_mm2 = wire_diameter_mm * 1.0;// 1.0 mm length.
    double wires_per_coil_len_mm = coil_len_mm / wire_diameter_mm;
    double turns_per_face = turns / coil_len_mm;
    double coil_radius_mm = turns_per_face * wire_diameter_mm + hole_radius_mm;
    double coil_face_area_mm2 = Math.PI * (coil_radius_mm * coil_radius_mm) - Math.PI * (hole_radius_mm * hole_radius_mm);
    double wire_len_mm = (coil_face_area_mm2 / wire_area_per_mm_mm2) * wires_per_coil_len_mm;
    return wire_len_mm;
  }

  /* ********************************************************************************* */
  public static double get_coil_radius(double wire_volume_per_mm_mm3, double wire_len_mm, double hole_radius_mm, double coil_len_mm) {
    /* Return coil radius. Need wire length, wire vol per mm, coil length.. */
    // wire_volume_per_mm_mm3 is volume of wire per mm, in mm 3.
    double coil_volumn_mm3 = wire_volume_per_mm_mm3 * wire_len_mm;
    double coil_face_area_mm2 = coil_volumn_mm3 / coil_len_mm;
    double hole_area = Math.PI * (hole_radius_mm * hole_radius_mm);
    /*
    area = pi * r2 - pi * rh2 
    area/pi = r2 - rh2 
    rh2 + area/pi = r2
    r = sqrt(rh2 + area/pi)
     */
    double coil_radius_mm = Math.sqrt((hole_radius_mm * hole_radius_mm) + (coil_face_area_mm2 / Math.PI));
    return coil_radius_mm;
  }

  /* ********************************************************************************* */
  public static void LowPass() {
    // https://www.electronics-tutorials.ws/filter/filter_2.html
    /* Given a frequency, capacitance, resistance and voltage, return the filtered AC voltage. */
    double Frequency = 1.0, Capacitance = 1.0;
    double Resistance = 1.0;
    double Vin = 10.0;

    Capacitance = 47.0 * (1.0 / 1000000000.0);// 47 nF
    Resistance = 4700.0;
    Frequency = 100.0;// Xc:33862.75384933943  Vout:9.905048648448663
    Frequency = 10000.0; // Xc:338.6275384933943 Vout:0.7186213663875665

    Capacitance = 100.0 * (1.0 / 1000000000.0);// 100 nF
    Frequency = 14000.0;
    Frequency = 10.0;
    Vin = 1000.0;// Xc:338.6275384933943 Vout:71.86213663875665
    Resistance = 100000.0;// Xc:113.68210220849667 Vout:1.1368202874935132
    
    double Xc = 1.0 / (2.0 * Math.PI * Frequency * Capacitance);// Xc is capacitive reactance
    double Vout = Vin * (Xc / Math.sqrt(Resistance * Resistance + Xc * Xc));
    System.out.println("Xc:" + Xc);
    System.out.println("Vout:" + Vout);
    System.out.println();
  }

  /* ********************************************************************************* */
  public static void Capacitors() {
    NumberFormat formatter = new DecimalFormat("#0.0000000000000000");
    double tomicro = 1.0 / 1000000.0;//0.0000001
    double tonano = 1.0 / 1000000000.0;
    double C0 = tomicro * 0.1;
    double C1 = tonano * 1.0;
    double C2 = tomicro * 100.0;
    double C3 = tonano * 10.0;

    System.out.println(formatter.format(C0));
    System.out.println(formatter.format(C1));
    System.out.println(formatter.format(C2));
    System.out.println(formatter.format(C3));
    /*
0.0000001000000000 .1 micro
0.0000000010000000  1 nano 
0.0000010000000000 
ideal, 10nf:
0.0000000100000000
0.0000000009900990 series 

0.0000001000000000
0.0000000010000000
0.0000010000000000
0.0000000100000000
0.0000000009990010 series

0.0000000100000000
0.0000000009999900
     */
    double series = 1.0 / (1.0 / C2 + 1.0 / C1);
    System.out.println(formatter.format(series));
    /// 0.000,000,000,9900990

    System.out.println(series);// 9.900990099009903E-10
    System.out.println();
  }
}
