package expj;

import expj.data_grid.data_line;
import expj.data_grid.data_table;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class preprocessor {
  /* ********************************************************************************************************* */
  public static boolean evaluate(data_line line, Set<String> flags) {
    List<String> expression = line.subList(1, line.size());
    String joined = String.join(" ", expression);
    String[] parts = joined.split("\\|\\|");// Really cheesy evaluation - assumes everything is OR without even checking the operator!
    for (String part:parts) {
      String[] subparts = part.trim().split("\\s+");
      for (String subpart:subparts) {
        if (flags.contains(subpart)) {
          return true;
        }
        if (subpart.trim().toLowerCase().equals("true")) {
          return true;// support literal 'true' for testing.
        }
      }
    }
    return false;
  }
 
  /* ********************************************************************************************************* */
  public static boolean is_if(data_line row) {
    if (row.isEmpty()) {return false; }
    // if (row.size() < 2){ return false; }
    String first_field = row.get(0);// only reads first line, not content of clause
    return first_field.equals("IF");
  }
 
  /* ********************************************************************************************************* */
  public static boolean is_elseif(data_line row) {
    String first_field = row.get(0);// only reads first line, not content of clause
    return first_field.equals("ELSEIF");
  }
 
  /* ********************************************************************************************************* */
  public static boolean is_else(data_line row) {
    String first_field = row.get(0);// only reads first line, not content of clause
    return first_field.equals("ELSE");
  }
 
  /* ********************************************************************************************************* */
  public static boolean is_end(data_line row) {
    if (row.isEmpty()) {return false; }
    String first_field = row.get(0);
    return first_field.equals("END");
  }
 
  /* ********************************************************************************************************* */
  public static int try_dead_if(data_table lines, int start_line_ndx) {
    int ndx = start_line_ndx;// Look for an IF clause within a disabled block.
    // If you find an IF, return line number right *after* END.
    data_line row = lines.get(ndx);
    if (row.size() < 2){ return start_line_ndx; }
    if (is_if(row)) {
      ndx++;// Ignore ELSE and ELSEIF.
      while (ndx < lines.size()) {
        if (is_end(lines.get(ndx))) {
          return ++ndx;
        } else if (((ndx = try_dead_if(lines, ndx)) > start_line_ndx)) {// Recurse nested IFs.
        } else {
          ndx++;
        }
        start_line_ndx = ndx;
      }
    }
    return ndx;
  }
 
  /* ********************************************************************************************************* */
  public static int try_live_if(data_table lines, int start_line_ndx, Set<String> flags, data_table condensed_lines) {
    // IF, ELSEIF, ELSE, END is a ladder like a case statement. The first rung to be true wins, skip everything else.
    boolean include;
    int ndx = start_line_ndx;
    data_line line = lines.get(ndx);
    if (is_if(line)) {
      include = evaluate(line, flags);
      ndx++;
      start_line_ndx = ndx;
      while (!include && ndx < lines.size()) {// Loop until we find something that evals to 'true', or we hit END and bail out.
        line = lines.get(ndx);
        if (is_elseif(line)) {
          ndx++;
          if (include = evaluate(line, flags)) {
            break;
          }
        } else if (is_else(line)) {
          include = true; ndx++; break;
        } else if (is_end(line)) {
          return ++ndx;
        } else if ((ndx = try_dead_if(lines, start_line_ndx)) > start_line_ndx) {// Recurse nested IFs.
        } else {// Normal lines to ignore.
          ndx++;
        }
        start_line_ndx = ndx;
      }
 
      // Everything that might eval to 'true' gets recorded in this block.
      start_line_ndx = ndx;
      while (include && ndx < lines.size()) {
        line = lines.get(ndx);
        if ((ndx = try_live_if(lines, start_line_ndx, flags, condensed_lines)) > start_line_ndx) {// Recurse nested IFs.
        } else if (is_elseif(line) || is_else(line)) {// block terminators
          ndx++;
          break;
        } else if (is_end(line)) {// block terminator
          return ++ndx;
        } else { // Normal lines to record.
          condensed_lines.add(line);
          ndx++;
        }
        start_line_ndx = ndx;
      }
 
      // After this, zombie-crawl through everything else that won't be evaluated.
      start_line_ndx = ndx;
      line = lines.get(ndx);
      while (ndx < lines.size()) {// Just skip to the END.
        if (is_end(line)) {
          return ++ndx;
        } else if (((ndx = try_dead_if(lines, ndx)) > start_line_ndx)) {// Recurse nested IFs.
        } else {// Normal lines to ignore.
          ndx++;
        }
        line = lines.get(ndx);
        start_line_ndx = ndx;
      }
    }
    return ndx;
  }
 
  /* ********************************************************************************************************* */
  public static void parse_all(data_table lines, Set<String> flags, data_table condensed_lines){
    int line_prev_ndx = 0, line_ndx = 0;
    while (line_ndx < lines.size()) {
      if ((line_ndx = try_live_if(lines, line_prev_ndx, flags, condensed_lines)) > line_prev_ndx) {
        System.out.println("out line_ndx:" + line_ndx);
      } else {
        condensed_lines.add(lines.get(line_ndx++));
      }
      line_prev_ndx = line_ndx;
    }
  }
 
  /* ********************************************************************************************************* */
  public static data_table preprocess_to_table(String file_txt, Set<String> flags) {
    data_table dlist = new data_table();
    dlist.consume(file_txt);
    data_table condensed = new data_table();
    parse_all(dlist, flags, condensed);
    return condensed;
  }
 
  /* ********************************************************************************************************* */
  public static void test() {
    String test_txt = "";
//    test_txt ="""
//            Line zero.
//            IF true
//              First if.
// 
//              IF false
//              Nested if.
//              ELSE
//              Nested else.
//              END
//            ELSEIF false
//              Yes print first else if.
//              IF false
//              Second nested if.
//              ELSE
//              This should print.
//              END
//            ELSE
//              Final Else, never print.
//            END
//            Bystander, always print.
//            """;
 
    data_table dlist = new data_table();
    dlist.consume(test_txt);
   
    data_table condensed = new data_table();
    HashSet<String> flags = new HashSet<>();
//    Set<String> flags = Set.of("bleh");
    flags.add("bleh");
 
    parse_all(dlist, flags, condensed);
    System.out.println("---------- condensed ----------");
    System.out.println(condensed.toString());
    System.out.println();
  }
}
 
 
